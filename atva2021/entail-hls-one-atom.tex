%!TEX root = atva2021.tex


\subsection{Consequent with one spatial atom}
\label{ssec:ent-1}

Consider the ordered entailment 
$\varphi \models_\preceq \psi$, where 
$\varphi \equiv \Pi: a_1 \sepc \cdots \sepc a_m$, 
$\psi \equiv \Pi': b_1$
and the constraints (\ref{eq:order})--(\ref{eq:order-conseq}) are satisfied.
From~(\ref{eq:order}) and the definition of $\abs$, we have that 
$C_\preceq \land \abs(\varphi)$ implies $\Pi'$, 
so we simplify this entailment to deciding:
\begin{align*}
C_\preceq \wedge \Pi: a_1 \sepc \cdots \sepc a_m \models_\preceq b_1,
\end{align*}
where, the atoms $a_i$ ($i\in [m]$) and $b_1$ represent \emph{non-empty} heaps,
and the start and end addresses of atoms $a_i$ as well as those of $b_1$ are totally ordered by $C_\preceq$.
  
Because $b_1$ defines a continuous memory region, 
the procedure checks the following necessary condition in \PbA: 
%for the entailment $\varphi  \models_\preceq \psi$ is 
{\small
\begin{align*}
C_\preceq \models 
%\\
\atomhead(a_1) = \atomhead(b_1) \wedge \atomtail(a_m) = \atomtail(b_1) \wedge \bigwedge \limits_{1 \le i < m} \atomtail(a_i) = \atomhead(a_{i+1}).
\end{align*}
}
%In the sequel, we assume that the aforementioned condition holds.
%Then, we distinguish between the different forms of $b_1$. 
\noindent
Then, the procedure does a case analysis on the form of $b_1$.
If $b_1 \equiv t_1 \pto t_2$ 
	then $\varphi  \models_\preceq \psi$ holds iff $m = 1$ and $a_1 = t'_1 \pto t'_2$.
If $b_1 \equiv \blk(t_1, t_2)$ 
	then $\varphi  \models_\preceq \psi$ holds.
For the last case, 
$b_1 \equiv \hls{}(t_1, t_2; t_3)$, we distinguish between $m=1$ or not.

%\vspace{-3mm}
\subsubsection{One atom in the antecedent:} A case analysis on the form of $a_1$ follows.

\myfpar{$a_1 \equiv t'_1 \pto t'_2$}
	Then $\varphi  \models_\preceq \psi$ does not hold, since a nonempty heap 
	modeling $b_1$ has to contain at least two memory cells.

\myfpar{$a_1 \equiv \blk(t'_1, t'_2)$}
	Then the entailment $\varphi  \models_\preceq \psi$ does not hold
	because a memory block of size $t'_2-t'_1$
	where the first memory cell stores the value $1$ 
	satisfies $\blk(t'_1, t'_2)$
	but does not satisfy $b_1 \equiv \hls{}(t_1, t_2; t_3)$
	where, by the inductive rule of $\hls{}$, 
	$t_1 \pto z - t_1$ and $2 \le z - t_1 \le t_3$.

\myfpar{$a_1 \equiv \hls{}(t'_1, t'_2; t'_3)$}
	Then the entailment problem seems easy to solve. 
	One may conjecture that 
	$C_\preceq \wedge \Pi: \hls{} (t'_1, t'_2; t'_3) \models \hls{}(t_1, t_2; t_3)$ 
	iff $C_\preceq \wedge \abs(\varphi) \models t'_3 \le t_3$, 
	which is \emph{not} the case as a matter of fact, 
	as illustrated by the following example. 
(Recall that, from (\ref{eq:start-end}) we have that $C_\preceq \wedge \abs(\varphi) \models t'_1 = t_1 \wedge t'_2 = t_2$.)

\begin{example}
Consider $x < y \wedge y -x  =  4: \hls{}(x, y; 3) \models \hls{}(x, y; 2)$. The entailment is valid, while we have $3 > 2$.
The reason behind this seemly counterintuitive fact is that when we unfold $ \hls{}(x, y; 3)$ to meet the constraint $y - x = 4$, it is impossible to have a memory chunk of size $3$. (Actually every memory chunk is of size $2$ during the unfolding.) 
\end{example}

We are going to show how to tackle this issue in the sequel.

\begin{definition}[Unfolding scheme of a predicate atom and effective upper bound]
Let $\varphi \equiv \Pi: \hls{}(t'_1, t'_2; t'_3)$ be an {\slah} formula and $s: \cV \rightarrow \NN$ be a stack such that $s \models \abs(\varphi)$ and $s(t'_2) - s(t'_1) \ge 2$. 
An \emph{unfolding scheme} of $\varphi$ w.r.t. $s$ is a sequence of numbers
$(sz_1, \cdots, sz_\ell)$ such that $2 \le sz_i \le s(t'_3)$ for every $i \in [\ell]$ and $s(t'_2) = s(t'_1) + \sum_{i \in [\ell]} sz_i$. 
Moreover, $\max(sz_1, \cdots, sz_\ell)$ is called the \emph{chunk size upper bound} associated with the unfolding scheme.
The \emph{effective upper bound} of $\varphi$ w.r.t. $s$, denoted by $\eub_\varphi(s)$, is defined as the maximum chunk size upper bound associated with the unfolding schemes of $\varphi$ w.r.t. $s$.
\end{definition}

\begin{example}
Let $\varphi \equiv x < y: \hls{}(x, y; 3)$ and $s$ be a store such that $s(x)= 1$ and $s(y) = 7$. Then there are two unfolding schemes of $\varphi$ w.r.t. $s$, namely, $(2, 2, 2)$ and $(3,3)$, whose chunk size upper bounds are $2$ and $3$ respectively. Therefore, $\eub_\varphi(s)$, the effective upper bound of $\varphi$ w.r.t. $s$, is $3$.
\end{example}

The following lemma (proved in the appendix) states that 
the effective upper bounds of chunks in heap lists atoms of $\varphi$ 
with respect to stacks %\mihaela{r3: not clear}
can be captured by a {\qfpa} formula.

\begin{lemma}\label{lem-eub}
For an {\slah} formula $\varphi \equiv \Pi: \hls{}(t'_1, t'_2; t'_3)$, a {\qfpa} formula $\xi_{eub,\varphi}(z)$ can be constructed in linear time such that for every store $s$ satisfying $s \models \abs(\varphi)$, we have $s[z \gets \eub_\varphi(s)] \models \xi_{eub,\varphi}(z)$ and $s[z \gets n] \not \models \xi_{eub,\varphi}(z)$ for all $n \neq \eub_\varphi(s)$.
\end{lemma}

The following lemma (proof in the appendix) provides the correct test used 
	for the case $a_1 \equiv \hls{}(t'_1, t'_2; t'_3)$.
%for deciding the ordered entailment $\Pi: \hls{}(t'_1, t'_2; t'_3) \models_\preceq  \hls{}(t_1, t_2; t_3)$ .
\begin{lemma}\label{lem-hls-hls}
Let $\varphi \equiv \Pi: \hls{}(t'_1, t'_2; t'_3)$, $\psi \equiv \hls{}(t_1, t_2; t_3)$, and $\preceq$ be a total preorder over $\addr(\varphi) \cup \addr(\psi)$ such that $C_\preceq \models t'_1 < t'_2 \wedge t'_1 = t_1 \wedge t'_2 = t_2$. 
Then $\varphi \models_\preceq \psi$ iff 
%$C_\preceq \wedge \abs(\varphi)  \models  \exists z.\ \xi_{eub, \varphi}(z) \wedge z \le t_3$.  
$C_\preceq \wedge \abs(\varphi)  \models  \forall z.\ \xi_{eub, \varphi}(z) \rightarrow z \le t_3$.  
\end{lemma}
From Lemma~\ref{lem-hls-hls}, it follows that $\varphi \equiv \Pi: \hls{}(t'_1, t'_2; t'_3) \models_\preceq \hls{}(t_1, t_2; t_3)$ is invalid iff $C_\preceq \wedge \abs(\varphi)  \wedge  \exists z.\ \xi_{eub, \varphi}(z) \wedge \neg z \le t_3$ is satisfiable, which is an {\EPbA} formula. Therefore, this special case of the ordered entailment problem is in coNP.

\medskip
%\vspace{-4mm}
\noindent {\bf At least two atoms in the antecedent:} Recall that 
$\varphi\equiv C_\preceq \wedge \Pi: a_1 \sepc \cdots \sepc a_m$; 
a case analysis on the form of the first atom of the antecedent, $a_1$, follows. 

\myfpar{$a_1\equiv \blk(t_1', t_2')$} 
	Then $\varphi \models_\preceq \hls{}(t_1, t_2; t_3)$ does not hold (see case $m=1$).

\myfpar{$a_1\equiv \hls{}(t_1', t_2'; t'_3)$}
	Then $\varphi \models_\preceq \hls{}(t_1, t_2; t_3)$ 
	 iff 
	 $\abs(\varphi): \hls{}(t'_1, t'_2; t'_3) \models_\preceq \hls{}(t_1, t'_2; t_3)$
	 and 
	 $\abs(\varphi): a_2 \sepc \cdots \sepc a_m \models_\preceq \hls{}(t'_2, t_2; t_3)$.

\myfpar{$a_1\equiv t'_1 \pto t'_2$}
	Then the analysis is more involved because we have to check that 
	$t'_2$ is indeed the size of the first chunk in $\hls{}(t_1, t_2; t_3)$
		(i.e., satisfies $2\leq t'_2\leq t_3$)
	and the address $t'_1+t'_2$, the end of the chunk starting at $t'_1=t_1$, 
        is the start of a heap list in the antecedent.
    The last condition leads to the following cases:
\begin{compactitem}
\item$t'_1+t'_2$ is the end of some $a_j$
	where $j \in [m]$ such that 
	$t'_1 + t'_2 = \atomtail(a_j) \wedge C_\preceq \wedge \abs(\varphi)$ is satisfiable.
	Then the following entailment shall hold: 
{\small
\begin{align*}
2 \leq t'_2\leq t_3 \land t'_1 + t'_2 = \atomtail(a_j) \land \abs(\varphi): 
   a_{j+1} \sepc \cdots \sepc a_m & \models_\preceq \hls{}(t'_1+t'_2, t_2; t_3).
\end{align*}
}
%does not hold, then $\varphi \models_\preceq \psi$ does  not hold.
% 
\item$t'_1+t'_2$ is inside a block atom $a_j$:
	where $j \in [m]$ such that $a_j\equiv \blk(t''_1, t''_2)$ and 
	$t''_1 < t'_1 + t'_2 < t''_2 \land \abs(\varphi)$ is satisfiable. 
	Then $\varphi \not\models_\preceq \psi$ because
	a block atom cannot match the head of a heap list in the consequent.
%the entailment 
%{\small
%\[
%\begin{array}{l}
%t''_1 < t'_1 + t'_2 < t''_2 \wedge \abs(\varphi): \blk(t'_1+t'_2, t''_2) \sepc a_{j+1} \sepc \cdots \sepc a_m \models_\preceq \hls{}(t'_1+t'_2, t_2; t_3)
%\end{array}
%\] 
%}
%does not hold.

\item$t'_1+t'_2$ is inside a heap-list atom $a_j$ 
	where $j \in [m]$ such that $a_j\equiv \hls{}(t''_1, t''_2; t''_3)$, 
	$t''_1 < t'_1 + t'_2 < t''_2 \land \abs(\varphi)$ is satisfiable.
	Then the following ordered entailment
	stating that the suffix of the antecedent starting at $t_1'+t_2'$ 
	      matches the tail of the consequent,
	shall hold: 
%$ \hls{}(t''_1, t''_2; t''_3)$ can be split into $\hls{}(t''_1, t'_1 + t'_2; t''_3) \sepc \hls{}(t'_1 + t'_2, t''_2; t''_3)$, 
\begin{align*}
2 \leq t'_2\leq t_3 \land t''_1 < t'_1 + t'_2 < t''_2 \land \abs(\varphi):~ & \\
	  \hls{}(t'_1 + t'_2, t''_2; t''_3) 
	  \sepc\ a_{j+1} \sepc \cdots \sepc a_m &
	  \models_\preceq \hls{}(t'_1+t'_2, t_2; t_3)
\end{align*}
%does not hold, then $\varphi \models_\preceq \psi$ does  not hold.
%
     and the following formula, 
     expressing that $t_1'+t_2'$ is inside a block of a chunk in $a_j$,
     shall be unsatisfiable (since otherwise the remaining suffix of the antecedent will start by a block atom and cannot match a heap list):
     %because the remainder of the block can not be matched by
     %the heap-list which is the tail of the consequent:
%\item 
%If there is $j \in [m]$ such that $a_j\equiv \hls{}(t''_1, t''_2; t''_3)$ and
\[\abs(
	 \begin{array}[t]{l} t''_1 \le  x' < t'_1 + t'_2 < x'' \le t''_2 \wedge 2 \le x'' - x' \le t''_3 \wedge C_\preceq \wedge \Pi : \\
	a_1 \sepc \cdots \sepc a_{j-1} \sepc {\sf Ufld}_{x', x''}(\hls{}(t''_1, t''_2; t''_3))\ \sepc a_{j+1} \sepc \cdots \sepc a_m)
	\end{array}
\]
%is satisfiable, 
      where $x', x''$ are two fresh variables and 
      the formula {\sf Ufld} specifies a splitting of $a_j$
      into a heap list from $t''_1$ to $x'$, a chunk starting at $x'$ and ending at $x''$,
      and a heap list starting at $x''$:
%\small
\begin{align*}
{\sf Ufld}_{x', x''}(\hls{}(t''_1, t''_2; t''_3)) \triangleq\  
           & \hls{}(t''_1, x'; t''_3) \sepc x' \pto x''- x' \sepc \\
           & \blk(x'+1, x'') \sepc \hls{}(x'', t''_2; t''_3),
\end{align*}
%then $\varphi \models_\preceq \psi$ does  not hold, since 
%      Indeed, 
%\[
%\begin{array}{l}
%t''_1 \le  x' < t'_1 + t'_2 < x'' \le t''_2 \wedge 2 \le x'' - x' \le t''_3 \wedge C_\preceq \wedge \Pi: \\
%\hfill \blk(t'_1+t'_2, x'') \sepc \hls{}(x'', t''_2; t''_3) \sepc a_{j+1} \sepc \cdots \sepc a_m \models \hls{}(t'_1+t'_2, t_2; t_3)
%\end{array}
%\] 
%does not hold.
%%
\end{compactitem}
Notice that all $j\in[m]$ shall be considered above; 
if one $j$ satisfying the premises does not lead to a valid conclusion
then the entailment is not valid.

%If $a_1 \equiv t'_1 \pto t'_2$ and none of the aforementioned situations occurs, then the entailment $\varphi \models_\preceq \psi$ holds.

%\begin{enumerate}
%\item If $m=1$, Equation~(\ref{eq:ma-hls-pto-f}) generates false 
%	because the entailment is invalid since the chunks of $\hls{}$
%	shall have at least size 2.
	
%\item If $m\ge 2$, then Equation~(\ref{eq:ma-hls-pto}) generates 
%	an entailment ensuring that $\Pi$ constraints 
%			$t_2'$ to satisfy the condition on the size of chunks in $b$.
%	Then, it considers all the cases where 
%	the block of the chunk started at $a_1$ 
%		ends among the atoms $a_j$ with $j\in [2,m]$
%	and calls recursively $\mathtt{matchAtom}'$.
%	In the recursive call, the pure constraint (first argument)
%	is updated with the constraint forcing
%	the end of the first chunk at the end of atom $a_j$.
%	Notice that only disjuncts where $a_{j+1}$ is either a points-to or a heap-list
%	atom will be different from \textit{false}.
%	To deal uniformly with the case $j=m$, we consider that $\mathtt{matchAtom}'$
%	called with an empty sequence returns \textit{true}.


\hide{
\item It is necessary that $t'_2$ denotes the size of the first unfolding of $\hls{}(t_1, t_2; t_3)$. Therefore, $\mathtt{matchAtom}$ checks whether $\abs(\varphi) \vDash c \le t'_2 \le t_3$, namely, the pure constraint in the recursive rule of $\hls{}$. If the answer is no, then the entailment does not hold. Otherwise, the procedure continues.

%
\item Because every unfolding of $\hls{}(t_1, t_2; t_3)$ starts with a points-to atom, we deduce that if the entailment holds, then the first unfolding of $\hls{}(t_1, t_2; t_3)$ ends at either $\atomtail(a_m)$, or at $\atomtail(a_k)$ for some $k < m$ satisfying that $a_{k+1}$ is a points-to or predicate atom. (Otherwise, $a_{k+1}$ is a block atom, then the points-to atom in the second unfolding of $\hls{}(t_1, t_2; t_3)$ would be matched to the first address in a block atom whose content is arbitrary, the entailment would not hold.) Let $S$ denote the set of such atoms. 
%
\item For each $a_k \in S$ such that $k < m$ and $\abs(\varphi) \wedge t'_1 + t'_2 = \atomtail(a_k)$ is satisfiable, we check whether $\abs(\varphi) \wedge t'_1 + t'_2 = \atomtail(a_k): a_{k+1} \sepc \cdots \sepc a_m \vDash \Pi': \hls{}(t'_1 + t'_2, t_2; t_3)$ holds. If the answer is no, then the entailment does not hold. (Note that if $\abs(\varphi) \wedge t'_1 + t'_2 = \atomtail(a_m)$ is satisfiable, then the entailment $\abs(\varphi) \wedge t'_1 + t'_2 = \atomtail(a_m): a_1 \sepc \cdots \sepc a_m \vDash \Pi': \hls{}(t_1, t_2; t_3)$ holds for sure.)
%
\item At last, if the procedure has not returned yet, report that the entailment holds.
}
%\end{enumerate}

%Another interesting case is $a_1\equiv \hls{}(t_1',t_2';t_3')$ and $m=1$
%in Equation~(\ref{eq:ma-hls-hls-1}).
%If $\Pi$ implies that the upper bound on the size $t_3'$ is less than $t_3$, 
%the entailment is clearly valid.
%If $\Pi$ implies that $t_3' > t_3$, we have to ensure that
%   $\Pi$ does not allow a chunk of $a_1$ to have a size bigger than $t_3$.
