%!TEX root = atva2021.tex

\section{Introduction}

%\vspace{-2mm}
\mypar{Context.}
Separation Logic (SL, \cite{Reynolds:2002,OHearn19}), an extension of Hoare logic, is a well-established formalism for the verification of heap manipulating programs. SL features a \emph{separating conjunction operator} and \emph{inductive predicates}, which allow to express how data structures are laid out in memory in an abstract way. Since its introduction, various verification tools based on separation logic have been developed. A notable one among them is the INFER tool \cite{CD11}, which was acquired by Facebook in 2013 and has been actively used in its development process \cite{CDD+15}. 

Decision procedures for separation logic formulas are vital for the automation of the verification process.
%
These decision procedures mostly focused on separation logic fragments called \emph{symbolic heaps} (SH)~\cite{BerdineCO04}, since they provide a good compromise between expressivity and tractability. 
%
The SH fragments comprise existentially quantified formulas that are 
conjunctions of atoms encoding 
aliasing constraints $x = y$ and $x \neq y$ between pointers $x$ and $y$, 
points-to constraints $x\pto v$ expressing that at the address $x$ is stored the value $v$, 
and predicate atoms $P(\vec{x})$ defining unbounded memory regions of a particular structure. The points-to and predicate atoms, also called spatial atoms,
are composed using the separating conjunction to specify the disjointness of memory blocks they specify.

Let us briefly summarize the results on the SH fragments in the sequel.
For the SH fragment with the singly linked list-segment predicate, arguably the simplest SH fragment, its satisfiability and entailment problems have been shown to be in PTIME~\cite{CookHOPW11} and efficient solvers have been developed for it~\cite{SLCOMPsite}.
%
The situation changes for more complex inductive predicates: 
The satisfiability problem for the SH fragment with a class of general inductive predicates was shown to be EXPTIME-complete~\cite{DBLP:conf/csl/BrotherstonFPG14}. On the other hand, the entailment problem for the SH fragment with slightly less general inductive predicates was shown to be 2-EXPTIME-complete~\cite{KatelaanMZ19,DBLP:conf/lpar/EchenimIP20}. 
%

\mypar{Motivation.}
Vast majority of the work on the verification of heap manipulating programs based on SL assumes that the addresses are \emph{nominal}, that is, they can be compared with only equality or disequality, but not ordered or obtained by arithmetic operations.
However, pointer arithmetic is widely used in low-level programs % , e.g. memory allocators.
to access data inside memory blocks. Memory allocators are such low-level programs. 
%Such programs usually use pointer variables to refer to the start addresses of memory regions and use arithmetic expressions on these variables to visit the memory cells inside these regions. 
They assume that the memory is organized into a linked list of memory chunks, 
called heap lists in this paper; pointer arithmetic is
%where arithmetic expressions are
used to jump from a memory chunk to the next one \cite{Knuth97,WJ+95}. 
%We call this data structure as heap lists in the sequel.
%
%One of the fundamental data structures used in these programs are heap lists
%
There have been some work to use separation logic for the static analysis and deductive verification of these low-level programs~\cite{CalcagnoDOHY06,MartiAY06,Chlipala11}. 
Moreover, researchers have also started to investigate the decision procedures for separation logic fragments containing pointer arithmetic. 
For instance, \emph{Array separation logic} (ASL) was proposed in~\cite{BrotherstonGK17}, which includes pointer arithmetic, the constraints $\blk(x,y)$ denoting a block of memory units from the address $x$ to $y$, as well as the points-to constraints $x\pto v$. It was shown in~\cite{BrotherstonGK17} that for ASL, the satisfiability is NP-complete and the entailment is in coNEXP resp. coNP for quantified resp. quantifier-free formulas.
Furthermore, the decidability can be preserved even if ASL is extended with the list-segment predicate~\cite{DBLP:journals/corr/abs-1802-05935}.
Very recently, Le identified in~\cite{DBLP:conf/vmcai/Le21} two fragments 
of ASL extended with a class of general inductive predicates for which 
the satisfiability (but not entailment) problem is decidable.

Nevertheless, none of the aforementioned work is capable of reasoning about heap lists, or generally speaking, pointer arithmetic inside inductive definitions, in a \emph{sound and complete} way. The state-of-the-art static analysis and verification tools, e.g. \cite{CalcagnoDOHY06,Chlipala11,MartiAY06}, resort to sound  (but incomplete) heuristics or interactive theorem provers, for reasoning about heap lists. 
On the other hand, the decision procedures for ASL or its extensions, e.g. \cite{BrotherstonGK17,DBLP:journals/corr/abs-1802-05935,DBLP:conf/vmcai/Le21}, are unable to tackle heap lists.
%\footnote{The fact that heap lists are beyond the two decidable fragments in~\cite{DBLP:conf/vmcai/Le21} was confirmed by a personal communication with the author of \cite{DBLP:conf/vmcai/Le21}.}.
This motivates us to raise the following research question: \emph{Can decision procedures be achieved for separation logic fragments allowing pointer arithmetic inside inductive definitions}?




%Separation logic~\cite{Reynolds:2002,OHearn19}  
%has a big impact on the verification or static analysis of programs
%manipulating the dynamic memory. Performant tools have been developed
%based on this formalism, among which we cite
%\textsc{Infer}~\cite{DBLP:journals/cacm/DistefanoFLO19} for static analysis
%and \textsc{VeriFast}~\cite{verifast} for deductive verification.\todo{MS: change tool}

%These tools employ the 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\hide{
The \emph{symbolic heap} (SH) fragment \todo{MS: change motivation}
of separation logic has been introduced in~\cite{BerdineCO04}
because it provides a good compromise between expressivity
and tractability. This fragment includes existentially quantified formulas
which are conjunctions of 
	aliasing constraints $x = y$ and $x \neq y$ between pointers $x$ and $y$,
and heap constraints $x\pto v$ expressing that at the address $x$ is allocated
    a memory zone containing the value $v$.
    The separating conjunction composes the heap constraints by ensuring 
    that the allocated memory blocks are disjoint.
To capture properties of heaps with unbounded data structures, the SH fragment is
extended with predicates that may be inductively defined using SH formulas or
	defined by the semantics, i.e., built-in.
The first category includes the predicate specifying acyclic singly linked list segments defined by the following two rules:
\begin{eqnarray}
\ls{}(x,y) & \Leftarrow & x = y : \emp \label{eq:ls-0} \\
\ls{}(x,y) & \Leftarrow & \exists x'\cdot x \neq y : x \pto x' \sepc \ls{}(x',y) \label{eq:ls-rec}
\end{eqnarray}
The satisfiability problem for the SH fragment with the $\ls{}$ predicate is in PTIME~\cite{CookHOPW11} and efficient solvers have been implemented for it~\cite{SLCOMPsite}.
This situation changes for more complex definitions of predicates, 
but still there are decidable fragments for which the satisfiability problem
is EXPTIME~\cite{DBLP:conf/lpar/EchenimIP20,DBLP:conf/lpar/KatelaanZ20}
% PSPACE~\cite{IosifRS13}.
in general and NP-complete if the arity of the predicate 
is bound by a known constant~\cite{DBLP:conf/csl/BrotherstonFPG14}.
%
In the second category of predicates, one which is of interest for this paper 
is the memory block predicate $\blk(x,y)$ introduced in \cite{CalcagnoDOHY06}
to capture properties of memory allocators. This predicate has been formalized
as part of the \emph{array separation logic} (ASL) 
	introduced in~\cite{BrotherstonGK17}.
The predicate denotes a block of memory units (bytes or integers) 
	between addresses $x$ and $y$. It is usually combined with
	pointer arithmetics allowing to compute addresses in the memory blocks.
This combination is enough to increase the complexity class of the
	decision problems in ASL: the satisfiability is NP-complete, 
	the bi-abduction is NP-hard and 
	the entailment is EXPTIME resp. coNP-complete for quantified resp. quantifier-free formulas~\cite{BrotherstonGK17}.
The ASL fragment extended with the $\ls{}$ predicate has been studied 
in~\cite{DBLP:journals/corr/abs-1802-05935}, and it is decidable for 
satisfiability and entailment.

Still, the ASL fragment or its extension with $\ls{}$ are not expressive enough 
to specify the heap list data structure that is defined by the following rules:
\begin{eqnarray}
\hls{}(x,y) & \Leftarrow & x = y : \emp \label{eq:hls-0} \\
\hls{}(x,y) & \Leftarrow & \exists x'\cdot x'-x \ge 2 : x \pto x'-x \sepc \blk(x+1,x') \sepc \hls{}(x',y) \label{eq:hls-rec}
\end{eqnarray}
From this definition, a heap list between addresses $x$ and $y$ is 
	either an empty block if $x$ and $y$ are aliased,
    or a list cell, called \emph{chunk}, starting at address $x$ and ending at
    address $x'$ followed by a heap-list from $x'$ to $y$.
    The chunk stores its size $x'-x$ at its start 
		(atom $x\pto x'-x$), that we also call \emph{chunk's header}.
	The \emph{chunk's body} is a memory block (atom $\blk(x+1,x')$) starting
		after the header and ending before the next heap-list's chunk.
Although this fragment has been used in the static analysis of memory
allocators~\cite{CalcagnoDOHY06} as well as deductive verification~\cite{Chlipala11,MartiAY06}, its decidability
has not been studied because these tools either employed sound heuristics or interactive theorem provers, but not decision procedures.
A special instance where all chunks have the same size
has been used in the deductive verification of 
	an implementation of the array list collection~\cite{CauderlierS18},
but required an interactive prover \textsc{VeriFast} and user-provided
lemma. 
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\mypar{Contribution.}
In this work, we propose decision procedures for a fragment of separation logic called {\slah}, which allows pointer arithmetic inside inductive definitions, 
so that inductive predicates specifying heap lists can be defined. 
We consider both satisfiability and entailment problems and show that they are NP-complete and coNP-complete respectively. 
The decision procedure for satisfiability is obtained by computing an equi-satisfiable abstraction in Presburger arithmetic, whose crux is to show that the summaries of the heap list predicates, which are naturally formalized as existentially quantified non-linear arithmetic formulas, can actually be transformed into Presburger arithmetic formulas. 
The decision procedure for entailment, on the other hand, reduces the problem to multiple instances of an ordered entailment problem, where all the address terms of spatial atoms are ordered. 
The ordered entailment problem is then decided by matching each spatial atom in the consequent to some spatial formula obtained from the antecedent by partitioning and splitting the spatial atoms according to the arithmetic relations between address variables.
Splitting a spatial atom into multiple ones in the antecedent is attributed to pointer arithmetic and unnecessary for SH fragments with nominal addresses. 
%Moreover, the availability of $\hls{}$ makes the splitting more involved.

We implemented the decision procedures on top of \cspen\ solver~\cite{GuCW16}.   
We evaluate the performance of the new solver, called {\cspenp}~\cite{CompSpenSite}, 
on a set of formulas originating from path conditions and verification conditions of programs working on heap lists in memory allocators. 
We also randomly generate some formulas, in order to test the scalability of {\cspenp} further.
The experimental results show that {\cspenp} is able to solve the satisfiability and entailment problems for {\slah} efficiently (in average, less than 1 second for satisfiability and less than 15 seconds for entailment).

To the best of our knowledge, this work presents the first decision procedure and automated solver for decision problems in a separation logic fragment allowing both pointer arithmetic and memory blocks inside inductive definitions. 
%As mentioned before, the logic ASL was introduced in~\cite{BrotherstonGK17}, but it contains no inductive predicates.
%As mentioned before, an extension of ASL with the classic singly linked list $\ls{}$ was considered in~\cite{DBLP:journals/corr/abs-1802-05935} 
%	where the only inductive predicate is the classic singly linked list $\ls{}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\hide{
The first part of our contributions is a theoretical study 
of the decision problems for ASL extended with 
the $\hls{}$ predicate. 
%We consider several
%versions of this predicate that lead to decidability of satisfiability and entailment problems. 
We propose decision procedures based on abstractions in Presburger arithmetic.
The second part of the contribution is experimental.
We implemented the decision procedures in a solver and
we applied it on a set of formulas originating from 
	path conditions and verification conditions of programs working on heap-lists. 
We also push the solver in the corner cases of our decision procedures
by providing randomly generated problems for each case.
This study is original to our knowledge, although the separation logic
	with heap-list has been used in 
	static analysis~\cite{CalcagnoDOHY06}
	and deductive verification~\cite{Chlipala11,MartiAY06}
	of memory allocators. Indeed, such work uses sound procedures 
	or interactive theorem provers and does not consider the decision problem.
The logic ASL has been introduced in~\cite{BrotherstonGK17} 
	where no inductively defined predicate is used.
An extension of ASL called SLA is considered 
	in~\cite{DBLP:journals/corr/abs-1802-05935} 
	where the only inductive predicate is the classic singly linked list $\ls{}$.
This year, \cite{DBLP:conf/vmcai/Le21} proposed a decision procedure dealing
    with satisfiability of ASL extended with inductively defined predicates. 
    The idea is to compute a so-called base formula for predicate atoms 
    that is enough for deciding satisfiability. 
Our class of heap-list predicates is a strict sub-class of inductive definitions
	considered in \cite{DBLP:conf/vmcai/Le21}, but we are able to compute an exact
    summary of the predicate atoms which is used in both satisfiability and
    entailment decision procedures.
%Our work extends ASL and demonstrates that adding the $\hls{}$ predicate requires 
%	a special translation
%	to obtain a linear constraint for the summary of $\hls{}$ atoms
%	and to match such atoms for deciding the entailment.
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\mypar{Organization.}
%The remainder of this paper is structured as follows. 
A motivating example and an overview of the decision procedures are provided in Section~\ref{sec:over-hls}.
The logic {\slah} is defined in Section~\ref{sec:logic-hls}.
%
Then the decision procedures for the satisfiability and entailment problems are presented in Sections~\ref{sec:sat-hls} \resp~\ref{sec:ent}.
%
%Section~\ref{sec:sat-wfid} extends the satisfiability results to \slah\ with
%	well-formed inductive definitions of heap-lists.
%
The implementation details and the experimental evaluation
	are reported in Section~\ref{sec:exp-hls}.
%Finally, the conclusion is given in Section~\ref{sec:conc-hls}.
%by a comparison of our work with existing fragments and its impact on verification of memory allocators.

