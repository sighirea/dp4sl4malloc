%!TEX root = main-hls.tex
\section{Extension with Heap-lists}
\label{sec:sla-hls}

To specify the properties of heap lists, we extend the symbolic heap fragment of \sla\
with the predicate describing heap lists and we denote this fragment \slhl. 
%
We introduce several versions of this predicate:\mihaela{To fix the number}{} 
   with the address of the successor cell stored in the chunk's header,
   with the size of the chunk stored in chunk's header,
   and with the status of the chunk (for early collapsing heap lists).
%
For each version, 
we investigate the decidability of satisfiability and entailment problems.
For readability, we present the results gradually, for several simple sub-fragments of \slhl.

\subsection{Syntax and semantics of \slhl}

The novelty is the introduction of two new generic predicates:
	a predicate to specify heap chunks
and a predicate for specifying heap lists.
The definition of the second predicate is inductive and uses the first predicate.

\begin{definition}[Syntax of \slhl]\label{def:syn-hls}
Terms $t$, pure formulas $\Pi$, spatial formulas $\Sigma$ and symbolic heaps $\phi$
of $\slhl$ are given by the following grammar:
\begin{eqnarray}
t & ::= & x \mid n \mid t+t \label{eq:syn-SLA-term} %\mid nt
\\[1mm]
\Pi & ::= & t=t \mid t \ne t \mid t \leq t \mid t < t \mid \Pi \land \Pi
\\[1mm]
\Sigma & ::= & \emp \mid t \pto t \mid \blk(t,t) \mid \hck(x,\vec{z}) \mid \hls{}(x,\vec{z};y,\vec{u};\vec{v}) \mid \Sigma \sepc \Sigma
\end{eqnarray}
\noindent
where 
$x$ ranges over a set $\mathcal{V}$ of variables, 
$\vec{z},\vec{u},\vec{v}$ over vectors of variables, 
and $n$ over $\NN$. 
%
\mihaela{This part repeats \sla.}
For convenience, we suppose that $\mathcal{V}$ includes a variable $nil$.
\end{definition}
%
The predicate $\hck(x,\vec{z})$ specifies a chunk built from a header and a payload.
For the moment, we let it unspecified and we define its various versions in the  next sections.
%
The predicate $\hls{}(x,\vec{z};y,\vec{u};\vec{v})$ specifies a heap list and
we define it inductively using the $\hck$ predicate.
The $\hls{}$ predicate has also several versions, every version being defined
by a linear inductive definition.

\begin{definition}[Linear inductive definitions in $\slhl$]\label{def:lid-hls}
A linear  inductive definition of a predicate $P$ in $\mathcal{P}^H$ 
has \emph{only one base rule} of the form:
\begin{eqnarray}
P(x,\vec{z};y,\vec{u};\vec{v}) & \Leftarrow & (x=y \land \vec{z}=\vec{u} : \emp) \label{eq:ID-emp}
\end{eqnarray}
and \emph{at least one recursive rule} of the form:
\begin{eqnarray}
P(x,\vec{z};y,\vec{u};\vec{v}) & \Leftarrow &
\big(\exists \vec{w},\pvec{z}'\cdot \Pi(x,\vec{z},w,\pvec{z}',\vec{v}) :
\hck(x,\vec{w}) \sepc P(\vec{w}_0,\pvec{z}';y,\vec{u};\vec{v})
\big)\quad \label{eq:ID-rec}
\end{eqnarray}
where $|\vec{z}|=|\pvec{z}'|=|\vec{u}|\ge 0$, and 
$\vec{w}_0$ is the first component of $\vec{w}$.
The pure constraints $\Pi$ in each recursive rule shall be satisfiable.
The parameters $x$ and $y$ are called \emph{start} resp. \emph{end} parameters
    because they delimit the heap segment defined by the predicate;
    notice that, the semantics constrains the start location to be inside
    the heap and end location to be outside the heap.
The parameters in $\vec{z}$ are called \emph{start update} 
	because they are updated at the recursive call.
The parameters $\vec{u}$ are \emph{end glue} 
	because they are not changed in the recursive call but are constrained by the base rule.
The parameters $\vec{v}$ are called static parameters 
	because they stay the same in the recursive call and are unconstrained in the base rule. 
\end{definition}

Although we do not require non aliasing of start and ending parameters in
the inductive rule of linear definitions, this aliasing is implicit.
Indeed, the successor of a cell in a heap list is specified in the chunk header
and its is usually obtained by pointer arithmetics using a strictly positive value.
Therefore, the heap-list segments are only linear blocks of values, 
they can not include loops (lassos).


\begin{definition}[Generic semantics]\label{def:sem-hls}
The satisfaction relation $I,h\models A$ with $I$ an interpretation of free variables in $A$ and $h$ a heap, is defined by extending the satisfaction relation in \sla\ by the following rules:
\begin{eqnarray*}
I,h\models \hck(x, \vec{w}) & \mbox{ iff } & 
	I,h\models \blk(x;\vec{w}_0)
\\
I,h\models P(x,\vec{z};y,\vec{u};\vec{v}) & \mbox{ iff } & 
	\exists k\in\NN\cdot I,h\models P^k(x,\vec{z};y,\vec{u};\vec{v})
\\
I,h\models P^0(x,\vec{z};y,\vec{u};\vec{v}) & \mbox{ iff } & 
    I,h\models (x=y \land \vec{z}=\vec{u} : \emp)
\\
I,h\models P^{k+1}(x,\vec{z};y,\vec{u};\vec{v}) & \mbox{ iff } & 
	\mbox{there exists a recursive rule } \\
& & P(x,\vec{z};y,\vec{u};\vec{v})\Leftarrow \exists w, \pvec{z}'\cdot \varphi(P(w,\pvec{z}';y,\vec{u};\vec{v}))\mbox{ s.t. } \\
& & I,h\models \exists w, \pvec{z}'\cdot \varphi(P^k(w,\pvec{z}';y,\vec{u};\vec{v}))
\end{eqnarray*}
\end{definition}
\zhilin{although evident, the interpretation of existential quantifiers is not defined}
\mihaela{No changes from the one given for \sla.}
 
The semantics of $\hck$ has to be refined by its several instances
described in the remaining of this section.


\subsection{Overview of variants and fragments of \slhl}

Table~\ref{tab:frg} provides the list of fragments we consider for \slhl\
and their main features.
Given a set of predicate names $A$ ($\pto$ and $\blk$ included), 
the fragment $\slhl(A)$ contains formulas whose spatial part is built
only from atoms using the predicate symbols in $A$.
Notice that $\emp$ is always included in the sets $A$.
%
In particular, the fragment $\sla$ is also the fragment $\slhl(\pto,\blk)$.
The fragment $\slhl(\hls{a},\pto)$ contains formula built with spatial atoms 
$t\pto u$
and $\hls{a}(x;y)$ where $\hls{a}$ is the instance of $\hls{}$
calling predicate $\hck^a$ defined in section~\ref{ssec:hcka}.

\begin{table}[t]
\caption{Fragments of $\slhlsf$}
\label{tab:frg}
\begin{center}
\begin{tabular}{l|c|c|c|c|l|l}
\textit{Fragment} & $\pto$ & $\blk$ & $\hck$ & $\hls{}$ 
				  & \textit{Sat} & \textit{Ent}
\\\hline\hline
\sla              & \checkmark & \checkmark &  &  
			      & \ref{ssec:sla-sat} & \ref{ssec:sla-ent}
\\\hline			      
$\slhl(\hls{a?})$  &   &   & $\hck^{a?}$ & $\hls{a?}$
			      & \multicolumn{2}{|c}{\ref{ssec:dp-hls-a}}
\\\hline			      
$\slhl(\hls{s?})$  &   &   & $\hck^{s?}$ & $\hls{s}$
			      & \multicolumn{2}{|c}{\ref{ssec:dp-hls-s}}
\\\hline			      
$\slhl(\hls{x},
      \pto)$      & \checkmark &    & $\hck^x$ & $\hls{x}$  
			      & \multicolumn{2}{|c}{\ref{sec:dp-hls-pto}}
\\\hline			      
\end{tabular}
\end{center}
\end{table}%



\subsection{Heap chunk with address field}
\label{ssec:hcka}

The following definition of $\hck$ captures the case of chunk headers 
storing the address of the next chunk in the list:
\begin{equation}
\hck^a(x,y,st) \triangleq 
		x+2 < y ~:~ %\land 0\le st \le 1
		x\pto y \sepc x+1\pto st \sepc \blk(x+2;y)
\label{eq:hcka}
\end{equation}

The semantics of the $\hck^a$, $I,h\models \hck^a(x,y,st)$ is defined by:
\begin{eqnarray*}
I,h & \models & x+2 < y %\land 0\le st \le 1
				~:~ x\pto y \sepc x+1\pto st \sepc \blk(x+2;y)
\end{eqnarray*}

The inductive definition of the heap list predicate for this kind of chunk 
is given by the following two rules:
\begin{eqnarray}
\hls{a}(x;y) & \Leftarrow & (x=y : \emp)  \\
\hls{a}(x;y) & \Leftarrow & \big(\exists w,f\cdot \hck^a(x,w,f) \sepc \hls{a}(w;y)\big)\label{eqn:hlsa}
\end{eqnarray}

The following version of the heap list predicate based 
on the $\hck^a$ uses the status field to avoid adjacent free chunks:
\begin{eqnarray}
\hls{ac}(x,st;y,st') & \Leftarrow & (x=y \land st=st' : \emp)  \\
\hls{ac}(x,st;y,st') & \Leftarrow & \big(\exists w,f\cdot st \neq f ~:~ \hck^a(x,w,f) \sepc \hls{ac}(w,f;y,st')\big)
\end{eqnarray}

The next version of $\hls{}$ allows adjacent free chunks, which is the case
for allocators with lazy coalescing of free chunks:
\begin{eqnarray}
\hls{al}(x,st;y,st') & \Leftarrow & (x=y \land st=st' : \emp)  \\
\hls{al}(x,st;y,st') & \Leftarrow & \big(\exists w,f\cdot \hck^a(x;w;f) \sepc \hls{al}(w,f;y,st')\big)
\end{eqnarray}


\subsection{Heap chunk with size field}
\label{ssec:hcks}

The following definition of $\hck^s$ captures the case of chunk headers 
storing the size of the chunk:
\begin{equation}
\hck^s(x,y,st,sz) \triangleq \begin{array}[t]{l}
 sz > 3 \land y=x+sz ~:~ % \land 0\le st \le 1
 x\pto sz \sepc x+1\pto st \sepc \blk(x,3,sz)
 \end{array}\label{eq:hcks}
\end{equation}

%
The semantics of the $\hck^s$, $I,h\models \hck^s(x,y,st,sz)$ is defined by:
\begin{eqnarray}
I,h &\models & \exists sz,st,nx\cdot\begin{array}[t]{l}
 		sz > 3 \land y=x+sz ~:~ %\land 0\le st \le 1
 		x\pto sz \sepc x+1\pto st \\
		\sepc\ \blk(x,3,sz)
 		\end{array}
\end{eqnarray}

The following three versions of the heap list predicate employ 
the atom $\hck^s$ to define respectively 
heap list without constraints of chunk status, 
heap lists for free chunk coalescing and 
heap lists with the free status exposed:
\begin{eqnarray}
\hls{s}(x;y) & \Leftarrow & (x=y : \emp)  \\
\hls{s}(x;y) & \Leftarrow & \big(\exists w,st,sz\cdot \hck^s(x,w,st,sz) \sepc \hls{s}(w;y)\big)
\\[2mm]
\hls{sc}(x,st;y,st') & \Leftarrow & (x=y \land st=st' : \emp)  \\
\hls{sc}(x,st;y,st') & \Leftarrow & \big(\exists w,f,sz\cdot st \neq f : \hck^s(x,w,f,sz) \sepc \hls{sc}(w,f;y,st')\big)~~~~~~
\\[2mm]
\hls{sl}(x,st;y,st') & \Leftarrow & (x=y \land st=st' : \emp)  \\
\hls{sl}(x,st;y,st') & \Leftarrow & \big(\exists w,f,sz\cdot \hck^s(x,w,f,sz) \sepc \hls{sc}(w,f;y,st')\big)
\end{eqnarray}


\subsection{Well-formed inductive definitions for heap lists}

The inductive definitions of $\hls{*}$ predicates given above 
belong to a class of definitions that we call 
\emph{well-formed}.\mihaela{Not sure that this is useful here}

For readability, we refer to the data stored in the header of chunks 
using terms whose syntax is:
\begin{eqnarray}
t & ::= & x \mid n \mid \fsz(x) \mid \fif(x) \mid t+t \label{eq:syn-SLA-term} %\mid nt
%
\end{eqnarray}
A term $t$ of the form $\fsz(x)$ denotes the size of the such 
and it is interpreted as 
$\sem \fsz(x) \antic_{I,h} = h(I(x))$ if $I(x)\in\wpos{h}$ 
or undefined otherwise.
Similarly, $\fif(x)$ denotes the status of the chunk
and it is interpreted as
$\sem \fif(x) \antic_{I,h} = h(I(x)+1)$ if $I(x),I(x)+1\in\wpos{h}$
or undefined otherwise.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\hide{
\begin{definition}[Well-formed linear inductive definitions in \slhl]\label{def:LID-hls-WF}
A linear inductive definition of a predicate $P$ in $\mathcal{F}$
is \emph{well-formed} in \slhl\ iff every recursive rule
$P(x,\vec{z};y,\vec{u};\vec{v})\Leftarrow \exists w,\pvec{z}'\cdot \Pi(x,\vec{z},w,\vec{z}',\vec{v}) : \hck(x;w)\sepc\ P(w,\pvec{z}';y,\vec{u};\vec{v})$
employs a restricted form of pure constraints:\todo{Not checked!}
\begin{eqnarray*}
\Pi(x,\vec{z},w,\pvec{z}',\vec{v}) & ::= &
\bigwedge_i \Omega_i(\fsz(x),\vec{v}_i)
\land \bigwedge_{\ell,k\in\{\fif(x)\}\cup\vec{z}\cup\pvec{z}'} \Delta(\ell,k)
\\
\Omega(x,y) & ::= & \textit{true} \mid x < y \mid x > y
\\
\Delta(x,y) & ::= & \textit{true} \mid x-y \sim n
\end{eqnarray*}
where $n \in \NN$, $\sim\in\{=,\ne,<,\le,\ge,>\}$.
\end{definition}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{definition}[Well-formed linear inductive definitions in \slhl]\label{def:LID-hls-WF}
A linear inductive definition of a predicate
$P(x,\vec{z};y,\vec{u};\vec{v})$ in $\mathcal{P}^H$
is \emph{well-formed} in \slhl\ iff
there exists $i_P\in\NN$ such that
for every recursive rule
$P(x,\vec{z};y,\vec{u};\vec{v})
	\Leftarrow \exists w,\pvec{z}'\cdot
					\Pi(x,\vec{z},w,\pvec{z}',\vec{v}) :
	                \hck(x;w)\sepc P(w,\pvec{z}';y,\vec{u};\vec{v})$
where $|\vec{z}|>0$ then $0\leq i_P \leq |\vec{z}|$
and the pure constraints of the rule satisfy the following syntactic constraints:
\begin{eqnarray}
\Pi(x,\vec{z},w,\pvec{z}',\vec{v}) & ::= &
	\Pi_{\fsz}(x,\vec{v}) \land
	\Pi_{\fif}(x,\vec{z}_{i_P},\pvec{z}'_{i_P}) \land
	\bigwedge_{j\neq i_P}\Pi_{\mathtt{oth}}(\vec{z}_j,\pvec{z}'_j)
\end{eqnarray}
The component $\Pi_{\fsz}(x,\vec{v})$ is defined by the following rules which allows only to compare $\fsz(x)$ with static parameters in $\vec{v}$:
%
\begin{eqnarray}
\Pi_{\fsz} & ::= & \textit{true} \mid
	\vec{v}_j^\bot \le \fsz(x)  \le \vec{v}_i^\top \mid
	\Pi_{\fsz} \land \Pi_{\fsz}
\end{eqnarray}
%
where $\vec{v}_j$ and $\vec{v}_i$ are components of $\vec{v}$ and
$v^\bot$ (resp. $v^\top$) means $v$ or $-\infty$ (resp. $\infty$).
%% \mihaela{How is summarized $\neq$?}
%% using disjunction

The component $\Pi_{\fif}(x,\vec{z}_{i_P},\pvec{z}'_{i_P})$ allows to compare $\fif(x)$ to $i_P$-th source resp. destination parameters:
%
\begin{eqnarray}
\Pi_{\fif} & ::= & \textit{true} \mid
	\fif(x)  \sim' \vec{z}_{i_P} \land \fif(x)  \sim'' \pvec{z}'_{i_P}
\end{eqnarray}
for some $i_P$ and $\sim', \sim'' \in \{=,\ne\}$.
Finally, $\Pi_{\mathtt{oth}}$ limits the constraints on data parameters to difference constraints between parameters with the same position in $\vec{z}$ and $\pvec{z}'$, excepting the position $i_P$ if $i_P\neq 0$:
%
\begin{eqnarray}
\Pi_{\mathtt{oth}}(\vec{z}_j, \pvec{z}'_j) & ::= & \textit{true} \mid
	\vec{z}_j - \pvec{z}'_j \sim''' n \mid
	\Pi_{\mathtt{oth}} \land \Pi_{\mathtt{oth}}
\end{eqnarray}
%
where %$j \neq i_P$,
$n \in \NN$ and $\sim''' \in \{\le, \ge\}$.
% intuitively, $\Pi_{\mathtt{oth}}$ specifies the constraints on the source resp. destination parameters in the positions other than $i_P$,
% moreover, these parameters are independent in the sense that the parameters in different positions cannot be compared
% In addition, we require that the indices $i_P$ in all recursive rules (if there is any) are the same.
\end{definition}

We denote by $\mathcal{R}(P)$ the set of recursive rules of $P$.
For a recursive rule $R$, we denote by
	$\Pi^{(R)}(x,\vec{z};y,\vec{u};\vec{v})$
the formula obtained from the pure formula $\Pi(x,\vec{z},w,\pvec{z}',\vec{v})$ in $R$ by simultaneously replacing $w$ with $y$ and $\pvec{z}'$ with $\vec{u}$.
Similarly, we denote by
	$\Pi^{(R)}_\fsz(x,\vec{z};y,\vec{u};\vec{v})$,
	$\Pi^{(R)}_\fif(x,\vec{z};y,\vec{u};\vec{v})$ and
	$\Pi^{(R)}_{\mathtt{oth}}(x,\vec{z};y,\vec{u};\vec{v})$
the three sub-formulas of $\Pi^{(R)}(x,\vec{z};y,\vec{u};\vec{v})$.

For a well-formed definition $P$, we classify its recursive rules into three types, according to the syntax of $\Pi_\fif$, as follows.
%%MS: avoid emp, it is not clear what is the name.
A recursive rule is of $\fif$-t type if $\Pi_\fif=\textit{true}$.
It is of $\fif$-e type (resp. $\fif$-i type) if in $\Pi_\fif$, $\sim' = \sim''$ (resp. $\sim' \neq \sim''$).
We use $\mathcal{R}_t(P)$ (resp. $\mathcal{R}_e(P), \mathcal{R}_i(P)$) to denote the set of recursive rules of $\fif$-t type (resp. $\fif$-e type, $\fif$-i type).
%
Similarly, we classify recursive rules into two four (non disjoint) types
as follows:
	$\mathcal{R}_{lb}(P)$ (resp. $\mathcal{R}_{ub}(P)$) are rules where $\Pi_\fsz$ contains a constraint $\vec{v}_i \le \fsz(x)$ (resp. $\fsz(x) \ge \vec{v}_i$);
	$\mathcal{R}_{\bot}(P)$ (resp. $\mathcal{R}_{\top}(P)$) are rules where $\Pi_\fsz$ does not constraint the lower (upper) value of $\fsz(x)$.

The constraints for well-formedness of definitions in $\mathcal{P}^H$
have been selected to obtain a tradeoff between the expressivity of the predicates to capture heap-list properties and ability to apply
the technique employed by the decision procedure.
%%\mihaela{I.e., the decidable fragment is bigger, but we only look at this one?}
%Note that we have deliberately choose the aforementioned constraints on $\Pi(x,\vec{z},w,\vec{z}',\vec{v})$ in Definition~\ref{def:LID-hls-WF} to specify the properties of heap-lists on the one hand and facilitate the decision procedures of the resulting fragment of SLID on the other hand.
%\end{remark}
All the inductive definitions of $\hls{}$ given in the previous sus-sections are well formed.
Moreover, the following ones are also well-formed:
\begin{itemize}
\item \emph{The heap list is split according the first-fit policy by the request of size $r$.}
We define a predicate $\hls{b}$ with a static integer parameter that corresponds to the bound to be put on the size of chunks. Its recursive rule is:
\begin{eqnarray}
%% \hls{b}(x;y;b) & \Leftarrow & (x=y : \emp) \\
\hls{b}(x;y;b) & \Leftarrow & \big(\exists w \cdot \fsz(x) < b : \hck^*(x,w,\dots) \sepc\ \hls{b}(w;y;b) \big)
\end{eqnarray}

\item \emph{The length of a heap list} requires to introduce data parameters to collect the length of segments before the start and the end of the list segment:
\begin{eqnarray}
\hlsl(x, z; y, u) & \Leftarrow & (x=y \wedge z = u : \emp) \\
\nonumber
\hlsl(x, z; y, u) & \Leftarrow & \big(\exists w, z' \cdot z = z'+1: \hck^*(x,w,\dots) \sepc\ \hlsl(w, z'; y, u) \big)
\end{eqnarray}
\end{itemize}

