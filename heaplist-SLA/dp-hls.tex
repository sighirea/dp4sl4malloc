%!TEX root = main-hls.tex

\section{Decision Procedures for $\slhl(\hls{*})$}
\label{sec:slhl-hls}

We show in this section that the fragments of \slhl\ using only the $\hls{}$ atoms in spatial formulas has a decidable satisfiability problem, and this for any well-formed linear definitions of $\hls{}$.
%
The result is based on the observation that 
any heap list atom $\hls{}(x,\vec{z};y,\vec{u};\vec{v})$
specifies a possibly empty memory block $\blk(x,y)$.
The additional parameters $\vec{z}$, $\vec{u}$ and $\vec{v}$
constrain the way in which the heap list atoms are composed
to form another heap list segment, i.e., 
to satisfy the composition lemmata (\ref{lemma:blk}) (page~\pageref{lemma:blk}).
For example, the composition lemmata for $\hls{ac}$ becomes:
\begin{equation}\label{lemma:hlsac}
\hls{ac}(x,st;y;st') \sepc \hls{ac}(y, st'; z, st''') \limp \hls{ac}(x, st; z, st''')
\end{equation}
We use this observation to apply the procedure 
proposed in~\cite{DBLP:journals/corr/abs-1802-05935},
sketched in Section~\ref{ssec:sla-ent},
to obtain decidability of both satisfiability and entailments
problems for fragments $\slhl(\hls{*})$.


\subsection{Decision Procedures for $\slhl(\hls{a})$}
\label{ssec:dp-hls-a}

First, we prove two properties of the $\hls{a}$ predicate.

\begin{property}[$\hls{a}$ entails $\blk$]
For any $x$ and $y$, 
$x + c < y : \hls{a}(x;y) \limp \blk(x,y)$,
where c is the size of the chunk header used in $\hls{a}$.
\end{property}

\begin{proof}
The proof proceeds by induction on the size of the heap.  
The pure condition $x + c < y$ implies that the heap list is not empty
(since $\emp \not\limp \blk(x,y)$). The atom $\hls{a}(x;y)$ is replaced
by the formula given in its recursive rule.
By applying the inductive rule (\ref{eqn:hlsa}) of $\hls{a}$ and 
the definition of $\hck^a$ in (\ref{eq:hcka}),
the entailment to be proved valid becomes:
$$\exists w, f\cdot x+c < y \land x+c < w : x\pto w \sepc x+1\pto f \sepc \blk(x+c,w) 
  \sepc \hls{a}(w;y) \limp \blk(x,y)$$
If $w = y$, $\hls{a}(w;y)$ is $\emp$ and the antecedent entails
$x+c < y : \blk(x,y)$ which trivially entails the consequent.
Otherwise, $w + c < y$ (to satisfy $\hls{a}(w;y)$);
using the induction hypothesis, the antecedent entails
$x+c < w < w+c < y : \blk(x,w) \sepc \blk(w,y)$ which entails the consequent
by applying the composition lemmata.
\end{proof}

\begin{property}
For any $x$, $y$ and $z$, 
$\hls{a}(x;y) \sepc \hls{a}(y;z) \limp \hls{a}(x;z)$.
\end{property}

\begin{proof}
By properties of well-formed ID.
\end{proof}

\begin{proposition}
The satisfiability of a formula in $\slhl(\hls{a})$ is decidable.
\end{proposition}

\begin{proof}
The procedure employs the idea in~\cite{DBLP:journals/corr/abs-1802-05935}:
an $\varphi$ formula is translated into an equi-satisfiable formula
in Presbuger by considering all the ordering of spatial atoms.
Therefore, for 
$\varphi \equiv \Pi : \Sigma$\mihaela{$\exists$ not needed for sat}, 
we have the following equivalence:
$$\models\varphi 
\Leftrightarrow 
	\models\bigvee_{\Sigma_i\in\textit{Perm}(\Sigma)} \Pi : \Sigma_i 
\Leftrightarrow 
	\models\bigvee_{\Sigma_i\in\textit{Perm}(\Sigma)} \Pi \land \textit{Order}(\Sigma_i)
$$
where the $\textit{Order}$ is defined as follows:
%in Table~\ref{tab:SLHLA-order}.
%
%\begin{table}[ht]
%\caption{Total ordering of spatial atoms in $\slhl(\hls{a})$}
\begin{eqnarray}
\textit{Order}(lb,\emp) 
	& \triangleq & \textit{true} 
\\
\textit{Order}(lb,\hls{a}(x;y)) 
	& \triangleq & lb \le x \land (x = y \lor x+c < y) 
\\
\textit{Order}(lb,\emp \sepc \Sigma) 
	& \triangleq & \textit{Order}(lb,\Sigma)
\\
\textit{Order}(lb,\hls{a}(x;y) \sepc \Sigma) 
	& \triangleq & lb \le x \land (x = y \lor x+c < y)  \land \textit{Order}(y, \Sigma)
\end{eqnarray}
%\label{tab:SLHLA-order}
%\end{table}%
The proof of this equivalence is based on the semantics of
the $\hls{a}$ predicate which defines a block of memory where heap chunks are
continuous between addresses $x$ (included) and $y$ (excluded).

The same property of $\hls{a}$ leads to an alternative procedure
which uses the procedure for $\sla$ 
by replacing the atoms $\hls{a}(x;y)$ 
either by $x = y : \emp$ (for empty list)
or by $x+c < y:\hck^a(x,y,z)$ (for one chunk in the list, $z$ fresh).
The intuition is that heap-lists of more that one chunk are 
abstracted in a chunk where the payload block covers the rest of the list.
%
Let denote by $\textit{Unfold}(\Sigma)$ the set of $\sla$ formulas obtained 
by unfolding zero or once each $\hls{a}$ atom in $\Sigma$.
For $\varphi \equiv \Pi : \Sigma$,
we have the following equivalence:
$$\models\varphi 
\Leftrightarrow 
	\models\bigvee_{\Pi_i:\Sigma_i\in\textit{Unfold}(\Sigma)} \Pi \land \Pi_i : \Sigma_i 
$$
where each formula $\Pi \land \Pi_i : \Sigma_i$ is an $\sla$ formula,
for which the satisfiability is decidable. 
\end{proof}

\begin{proposition}
The entailment problem in QF of $\slhl(\hls{a})$, i.e.,
$\varphi \limp \lor_{i\in I} \psi_i$ 
where $\varphi, \psi_i$ are QF formulas in $\slhl(\hls{a})$,
is decidable.
\end{proposition}

\begin{proof}
Like in~\cite{DBLP:journals/corr/abs-1802-05935}, 
we define a translation $P$ of entailment problem in $\slhl(\hls{a})$
into a Presburger formula. The principle is similar: 
for each permutation $\varphi_i\in\textit{Perm}(\varphi)$,
we search a formula in $\lor_{\psi'_j\in \textit{Perm}(\psi_i), i\in I} \psi'_j$
that matches the spatial atoms in $\varphi_i$.
The translation $P$ is defined by induction with the following set
of ordered cases:
%in Table~\ref{tab:SLHLA-P}.
%
%\begin{table}[ht]
%\caption{Translation $P$ of entailment in $\slhl(\hls{a})$}
\begin{eqnarray}
P(\Pi, \emp, \{(\Pi_i,\emp)\}_{i\in I'}) 
	& \triangleq &\Pi \limp \lor_{i\in I'} \Pi_i
\\
P(\Pi, \emp, \{(\Pi',\Sigma')\}\cup S) 
	& \triangleq & \textit{P}(\Pi, \emp, S)
	\mbox{ if }\Sigma' \not\equiv \emp
\\
P(\Pi, \Sigma, \{(\Pi',\emp)\} \cup S)
	& \triangleq & P(\Pi, \Sigma, S)
	\mbox{ if }\Sigma \not\equiv \emp
\\
P(\Pi,\emp\sepc \Sigma, S)
	& \triangleq & P(\Pi,\Sigma,S)
\\
P(\Pi,\Sigma, \{(\Pi',\emp\sepc\Sigma')\} \cup S)
	& \triangleq & P(\Pi,\Sigma, \{(\Pi',\Sigma')\} \cup S) \\
	&&\nonumber\mbox{ if }\Sigma \not\equiv \emp\sepc\Sigma''
\end{eqnarray}
\begin{eqnarray}
\lefteqn{P(\Pi,\hls{a}(t;u)\sepc \Sigma, 
           \{(\Pi_i, \hls{a}(t_i;u_i)\sepc \Sigma_i)\}_{i\in I})} \\
   & \triangleq & 
    %% eliminate atom in antecedent
   	\land_{J\subseteq I}
	P(\begin{array}[t]{l}
	      \Pi \land t+c < u \land \textit{Order}(u,\Sigma) 
		  \land_{j\in J} u-t=u_j-t_j 
		  \land_{i\in I\setminus I'} u-t < u_i-t_i, \\
       \Sigma, \\
	  %% matching exactly the antecedent
	  \{(\Pi_i \land t=t_j \land \textit{Order}(u_j,\Sigma_j), \Sigma_j) \}_{j \in J} \\
	  %% bigger than the antecedent
	  \cup \{(\Pi_i \land t=t_i, \hls{a}(u;u_i) \sepc \Sigma_i\}_{i\in I\setminus I'}))
	  \end{array} \\
    %% split atom in antecedent
   && \land_{\emptyset\neq J\subseteq I}
	P(\begin{array}[t]{l}
	  )
	  \end{array}
\end{eqnarray}
%\label{tab:SLHLA-P}
%\end{table}%



\end{proof}

For the fragment $\slhl(\hls{al})$, the predicate $\hls{al}$ satisfies
the same constraints as $\hls{a}$ and the two decidability results also hold.

For the fragment $\slhl(\hls{ac})$, the predicate $\hls{ac}$ satisfies
the following composition lemmata:
\begin{equation}\label{lemma:hlsac}
\hls{ac}(x,st;y,st') \sepc \hls{ac}(y,st';z,st'') \limp \hls{ac}(x,st;z,st'')
\end{equation}
Therefore, when translating an atom $\hls{ac}(x,st;y,st')$ into a $\blk(x;y)$ atoms,
additional constraints are added on $st$ and $st'$.


\subsection{Decision Procedures for $\slhl(\hls{s})$}
\label{ssec:dp-hls-s}

