\documentclass[draft]{llncs}

%% Packages
\usepackage{packages}
%% Macros
% for comments
%\declareauthor{ms}{Mihaela}{red}
%\declareauthor{zw}{Zhilin}{blue}

\include{commands}

%% Title
\title{Decision Procedures for Inductive  Separation Logic 
 to Reason About Memory Allocators}
\author{Mihaela Sighireanu, Zhilin Wu}
\date{}

\begin{document}
\sloppy

%------------------------------------------------------------------------------

\maketitle

%------------------------------------------------------------------------------
\section{Introduction}

outline

motivation

data structures used in memory allocators

free list, heap list

fragment for heap list: syntax, semantics, decision procedures

fragment for free list: syntax, semantics, decision procedures



\paragraph{Overview:}
Section~\ref{sec:slid-fld} studies the logic which extends the Array Separation Logic~\cite{BrotherstonGK17} with linear compositional inductive definitions and hierarchical conjunction operator.
%
Section~\ref{sec:slid-wrd} introduces the logic which extends the Array Separation Logic~\cite{BrotherstonGK17} with list inductive definitions collecting words of addresses.


%------------------------------------------------------------------------------
\section{Preliminaries on Array Separation Logic}
\label{sec:ASL}

The logics we are studying are built on top of Array Separation Logic~\cite{BrotherstonGK17}, whose definition is recalled below.

\begin{definition}[ASL Syntax]\label{def:syn-ASL}
Terms $t$, pure formulas $\Pi$, spatial formulas $\Sigma$ and symbolic heaps $SH$ of ASL are given by the following grammar:
\begin{eqnarray}
t & ::= & x \mid n \mid t+t \label{eq:syn-ASL-term} %\mid nt
\\[1mm]
\Pi & ::= & t=t \mid t \ne t \mid t \leq t \mid t < t \mid \Pi \land \Pi
\\[1mm]
\Sigma & ::= & \emp \mid t \pto t \mid \blk(t,t) \mid \Sigma \sepc \Sigma
\\[1mm]
SH & ::= & \exists \vec{z}\cdot \Pi : \Sigma
\end{eqnarray}
where 
$x$ ranges over an infinite set $\mathcal{V}$ of variables, 
$\vec{z}$ over sets of variables in $\mathcal{V}$, 
and $n$ over $\NN$. 
Whenever one of $\Pi$ or $\Sigma$ is empty, we omit the colon. 
%We write $FV (A)$ for the set of free variables occurring in $A$. 
%If $A = \exists\vec{z}\cdot\Pi:\Sigma$, we write $qf(A)$ for $\Pi:\Sigma$, the quantifier-free part of $A$.
For convenience, we suppose that $\mathcal{V}$ includes a variable $nil$.
\end{definition}


The model of a formula is a pair $(I, h)$ composed of 
an \emph{interpretation} $I$ of logic variables such that $nil$ is mapped always on $0$
and a \emph{heap} $h$ mapping locations to values. 
For simplicity, we consider that both locations are values are natural numbers, therefore $I:\mathcal{V}\tfun\NN$ and $h:\NN\pfun\NN$.
%%MS: this really maps what happens in memory models for DMA because 
%% the fields of the header are either positive values (status or size) 
%% or locations; the content of the block is not interpreted.

In ASL, semantics of terms need only the interpretation $I$. 
Since we will extend these terms such that we need the heap, we will provide 
a semantics for terms that employs the heap also.

\begin{definition}[Semantics of ASL terms]
The evaluation of a term in a model $(I,h)$ leads to a natural number as follows:
$$ %\begin{eqnarray*}
\sem n \antic_{I,h}\ = \ n 
\quad\quad 
\sem x \antic_{I,h} \ = \ I(x) 
\quad\quad 
\sem t_1 + t_2 \antic_{I,h} \ =\ \sem t_1 \antic_{I,h} + \sem t_2 \antic_{I,h} 
$$ %\end{eqnarray*}
\end{definition}

To give the semantics of formulas, we use the following notations.
For a variable $v\in\mathcal{V}$ and $m\in\NN$, we denote by $I[v\mapsto m]$ for the interpretation defined as $I$ but with $v$ mapped to $m$. Interpretations are extended in a natural way to tuples.
The domain of a heap is denoted by $\wpos{h}$. 
% and the heap with empty domain is $e$.
Two heaps $h_1$ and $h_2$ are composed by $h_1\uplus h_2$ if they have disjoint domains, otherwise the composition is undefined.

\begin{definition}[Semantics of ASL formulas]
The satisfaction relation $I,h\models A$ with $I$ an interpretation of free variables in $A$ and $h$ a heap, is defined by structural induction on $A$ as follows:
\begin{eqnarray*}
I,h \models t_1 \sim t_2 & \mbox{ iff } & 
	\sem t_1 \antic_{I,h} \sim \sem t_2 \antic_{I,h}\mbox{ where }\sim\in\{=,\ne,\le,<\}
\\
I,h \models \Pi_1 \land \Pi_2 & \mbox{ iff } &
	I,h \models \Pi_1 \mbox{ and } I,h\models \Pi_2
\\
I,h \models \emp & \mbox{ iff } & 
	\wpos{h}=\emptyset
\\
I,h \models t_1 \pto t_2 & \mbox{ iff } & 
	\exists n\in\NN\mbox{ s.t. }
	\sem t_1 \antic_{I,h}=n,\ \wpos{h}=\{n\}\mbox{ and } h(n)=\sem t_2 \antic_{I,h}
\\
I,h \models \blk(t_1, t_2) & \mbox{ iff } & 
	\exists n,n'\in\NN\mbox{ s.t. }
	\sem t_1 \antic_{I,h}=n,\ \sem t_2 \antic_{I,h}=n',\ n < n'\mbox{ and }\\
	& & \quad \wpos{h}=\{n,...,n'-1\}
\\
I,h \models \Sigma_1 \sepc \Sigma_2 & \mbox{ iff } & 
	\exists h_1,h_2\mbox{ s.t. } h=h_1\uplus h_2\mbox{ and }
	I,h_1\models\Sigma_1 \mbox{ and } I,h_2\models\Sigma_2
\\
I,h \models \exists\vec{z}\cdot\Pi:\Sigma & \mbox{ iff } &
	\exists \vec{m}\in\NN^{|\vec{z}|}\mbox{ s.t. } 
	I[\vec{z}\mapsto\vec{m}],h\models\Pi \mbox{ and } I[\vec{z}\mapsto\vec{m}],h\models\Sigma	
\end{eqnarray*}
\end{definition}

This semantics differs from the one given in~\cite{BrotherstonGK17} in the definition of contiguous arrays of values because we consider that in $\blk(\ell,\ell')$, the location $\ell'$ is the first after the last location in the array, firt proposed by in~\cite{CalcagnoDOHY06,FangS16} for the static analysis of memory allocators.
This is because it is more convenient to have the lemmata about blocks:
$\blk(t_1,t_2)\sepc\blk(t_2,t_3)\limp\blk(t_1,t_3)$, i.e., this make compositional the block definition.
Similarly to \cite{BrotherstonGK17}, the block predicate may be employed 
with the absolute addressing $\blk(\ell,\ell')$ or 
with base-offset addressing $\blk(\ell, i, j)$ for the array from $\ell+i$ to $\ell+j-1$.

%\paragraph{Fragments:}
\begin{definition}[Two-variable fragment of ASL]
The fragment of ASL where the pure and spatial constraints satisfy the following constraints:
\begin{itemize}
\item restriction on pure constraints to difference constraints of the form 
$x=k$, $x=y+k$, $x\le y+k$, $x\ge y+k$ where $x$ and $y$ are variables and $k\in\NN$ ($x\neq y$ is not included)
\item the spatial constraints contain only 
$a\mapsto v$\todo[author=MS]{Why is $k$ instead of $a$}, 
$\blk(v,0,j)$, $\blk(a, 1, j)$, and $\blk(k, j, j+1)$, 
where $v$, $a$, and $j$ are variables, and $k\in\NN$.
\end{itemize}
is called \emph{two-variable fragment}.
\end{definition}

\noindent
The results in \cite{BrotherstonGK17} for the satisfiability of this logic are:
\begin{itemize}
\item The two-variable restriction of ASL is decidable in polynomial time.
\item The satisfiability is NP-complete. 
\end{itemize}
The proof is by encoding into a Presburger arithmetic formulas in the class $\Sigma^0_1$.


%------------------------------------------------------------------------------
\section{SL with Inductive Definitions over ASL}
\label{sec:slid-fld}

We define three logics that introduce special inductive definitions using ASL formulas.

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Free-list Inductive Definitions}

The first logic is a symbolic heap logic where the spatial formulas specify chunks of memory prefixed by a record whose first field give the size of the chunk and linked lists built using these chunks and one pointer selector included in the record.
The record included a third field, always set to 1 (because the free list includes only free chunks).
The size field may be used in terms as $\fsz(x)$ if the location $x$ occurs as left term of a points-to constraint.

\begin{definition}[Syntax of $\slflsf$]\label{def:syn-flsf}
Terms $t$, pure formulas $\Pi$, spatial formulas $\Sigma$ and symbolic heaps $\phi$ of $\slflsf$ are given by the following grammar:
\begin{eqnarray}
t & ::= & x \mid n \mid \fsz(x) \mid t+t \label{eq:syn-ASL-term} %\mid nt
\\[1mm]
\Pi & ::= & t=t \mid t \ne t \mid t \leq t \mid t < t \mid \Pi \land \Pi
\\[1mm]
\Sigma & ::= & \emp \mid \fck(x;y) \mid P(x,\vec{z};y,\vec{u};\vec{v}) \mid \Sigma \sepc \Sigma
\\[1mm]
\phi & ::= & \exists \vec{z}\cdot \Pi : \Sigma \mid \phi \lor \phi
\end{eqnarray}
where 
$x,y$ ranges over an infinite set $\mathcal{V}$ of variables, 
$\vec{z},\vec{u},\vec{v}$ over vectors of variables, 
$n$ over $\NN$, and 
$P$ is a predicate in a finite set of predicates $\mathcal{P}^F$ which have linear  inductive definitions. 
For convenience, we suppose that $\mathcal{V}$ includes a variable $nil$.
The predicate $\fck(x;y)$ is defined by the ASL formula:
$$\fck(x;y)\triangleq\exists sz,st\cdot sz > 3 : x\pto sz \sepc x+1\pto 1 \sepc x+2\pto y \sepc \blk(x,3,sz)$$
\end{definition}

\begin{definition}[Linear inductive definitions in $\slflsf$]\label{def:syn-fls-LID}
A linear  inductive definition of a predicate $P$ in $\mathcal{F}$ 
has \emph{only one base rule} of the form:
\begin{eqnarray*}
P(x,\vec{z};y,\vec{u};\vec{v}) & \Leftarrow & (x=y \land \vec{z}=\vec{u} : \emp) 
\end{eqnarray*}
and \emph{at least one recursive rule} of the form:
\begin{eqnarray*}
P(x,\vec{z};y,\vec{u};\vec{v}) & \Leftarrow &
\big(\exists w,\vec{z}'\cdot \Pi(x,\vec{z},w,\vec{z}',\vec{v}) :
\fck(x;w) \sepc P(w,\vec{z'};y,\vec{u};\vec{v})
\big)
\end{eqnarray*}
where $|\vec{z}|=|\vec{z}'|=|\vec{u}|\ge 0$.
The pure constraints $\Pi$ in each recursive rule shall be satisfiable.
The parameters $\vec{z}$ are called cumulating\todo{vocabulary?}{} parameters 
	because they are updated at the recursive call.
The parameters $\vec{u}$ are called glue\todo{vocabulary?}{} parameters 
	because they are not changed in the recursive call but are constrained by the base rule.
The parameters $\vec{v}$ are called static parameters 
	because they are not changed in the recursive call and not constrained by the base rule. 
\end{definition}

Although the free lists used by DMA are either acyclic (ending in $nil$) or cyclic, we don't restrict inductive rules to avoid lasso shaped free lists.
(Such non-lasso shapes may be obtained by adding the non aliasing constraint $x\ne y$ in the recursive rules.)
However, lasso shapes are not useful because they may lead to infinite loops in the search for a matching free chunk.


\begin{definition}[Semantics of $\slflsf$]\label{def:sem-fls}
A term $t$ of the form $\fsz(x)$ is interpreted as 
$\sem t \antic_{I,h} = h(I(x))$ if $I(x)\in\wpos{h}$ and undefined otherwise.
%
The satisfaction relation $I,h\models A$ with $I$ an interpretation of free variables in $A$ and $h$ a heap, is defined by extending the satisfaction relation in ASL by the following rules:
\begin{eqnarray*}
I,h\models \fck(x;y) & \mbox{ iff } & 
	I,h\models \exists sz,st\cdot \begin{array}[t]{l}
		sz > 3 : x\pto sz \sepc x+1\pto 1 \\
		\sepc\ x+2\pto y\sepc\blk(x,3,sz)
		\end{array}
\\
I,h\models P(x,\vec{z};y,\vec{u};\vec{v}) & \mbox{ iff } & 
	\exists k\in\NN\cdot I,h\models P^k(x,\vec{z};y,\vec{u};\vec{v})
\\
I,h\models P^0(x,\vec{z};y,\vec{u};\vec{v}) & \mbox{ iff } & 
    I,h\models (x=y \land \vec{z}=\vec{u} : \emp)
\\
I,h\models P^{k+1}(x,\vec{z};y,\vec{u};\vec{v}) & \mbox{ iff } & 
	\mbox{there exists a recursive rule } \\
& & P(x,\vec{z};y,\vec{u};\vec{v})\Leftarrow \varphi(P(w,\vec{z}';y,\vec{u};\vec{v}))\mbox{ s.t. } \\
& & I,h\models \varphi(P^k(w,\vec{z}';y,\vec{u};\vec{v}))
\end{eqnarray*}
\end{definition}


\subsubsection{Classes of Inductive Definitions}
%``````````````

To obtain a small model property of formulas employing inductively defined predicate atoms $P(x,\vec{z};y,\vec{u};\vec{v})$, we restrict the constraints used in the inductive definitions for:
\begin{itemize}
\item the addresses bounding the list segment ($x$ and $y$),
\item the number of cells in the list (that may be counted using cumulating parameters in $\vec{z}$),
\item the size of the chunks in the list, which is given by the $\fsz(x)$ term.
\end{itemize}
The key idea is to keep the length of the list segments defined by these predicates ``elastic'' such that the list segments may be unfolded a finite number of times without losing satisfiability.
  
\begin{definition}[Elastic linear inductive definitions in $\slflsf$]\label{def:syn-fls-EID}
A linear inductive definition of a predicate $P$ in $\mathcal{F}$ 
is \emph{elastic} iff they satisfy the following constraints: 
\begin{enumerate}
\item the predicate has no cumulative parameters, i.e., it has only parameters $P((x;y;\vec{v})$,
\item the recursive rule 
$P(x;y;\vec{v})\Leftarrow \exists w\cdot \Pi(x,w,\vec{v}) : \fck(x;w)\sepc P(w;y;\vec{v})$ 
includes a restricted form of pure constraints:
\begin{eqnarray*}
\Pi(x,w,\vec{v}) & ::= &
\Omega(x,w) \land \bigwedge_j \Omega_j(\fsz(x),\vec{v}_j) 
\\
\Omega(x,y) & ::= & \textit{true} \mid x < y \mid x > y 
\end{eqnarray*}
\end{enumerate}
\end{definition}

Elastic ID allows only empty (true) or an ordering constraint ($x < w$ or $x > w$ ) between $x$, the first location in the list segment, and its successor in the list $w$ by the field next (3rd field).
Also, the size of each chunk can only be compared with some static parameters.

\subsubsection{Examples of Free-list Invariants}
%``````````````

\paragraph{The free list is circular.}
We use the simplest predicate that satisfy the wellformedness constraints, and has only two (mandatory) parameters:
\begin{equation}
\fls{}(x;y) \triangleq (x=y :\emp) \lor (\exists w\cdot \fck(x,w)\sepc\fls{}(w,y))
\end{equation}
The property is expressed by the formula:
\begin{equation}
\fls{}(x,y) \sepc \fck(y;x)\label{eq:flsf}
\end{equation}

\paragraph{The free list is acyclic and sorted by addresses of free chunks.}
The definition of the predicate $\fls{o}$ expressing the sorting constraint is:
\begin{eqnarray*}
\fls{o}(x;y) & \triangleq & (x=y : \emp) \\
& \lor & \big(\exists w\cdot x < w : \fck(x;w) \sepc \fls{o}(w;y)\big)
\end{eqnarray*}
Using this predicate, the property is specified by:
\begin{equation}
\fls{o}(x;y) \sepc \fck(y;nil)
\end{equation}
Notice that we specify a list with at least one element because 
the ordering constraint is not satisfied for $nil$ which is the smallest 
value for locations.

\paragraph{The free list is acyclic and split according the first-fit policy by the request of size $r$.}
We collect the first-fit policy by the predicate $\fls{<}$ which has one static parameter used to bound the size of each chunk:
\begin{eqnarray*}
\fls{<}(x;y;v) & \triangleq & (x=y : \emp) \\
& \lor & \big(\exists w\cdot fsz(x) < v : \fck(x;w) \sepc \fls{<}(w;y;v))
\end{eqnarray*}
The invariant is specified using both $\fls{<}$ and $\fls{}$:
\begin{equation}
\fsz(Y) \ge r : \fls{<}(x;y;r) \sepc \fck(y;z) \sepc \fls{}(z;nil)
\end{equation}

\paragraph{The free list is acyclic and split according the best-fit policy by the request of size $r$.}
The predicate $\fls{bf}$ is used to express that the chunks inside the list segment are not the best choice for the request $v$ compared with the best-fit chunk which size is $v'$:
\begin{eqnarray*}
\fls{bf}(x;y;v,v') & \triangleq & (x=y : \emp) \\
& \lor & \big(\exists w\cdot fsz(x) < v : \fck(x;w) \sepc \fls{bf}(w;y;v,v'))\\
& \lor & \big(\exists w\cdot fsz(x) > max(v,v') : \fck(x;w) \sepc \fls{bf}(w;y;v,v'))
\end{eqnarray*}
The invariant is specified using the above predicate:
\begin{equation}
b=\fsz(y) \land b > r : \fls{bf}(x;y;r,b) \sepc \fck(y;z) \sepc \fls{bf}(z;nil;r,b) 
\end{equation}


\subsubsection{Decision Procedure for Satisfiability}
%``````````````
\begin{theorem}
Formulas in $\slflsf$ have the small model property.
\end{theorem}

\begin{theorem}
The satisfiability of formulas in $\slflsf$ is decidable in NP.
\end{theorem}



%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Heap-list Inductive Definitions}



%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Mixing Free-lists and Heap-lists}



\newpage
%------------------------------------------------------------------------------
\section{SL for Specifying Free List}
\label{sec:slid-wrd}

We introduce two \emph{symbolic heap} fragments of SL to capture the properties of free list used in memory allocators:
\begin{description}
\item[$\slflsf$] allows inductive definitions of the free list with summary parameters on integer data and compositionality property,
\item[$\slflsw$] allows inductive definitions that collect the sequence of cell addresses and pure constraints that are universal properties on these sequences.
\end{description}

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Syntax}

Both logics use the predicate $\blk(X,Y)$ denoting of raw bloc of bytes between location $X$ (included) and $Y$ (excluded) which was introduced in~\cite{CalcagnoDOHY06} for the static analysis of memory allocators. This predicate is exactly the array predicate used in Array Separation Logic~\cite{BrotherstonGK17}.


In addition, we use constraints on memory locations (like in pointer-arithmetics logic in ~\cite{BrotherstonK18}) and on data stored at locations dereferenced by the points-to predicate. These data are of a fixed record type, whose fields are an integer field $\fsz$ that is used to constrain locations by some pointer arithmetics (see definition of $\hck$ predicate), the boolean field $\fif$ that gives the status of the chunk (always true for free list chunks), and the pointer field $\fnx$ that contains the location of the next (free) chunk in the free list. We suppose that the size of this record is of 9 bytes, which is a constant used in the definition of $\hck$.

In $\slflsw$, the predicate $\fls{w}$ collects the start addresses of free chunks into a sequence\todo[author=MS]{Shall be added ``in a well structured way''?}. The length of these sequences may only be compared with arithmetic terms. The content of the sequences is restricted using an array folding logic \cite{DacaHK16}.
The integer terms $t$ are built over integer variables and field accesses using classic arithmetic operations and constants. We denote by $\Pi_\forall$ (resp. $\Pi_W$, $\Pi_\exists$) the set of sub-formulas of $\Pi$ built from universal constraints (resp. sequence constraints, quantifier free arithmetic constraints).

In $\slflsf$, the predicate $\fls{f}$ is a well formed compositional predicate with summary parameters as introduced in \cite{GuCW16}. Indeed, in addition to the start and ending location of the list segment, $\fls{f}$ has \emph{summary} parameters $\vec{i}$ and $\vec{j}$ (vectors of equal length) which collect constraints about the cells of the list using the \emph{folding function} $f$; the \emph{boundary} parameters are used in the folding function but are not changed in the inductive definition.
 
\begin{table}[htp]
\caption{Two logics for free lists}
\label{tab:syn-fls}
\begin{center}
\fbox{~Vocabulary:~}
\begin{tabular}{l@{\hskip 2eX}l@{\hskip 3eX}l@{\hskip 2eX}l}
$x,y,z,X,Y,Z,nil \in \mathcal{X}$ & integer and location variables &
$W,W' \in \mathcal{W}$ & sequence variables 
\\
$i,j \in \mathcal{P}$ & word's position variables &
$n \in \ZZ$ & integer constant
%$d,k,l \in \mathcal{I}$ & integer variables 
\\
\\
%$\{\fnx,\fsz,\fif\} = \mathcal{F}$ & field constants & 
\end{tabular}

\vspace{2eX}
\fbox{~Shared terms and formulas:~}
\begin{eqnarray*}
t & ::= & n \mid X \mid \fsz(X) \mid \fif(X) \mid t + t \mid t - t 
\\[1mm]
A & ::= & t \# n \mid A \land A \mid \lnot A
\hspace{5eX} \#\in\{=,\le,\ge,\} 
\\[1mm]
\hck(X;Z) & \triangleq & X \pto \{ (\fsz,x), (\fif,y), (\fnx,Y) \} \sepc \blk(X+3,Z) \land Z-X=x
\\[1mm]
\fck(X;Y) & \triangleq & \hck(X;Z) \land \fif(X)=1 \land \fnx(X)=Y
\end{eqnarray*}

\fbox{~Logic $\slflsw$:~}
\begin{eqnarray*}
t & ::= & \textrm{ as before } \mid W[i] \\[1mm]
G & ::= & \mathit{true} \mid j=i+1 \\[1mm]
U & ::= & \forall i,j \in \wpos{W}\cdot G \limp A \mid U \land U
\\[2mm]
w & ::= & \wemp \mid \wsgl{X} \mid w.w'
\\[2mm]
\Pi_w & ::= & X - Y \# t \mid W=w \mid |W| \# t \mid A \mid U \mid \Pi_w  \land \Pi_w 
\\[2mm]
\Sigma^F_w & ::= & \emp \mid X \pto \{ (\fsz,d), (\fif,k), (\fnx,Y) \} \mid \blk(X,Y) \mid \Sigma^F_w  \sepc \Sigma^F_w \\
& \mid & \fls{w}(X;Y)[W]
\\[2mm]
\phi_w & ::= & \exists \vec{X}\cdot \Pi_w  \land \Sigma^F_w  \mid \phi_w  \lor \phi_w  
\\[2mm]
\fls{w}(X;Y)[W] & \triangleq & (\emp \land X=Y\land W=\wemp) \\
& \lor & (\exists Z,W'\cdot \fck(X;Z) \sepc \fls{w}(Z;Y)[W'] \land W=\wsgl{X}.W')
\end{eqnarray*}

\fbox{~Logic $\slflsf$:~}
\begin{eqnarray*}
\Pi_f & ::= & X - Y \# t \mid A \mid \Pi_f  \land \Pi_f 
\\[2mm]
\Sigma^F_f & ::= & \emp \mid X \pto \{ (\fsz,i), (\fif,k), (\fnx,Y) \} \mid \blk(X,Y) \mid \Sigma^F_f \sepc \Sigma^F_f \\
& \mid & \fls{f}(X,\vec{i};Y,\vec{j};\vec{b};f)
\\[2mm]
\phi_f & ::= & \exists \vec{X}\cdot \Pi_f  \land \Sigma^F_f  \mid \phi_f  \lor \phi_f  
\\[2mm]
\fls{f}(X,\vec{i};Y,\vec{j};\vec{b};R) & \triangleq & (\emp \land X=Y\land \vec{i}=\vec{j}) \\
& \lor & (\exists Z,\vec{k}\cdot \fck(X;Z) \sepc \fls{f}(Z,\vec{k};Y,\vec{j};\vec{b};R) \land R(X;\vec{i};\vec{k};\vec{b}))
\end{eqnarray*}
\end{center}
\end{table}%

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Semantics}

For simplicity, we will consider that all fields have the same type of values $\mathbb{V}$, which is also the type of location (pointer) values.
The model of a formula is a pair $(I, h)$ composed of an interpretation of logic variables (such that $nil$ is mapped on 0) and a \emph{heap} $h$, a partial map from locations in $\mathbb{L}$ (where $\mathbb{L}\subset\mathbb{V}$) on values $\mathbb{V}$.
A location $l$ is \emph{allocated} in $(I,h)$ if and only if $l$ is in the domain of $h$.
\begin{eqnarray*}
\textrm{interpretation}\quad I \in \mathbb{I} & \triangleq & ((\mathcal{X}\cup\mathcal{I}\cup\mathcal{P}) \pfun \mathbb{V})\cup (\mathcal{W}\pfun\mathbb{L}^*) 
\\
\textrm{heap}\quad h \in \mathbb{H} & \triangleq & \mathbb{L} \pfun \mathbb{V}
\end{eqnarray*}

The block atom $\blk(X;Y)$ holds iff the heap contains a sequence of values starting at location $X$ ending before the location $Y$.
The predicates $\hck$ and $\fck$ as well as the inductive definitions are derived from $\blk$ using formulas. The main points of the logics' semantics are defined in Table~\ref{tab:fls-sem}.

\begin{table}[ht]
\caption{Semantics for logics $\slflsw$ and $\slflsf$}
\label{tab:fls-sem}
\begin{center}
Terms: integer and word \\
$\begin{array}[t]{rcl}
\sem n \antic_{I,h} & \triangleq & n \\
\sem d \antic_{I,h} & \triangleq & I(d) \\
\sem W[i] \antic_{I,h} & \triangleq & I(W)[I(i)] \\
\sem \wsgl{X} \antic_{I} & \triangleq & \wsgl{I(X)}
\end{array}$
$\begin{array}[t]{rcl}
\sem \fsz(X) \antic_{I,h} & \triangleq & h(I(X)+\fsz) \\
\sem \fif(X) \antic_{I,h} & \triangleq & h(I(X)+\fif) \\
\sem \epsilon \antic_{I} & \triangleq & \epsilon \\
\sem w.w' \antic_{I} & \triangleq & \sem w \antic_{I} . \sem w' \antic_{I}
\end{array}$

\vspace{2eX}
Pure formulas: \\
$\begin{array}[t]{rcl}
I,h\models X-Y\# t & \mbox{iff} & I(X)-I(Y)\#\sem t\antic_{I,h} \\
I,h\models W=w & \mbox{iff} & I(W)=\sem w \antic_I \\
I,h\models |W| \# t & \mbox{iff} & |I(W)| \# \sem t \antic_{I,h} \\
I,h\models \forall i,j\in \wpos{W}\cdot G \limp A & \mbox{iff} & 
\mbox{for any } n,n'\in[0,|I(W)|-1]\\
& & I[i\leftarrow n, j\leftarrow n'], h\models G\limp A
\end{array}$

\vspace{2eX}
Spatial formulas:\\
$\begin{array}[t]{rcl}
I,h \models \emp & \mbox{iff} & \wpos{h}=\emptyset \\
I,h \models \blk(X,Y) & \mbox{iff} & \wpos{h}=[I(X),I(Y)) \\
I,h \models X \pto \{ (\fsz,d), (\fif,k), (\fnx,Y) \} & \mbox{iff} & \wpos{h}=[I(X),I(X)+2], h(I(X))=I(d), \\
& & h(I(X)+1)=I(k), h(I(X)+2)=I(Y) \\
\mbox{for } c\in\{w,f\},\ I,h \models \Sigma^F_c \sepc \Sigma^{F'}_c & \mbox{iff} & \mbox{exists } h_1,h_2 \mbox{ s.t. } h = h_1\uplus h_2,\\
& & I,h_1\models \Sigma^F_c,\ I,h_2\models \Sigma^{F'}_c 
\end{array}$
\end{center}
\end{table}



%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Syntactic Restrictions}

\paragraph{Word constraints:}
The word variables used in the word constraints are used with the following restrictions:
\begin{itemize}
\item The predicate spatial formulas $P(\ldots)[W]$ employ word variables which are not bound by other predicate constraints and not constrained in the left part of a pure constraints $W=w$. 
\item The word variable $W$ used as left term in the constraints $W=w$ is constrained only once and not used in any other word constraint.
\item The universal constraints refer only to word variables attached to predicate atoms.
\end{itemize}

\paragraph{Inductive definitions:}
The inductive definitions (ID) are \emph{linear and compositional}~\cite{GuCW16}.

In addition, the word constraint is only one allowed to appear in ID of $\slflsw$.

Fo ID in $\slflsf$, the function $f:\ZZ^{n+m}\tfun\ZZ^n$ is a \emph{folding function} satisfying the constraints in~\cite{DacaHK16}:
\begin{itemize}
\item it has the form $(\textit{grd}) ? \textit{upd}_1 : \textit{upd}_0$ for each dimension $u$ (here $0\le u < n$);
\item the guards are linear constraint on $\vec{i}$ and $\vec{b}$;
\item the update for dimension $u$ is either $\vec{i}[u]$ or $\vec{i}[u]+t$ with $t$ using only boundary variables in $\vec{b}$.
\end{itemize}



%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Examples of Invariants}

\paragraph{The free list is circular.}
In $\slflsw$, the predicate used is the one presented in the syntax: 
\begin{equation}
\fls{w}(X;Y)[W] \sepc \fck(Y;X)
\end{equation}

In $\slflsf$, the predicate used does not employ any additional summary of boundary parameter:
\begin{equation}
\fls{f}(X;Y) \sepc \fck(Y;X)\label{eq:flsf}
\end{equation}


\paragraph{The free list is sorted by addresses of free chunks.}
In $\slflsw$:
\begin{equation}
\fls{w}(X;Y)[W] \land \forall i,j\in\wpos{W}\cdot i+1=j \limp W[i] < W[j]
\end{equation}

In $\slflsf$, the summary parameters $X'$ and $Y'$ collect the location smaller resp. larger than all locations in the list segment using the relation $R(X;i;k)\triangleq i < X \land k=X$:
\begin{eqnarray*}
\fls{f}(X,X';Y,Y';R) & \triangleq & (\emp \land X=Y \land X'=Y') \\
& \lor & \big(\exists Z,k\cdot \fck(X;Z) \sepc \fls{f}(Z,k;Y,Y';R) \land R(X;X';k) \big)
\end{eqnarray*}
Using this inductive definition, the invariant is specified by:
\begin{equation}
\fls{f}(X,hst-1;Y,hli;<)
\end{equation}
where $hst$ and $hli$ are the boundary locations in the heap.


\paragraph{The free list is acyclic and split according the first-fit policy by the request of size $r$.}
In $\slflsw$:
\begin{multline}
\fls{w}(X;Y)[W_1] \sepc \fck(Y;Z) \sepc \fls{w}(Z;nil)[W_2] \\
\land \fsz(Y)\ge r \land \forall i\in \wpos{W_1}\cdot \fsz(W_1[i]) < r
\end{multline}

In $\slflsf$, the following inductive definition captures free list segments where chunks have sizes less than the boundary parameter $b$:
\begin{eqnarray*}
\fls{f}(X;Y;b;<) & \triangleq & (\emp \land X=Y) \\
& \lor & \big(\exists Z\cdot \fck(X;Z) \sepc \fls{f}(Z;Y;b;<) \land <(\fsz(X), b)\big)
\end{eqnarray*}
The invariant is specified using this ID and the one without parameters used in equation (\ref{eq:flsf}):
\begin{equation}
\fls{f}(X;Y;r;<) \sepc \fck(Y;Z) \sepc \fls{f}(Z;nil) \land \fsz(Y) \ge r
\end{equation}

\paragraph{The free list is acyclic and split according the best-fit policy by the request of size $r$.}
In $\slflsw$:
\begin{multline}
\fls{w}(X;Y)[W_1] \sepc \fck(Y;Z) \sepc \fls{w}(Z;nil)[W_2] \\
\land \fsz(Y)\ge r \land \forall i\in \wpos{W_1}\cdot \fsz(W_1[i]) < r \land \forall i\in \wpos{W_2}\cdot \fsz(W_1[i]) < r 
\end{multline}

In $\slflsf$, we use the ID $\fls{f}$ with relation $R_b(x;u,v): \fsz(x) \ge u \rightarrow \fsz(x)\ge v$ as follows:
\begin{equation}
\fls{f}(X;Y;r,\fsz(Y); R_b) \sepc \fck(Y;Z) \sepc \fls{f}_b(Z;nil;r,\fsz(Y);R_b) \land \fsz(Y)\ge r
\end{equation}


%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Decision Procedures for Satisfiability}

\begin{theorem}{}
Satisfiability problem for $\slflsf$ formulas is decidable.
\end{theorem}
\begin{proof}
Based on the result of ~\cite{GuCW16}, by reduction to reverse bounded counter machines to compute summaries and translate formulas on Presbuger arithmetics.
\todo[inline,author=MS]{What is the complexity for this specific case?}

Notes 17/09/2018: more restrictions on this fragment to obtain small model property: 
only comparison relations between locations; only comparison relations between sizes and integer static parameters. Unfold twice the predicates to capture the situation where the end of the list is allocated inside a block of the list.

A translation to MSO with single function may also work.

Also extension to length parameters which are only comparable witch some data in the style of CSP paper at CONCUR'09.
\end{proof}

\begin{theorem}{Decidability of $\slflsw$}
For any formula $\varphi$ in $\slflsw$, there exists an equi-satisfiable formula $\hat{\varphi}$ in $\slflsw$ with a number of inductive definitions less than the number of universal constraints.
\end{theorem}
\begin{proof}
\todo[inline,author=MS]{Show that the sizes of $\varphi$ and $\hat{\varphi}$ are in the same class and that the number of ID is bounded.}
\end{proof}


\newpage
%------------------------------------------------------------------------------
\section{SL for Heap List}

Similarly to free list, we introduce two \emph{symbolic heap fragments of SL} to capture the properties of the heap list used by memory allocators.
The first fragment is $\slhlsf$ for the logic with inductive definitions linear compositional and using folding parameters; it is analogous of $\slflsf$ for free lists.
The second fragment is $\slhlsw$ where inductive definitions collect words of addresses starting the cells of the list; it is analogous of $\slflsw$ for free lists.


%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Syntax}

The syntaxes of these logics use the elements from the syntaxes of free list logics in Table~\ref{tab:syn-fls}.
The new logics employ in spatial formulas only the $\blk$ and $\hck$ predicates and the inductive definitions given in Table~\ref{tab:syn-hls}.


\begin{table}[htp]
\caption{Two logics for heap lists (for unspecified elements, see Table~\ref{tab:syn-fls})}
\label{tab:syn-fls}
\begin{center}
\fbox{~Logic $\slhlsw$:~}
\begin{eqnarray*}
\Sigma^H_w & ::= & \emp \mid X \pto \{ (\fsz,d), (\fif,k), (\fnx,Y) \} \mid \blk(X,Y) \mid \Sigma^H_w  \sepc \Sigma^H_w \\
& \mid & \hls{w}(X;Y)[W]
\\[2mm]
\phi_w & ::= & \exists \vec{X}\cdot \Pi_w  \land \Sigma^H_w  \mid \phi_w  \lor \phi_w  
\\[2mm]
\hls{w}(X;Y)[W] & \triangleq & (\emp \land X=Y\land W=\wemp) \\
& \lor & (\exists Z,W'\cdot \hck(X;Z) \sepc \hls{w}(Z;Y)[W'] \land W=\wsgl{X}.W')
\end{eqnarray*}

\fbox{~Logic $\slhlsf$:~}
\begin{eqnarray*}
\Sigma^H_f & ::= & \emp \mid X \pto \{ (\fsz,i), (\fif,k), (\fnx,Y) \} \mid \blk(X,Y) \mid \Sigma^H_f \sepc \Sigma^H_f \\
& \mid & \hls{f}(X,\vec{i};Y,\vec{j};\vec{b};f)
\\[2mm]
\phi_f & ::= & \exists \vec{X}\cdot \Pi_f  \land \Sigma^H_f  \mid \phi_f  \lor \phi_f  
\\[2mm]
\hls{f}(X,\vec{i};Y,\vec{j};\vec{b};f) & \triangleq & (\emp \land X=Y\land \vec{i}=\vec{j}) \\
& \lor & (\exists Z,\vec{k}\cdot \hck(X;Z) \sepc \hls{f}(Z,\vec{k};Y,\vec{j};\vec{b};f) \land \vec{k}=f(\vec{i};\vec{b}))
\end{eqnarray*}
\end{center}
\end{table}%

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Semantics}

The semantics is similar to the one for the free lists.

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Syntactic Restrictions and Fragments}

The same syntactic restrictions as for free lists apply for the heap lists fragments.

\paragraph{Chunks and points-to:} equivalent to Array SL.

\paragraph{Chunks, points-to and heap-list with summary:} array SL with folding functions~\cite{DacaHK16} or parameters~\cite{GuCW16}.

\paragraph{Chunks, points-to and heap-list with words:} array SL with pure word constraints~\cite{FangS16}.

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Examples of Invariants}

\paragraph{The heap list is well formed.} This means that there are no chunks overlaps or gaps between chunks (memory leaks) due to bad sizes set in chunks.

In $\slhlsw$, this property is expressed simply by the ID $\hls{w}(X;Y)[W]$.

In $\slhlsf$, the ID $\hls{f}$ has no parameters at all.

\paragraph{The heap list is early coalesced.} This means that no adjacent chunks have both status free.

In $\slhlsw$, this property is expressed simply by the following formula:
$$\hls{w}(X;Y)[W] \land 
  \forall i,j\in\wpos{W}\cdot j=i+1 \limp 0 \le \fif(W[i])+\fif(W[j]) \le 1$$.
 
In $\slhlsf$, the ID $\hls{f}$ needs two integer parameters and one folding function as follows:
\begin{eqnarray*}
\hls{f}_c(X,s_{pX};Y,s_{pY};f_s) & \triangleq & (\emp \land X=Y \land s_{pX}=s_{pY}) \\
& \lor & (\exists Z,s\cdot \hck(X;Z) \sepc \hls{f}_c(Z,s;Y,s_{pY};f_s) \land s=f_s(\fif(X),s_{pX}))
\\
\mbox{where } f_s(s_1,s_2) & \triangleq & (0\le s_1+s_2 \le 1) ? s_1 : 2
\end{eqnarray*}

\paragraph{The heap list is split according the first-fit policy by the request of size $r$.}
The specification is very similar to the one for the free list.


%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Decision Procedures for Satisfiability}


\paragraph{Chunks and points-to:} equivalent to Array SL, use procedure in~\cite{BrotherstonGK17}.

\paragraph{Chunks, points-to and heap-list with summary:} by reduction to reverse bounded counter machines combining results from ASL and \cite{GuCW16}.

\paragraph{Chunks, points-to and heap-list with words:} reduction to heap lists with summaries.

\newpage
%------------------------------------------------------------------------------
\section{SL for Memory Allocators}

The static analysis by abstract interpretation of DMA proposed in~\cite{CalcagnoDOHY06} employs only a sub-fragment of $\slflsf$ and it is not able to capture the heap list properties.

The abstract domain proposed in \cite{FangS16} is based on a combination of $\slflsw$ and $\slhlsw$ that allows to efficiently encode the abstract transformers on the logic domain. We present in the following this combination in a logic called $\sldma$.

For convenience, this logic is defined by combination of $\slflsw$ and $\slhlsw$ logics.

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Syntax}

The logic $\sldma$ combines formulas in $\slflsw$ and $\slhlsw$ logics in a particular way, as formalized in Table~\cite{tab:syn-dma}.
Two special variables $W_H$ and $W_F$ are used to collect the sequence of chunks in the heap list resp. free list. By the syntactic restrictions of $\slflsw$ and $\slhlsw$, these two variables are not allowed to appear in any other (universal or word) pure or spatial constraint.

The spatial constraint $\Sigma^H_w \supset \Sigma^F_w$ means that the set of locations collected in $W_H$ includes the one of locations in $W_F$.
 
\begin{table}[htp]
\caption{Syntax of $\sldma$}
\label{tab:syn-dma}
\begin{center}
$\begin{array}[t]{rcl}
\Sigma & ::= & \Sigma^H_w \supset \Sigma^F_w \\
\Pi & ::= & \Pi_w \mid W_H = w \mid W_F=w \\
\phi & ::= & \exists \vec{X}\cdot \Pi \land \Sigma \mid \phi \lor \phi
\end{array}$
\hspace{8eX}
$\begin{array}[t]{rcl}
\Sigma & ::= & \Sigma^H_f \supset \Sigma^F_f \\
\phi & ::= & \exists \vec{X}\cdot \Pi_f \land \Sigma \mid \phi \lor \phi
\end{array}$
\end{center}
\end{table}%

The constraint on $W_H$ and $W_F$ may be transferred in the semantics by introducing an additional semantic object that collects the set of locations that are constrained by a points-to atom (i.e., they are the start address of a chunk header).

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Semantics}

For the definition of the logic based on words, the semantics for the fragments $\slflsw$ and $\slhlsw$ are combined to obtain the semantics of spatial constraint $\Sigma^H_f \supset \Sigma^F_f$ as shown in the first part of Table~\ref{tab:sem-dma}.


For the semantics with folding, we have to collect during the semantics the locations constrained using points-to. Therefore, we change the semantics judgements to
$I,h,\tau\models \phi_f$ where $\tau$ is a set of chunk header locations, collected as shown in the second part of Table~\ref{tab:sem-dma}.


\begin{table}[ht]
\caption{Two semantics for logic $\sldma$}
\label{tab:sem-dma}
\begin{center}
Based on $\slflsw$ and $\slhlsw$:
\begin{eqnarray*}
I,h \models \Sigma^H_f \supset \Sigma^F_f & \mbox{iff} & I(W_H) \supset I(W_F)
\end{eqnarray*}

Based on $\slflsf$ and $\slhlsf$ and new judgment $I,h,\tau\models\phi$:
\begin{eqnarray*}
I,h,\tau \models \emp & \mbox{iff} & \wpos{h}=\emptyset=\tau \\
I,h,\tau \models \blk(X,Y) & \mbox{iff} & \wpos{h}=[I(X),I(Y)),\ \tau=\emptyset \\
I,h,\tau \models X \pto \{ (\fsz,d), (\fif,k), (\fnx,Y) \} & \mbox{iff} & \wpos{h}=[I(X),I(X)+2], h(I(X))=I(d),\ \tau=\{I(X)\}\\
& & h(I(X)+1)=I(k), h(I(X)+2)=I(Y) \\
I,h,\tau \models \Sigma^F_f \sepc \Sigma^{F'}_f & \mbox{iff} & \mbox{exists } h_1,\tau_1,h_2,\tau_2 \mbox{ s.t. } h = h_1\uplus h_2, \tau = \tau_1\cup\tau_2\\
& & I,h_1,\tau_1\models \Sigma^F_f,\ I,h_2,\tau_2\models \Sigma^{F'}_f 
\end{eqnarray*}
\end{center}
\end{table}



%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
\subsection{Decision Procedures for Satisfiability}

\todo[inline,author=MS]{Idea: export the summary constraints at free-list level on the heap-list summaries.
Take care of he order in which the free-list segments are mapped on the heap-list (always ordered).}


\newpage
%------------------------------------------------------------------------------
\bibliographystyle{abbrv}
\bibliography{biblio}
\end{document}
