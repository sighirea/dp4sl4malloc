\section{Preliminaries on Array Separation Logic (\sla)}
\label{sec:SLA}

\zhilin{Should we directly define the logic PASLID here ?}

The logics we propose are built on top of Array Separation Logic (\sla)~\cite{BrotherstonGK17}, for which we recall its syntax, semantics and decision problems.

\subsection{Syntax and Semantics}

\begin{definition}[\sla\ Syntax]\label{def:syn-SLA}
Terms $t$, pure formulas $\Pi$, spatial formulas $\Sigma$ and symbolic heaps $SH$ of \sla\ are given by the following grammar:
\begin{eqnarray}
t & ::= & x \mid n \mid t+t \label{eq:syn-SLA-term} %\mid nt
\\[1mm]
\Pi & ::= & t=t \mid t \ne t \mid t \leq t \mid t < t \mid \Pi \land \Pi
\\[1mm]
\Sigma & ::= & \emp \mid t \pto t \mid \blk(t,t) \mid \Sigma \sepc \Sigma
\\[1mm]
SH & ::= & \exists \vec{z}\cdot \Pi : \Sigma
\end{eqnarray}
where 
$x$ ranges over an infinite set $\mathcal{V}$ of variables, 
$\vec{z}$ over sets of variables in $\mathcal{V}$, 
and $n$ over $\NN$. 
Whenever one of $\Pi$ or $\Sigma$ is empty, we omit the colon. 
%We write $FV (A)$ for the set of free variables occurring in $A$. 
%If $A = \exists\vec{z}\cdot\Pi:\Sigma$, we write $qf(A)$ for $\Pi:\Sigma$, the quantifier-free part of $A$.
For convenience, we suppose that $\mathcal{V}$ includes a variable $nil$.
\end{definition}



The model of a formula is a pair $(I, h)$ composed of 
an \emph{interpretation} $I$ of logic variables such that $nil$ is mapped always to $0$
and a \emph{heap} $h$ mapping locations to values. 
For simplicity, we consider that both locations and values are natural numbers, therefore $I:\mathcal{V}\tfun\NN$ and $h:\NN\pfun\NN$.
%%MS: this really maps what happens in memory models for DMA because 
%% the fields of the header are either positive values (status or size) 
%% or locations; the content of the block is not interpreted.

In \sla, semantics of terms need only the interpretation $I$. 
However, we will extend these terms with operator that require the heap.
Therefore, we provide here a unified semantics for terms that employs the heap also.

\begin{definition}[Semantics of \sla\ terms]
The evaluation of a term in a model $(I,h)$ leads to a natural number as follows:
$$ %\begin{eqnarray*}
\sem n \antic_{I,h}\ = \ n 
\quad\quad 
\sem x \antic_{I,h} \ = \ I(x) 
\quad\quad 
\sem t_1 + t_2 \antic_{I,h} \ =\ \sem t_1 \antic_{I,h} + \sem t_2 \antic_{I,h} 
$$ %\end{eqnarray*}
\end{definition}

To give the semantics of formulas, we use the following notations.
For a variable $v\in\mathcal{V}$ and $m\in\NN$, we denote by $I[v\mapsto m]$ for the interpretation defined as $I$ but with $v$ mapped to $m$. Interpretations are extended in a natural way to tuples.
The domain of a heap is denoted by $\wpos{h}$. 
% and the heap with empty domain is $e$.
Two heaps $h_1$ and $h_2$ are composed by $h_1\uplus h_2$ if they have disjoint domains, otherwise the composition is undefined.


\begin{definition}[Semantics of \sla\ formulas]
The satisfaction relation $I,h\models A$ with $I$ an interpretation of free variables in $A$ and $h$ a heap, is defined by structural induction on $A$ as follows:
\begin{eqnarray*}
I,h \models t_1 \sim t_2 & \mbox{ iff } & 
	\sem t_1 \antic_{I,h} \sim \sem t_2 \antic_{I,h}\mbox{ where }\sim\in\{=,\ne,\le,<\}
\\
I,h \models \Pi_1 \land \Pi_2 & \mbox{ iff } &
	I,h \models \Pi_1 \mbox{ and } I,h\models \Pi_2
\\
I,h \models \emp & \mbox{ iff } & 
	\wpos{h}=\emptyset
\\
I,h \models t_1 \pto t_2 & \mbox{ iff } & 
	\exists n\in\NN\mbox{ s.t. }
	\sem t_1 \antic_{I,h}=n,\ \wpos{h}=\{n\}\mbox{ and } h(n)=\sem t_2 \antic_{I,h}
\\
I,h \models \blk(t_1, t_2) & \mbox{ iff } & 
	\exists n,n'\in\NN\mbox{ s.t. }
	\sem t_1 \antic_{I,h}=n,\ \sem t_2 \antic_{I,h}=n',\ n < n'\mbox{ and }\\
	& & \quad \wpos{h}=\{n,...,n'-1\}
\\
I,h \models \Sigma_1 \sepc \Sigma_2 & \mbox{ iff } & 
	\exists h_1,h_2\mbox{ s.t. } h=h_1\uplus h_2\mbox{ and }
	I,h_1\models\Sigma_1 \mbox{ and } I,h_2\models\Sigma_2
\\
I,h \models \exists\vec{z}\cdot\Pi:\Sigma & \mbox{ iff } &
	\exists \vec{m}\in\NN^{|\vec{z}|}\mbox{ s.t. } 
	I[\vec{z}\mapsto\vec{m}],h\models\Pi \mbox{ and } I[\vec{z}\mapsto\vec{m}],h\models\Sigma	
\end{eqnarray*}
\end{definition}


This semantics differs from the one given in~\cite{BrotherstonGK17} in the definition of contiguous arrays of values because we consider that in $\blk(\ell,\ell')$, the location $\ell'$ is the first after the last location in the array, like proposed first by in~\cite{CalcagnoDOHY06} for the static analysis of memory allocators.
Moreover, this form is more convenient to have the lemmata about blocks:
\begin{equation}\label{lemma:blk}
\blk(t_1,t_2)\sepc\blk(t_2,t_3)\limp\blk(t_1,t_3),
\end{equation}
i.e., such block definition is compositional.
Similarly to \cite{BrotherstonGK17}, the block predicate may be employed
with the absolute addressing $\blk(\ell,\ell')$ or
with base-offset addressing $\blk(\ell, i, j)$ for the array from $\ell+i$ to $\ell+j-1$.



\subsection{Satisfiability problem for \sla}
\label{ssec:sla-sat}

Brotherston et al. \cite{BrotherstonGK17} shows that
satisfiability checking for \sla\ is NP-complete.
%
Moreover, they identified a fragment, for the two-variable fragment of \sla,
for which the satisfiability decidable in polynomial time.
The proof is by encoding into a Presburger arithmetic formulas
in the class $\Sigma^0_1$.

\begin{definition}[Two-variable fragment of \sla]
The fragment of \sla\ where the pure and spatial constraints which satisfies the following constraints:
\begin{itemize}
\item the pure formulas allow only difference constraints of the form
$x=k$, $x=y+k$, $x\le y+k$, $x\ge y+k$ where $x,y\in\mathcal{V}$ and $k\in\NN$ ($x\neq y$ is not allowed),
\item the spatial constraints contain only
$a\mapsto v$\mihaela{$k$ instead of $a$ to have two variable params},
$\blk(a,0,j)$, $\blk(a, 1, j)$, and $\blk(k, j, j+1)$,
where $v$, $a$, and $j$ are variables, and $k\in\NN$,
\end{itemize}
is called \emph{two-variable fragment}.
\end{definition}

\medskip
The procedure proposed in \cite{DBLP:journals/corr/abs-1802-05935} has
as corollary a decision procedure for testing satisfiability of
formulas in the symbolic heap fragment of \sla.
%
Because we will use a similar principle for our fragments,
we explain (and reformulate) the procedure in \cite{DBLP:journals/corr/abs-1802-05935}.

Let $\varphi$ denote a formula $\Pi : \Sigma$, in this fragment.
The decision procedure translates $\varphi$ into a
quantifier free Presburger formula in disjunctive normal form $\bigvee_i \Pi_i$.
Each conjunct $\Pi_i$ includes $\Pi$ and literals 
which are comparisons of terms $t \sim u$ where $t$ and $u$
are addresses used in $\Sigma$ atoms; these comparisons fix a
total order between the addresses mentioned by the atoms as follows.
%
We denote by $A_\Sigma$ the atoms in $\Sigma$. 
We denote by $\textit{Perm}(\Sigma)$ the set of formulas
containing all atoms in $A_\Sigma$ in a different order.
By the commutativity of separating conjunction, 
$\Sigma$ is equivalent to any formula in $\textit{Perm}(\Sigma)$.
The function $\textit{Order}$ defined in Table~\ref{tab:sorted}
translates a formula $\Sigma_i$ in $\textit{Perm}(\Sigma)$ by ordering the
addresses in atoms following the order fixed in $\Sigma_i$.
The first parameter of $\textit{Order}$ is the lower bound of
addresses in $\Sigma_i$; its value is $0$ initially.
%
It is shown in~\cite{DBLP:journals/corr/abs-1802-05935}
that $\varphi$ and $\bigvee_{\Sigma_i\in\textit{Perm}(\Sigma)} \Pi \land \textit{Order}(0, \Sigma_i)$
are equi-satisfiable. 
This gives a decision procedure for satisfiability.

\begin{table}[ht]
\caption{Total ordering addresses in spatial formulas for non-commutative $\star$}
\label{tab:SLA-order}
\begin{center}
\begin{eqnarray}
\textit{Order}(lb,\emp) 
	& \triangleq & \textit{true} 
\\
\textit{Order}(lb,t\pto u) 
	& \triangleq & lb \le t 
\\
\textit{Order}(lb,\blk(t,u)) 
	& \triangleq & lb \le t \land t < u 
\\
\textit{Order}(lb,\emp \sepc \Sigma') 
	& \triangleq & \textit{Order}(lb,\Sigma') 
\\
\textit{Order}(lb,t\pto u \sepc \Sigma') 
	& \triangleq & lb \le t \land \textit{Order}(t+1,\Sigma')
\\
\textit{Order}(lb,\blk(t,u) \sepc \Sigma') 
	& \triangleq & lb \le t \land t < u \land  \textit{Order}(u, \Sigma') 
\end{eqnarray}
\end{center}
\end{table}%
%
%\begin{corollary}[Satisfiability of \sla\ based on \cite{DBLP:journals/corr/abs-1802-05935}]
%Given a formula $SH$ in \sla, SH is satisfiable iff
%the following Presburger formula
%$\bigvee_{SH'\in\textit{Perm}(SH)} \textit{Sorted}(SH')$ is satisfiable.
%\end{corollary}


\subsection{Entailment problem for \sla}
\label{ssec:sla-ent}

Brotherston et al. \cite{BrotherstonGK17} consider
the problem of checking validity of entailments in \sla.
Their decision procedure the entailment checking to
%%MS: to check!
	an equivalent condition that checks the existence of a counter-model for the given entailment;
the condition is checked using equi-satisfiable Presburger formula.
This procedure works for entailments where the succedent (right part)
have a restricted form of points-to atoms $t\pto t'$,
where $t'$ shall not include existentially quantified variables.


\medskip

Tatsuta et al. \cite{DBLP:journals/corr/abs-1802-05935} also
investigated the problem of checking validity of entailments in \sla\ 
of the following form:
$$\varphi \limp \bigvee \{ \psi_i \}_{i\in I}$$
where the $\blk(t,0,u)$ atoms in 
$\psi_i \equiv \exists \vec{z}_i \cdot \Pi_i : \Sigma_i$
(with $t$ and $u$ terms) are such that $u$ does not contain variables in $\vec{z}_i$, 
i.e., the sizes of blocks in the consequent do not depend on existentially quantified variables.
%
The decision procedure proposed reduces the problem to the
checking satisfiability or entailment of Presburger formulas.
The procedure consists in checking that
each ordering of spatial sub-formula of $\varphi$ 
matches one of the orderings of spatial sub-formulas of some $\psi_i$;
the matching employs the following lemmata on spatial atoms:
\begin{eqnarray}
\Pi : t\pto u & \limp & \Pi' : t' \pto u'
\quad \mbox{if } \Pi \limp t = t' \land u = u'
\\
\Pi : t\pto u & \limp &  \Pi' : \blk(t, 0, 1)
\quad \mbox{if } \Pi \limp \Pi' 
\\
\Pi : \blk(t,u) & \limp &  \Pi' : t\pto z \sepc \blk(t+1,u)
\quad \mbox{if } \Pi \limp t+1 < u
\\
\Pi : \blk(t,u) & \limp &  \Pi' : \blk(t', z) \sepc \blk(z',u) \\
&&
\nonumber\quad \mbox{if } \Pi \limp z = z' \land t=t'\land t < z \land z' < u
\end{eqnarray}
As shown in these lemmata, the matching generates additional
Presburger constraints (the side conditions), which are added 
to the pure formulas of the antecedent resp. consequent of matching.
The matching ends when each spatial atom of the consequent has been matched.
At this point, if the antecedent still has spatial atoms which are unmatched,
the procedure checks that the remaining formulas is unsatisfiable.
Otherwise, the procedure checks the validity of the entailment between the pure formulas of the antecedent and the consequent.

