
\section{Extension with Free-lists}
\label{sec:slid-fls}

The logic $\slflsf$ specifies chunks with status always set to 1 (free) and linked using the third field of the record stating the chunk.

\subsection{Syntax and semantics}

\begin{definition}[Syntax of $\slflsf$]\label{def:syn-flsf}
Terms $t$, pure formulas $\Pi$, spatial formulas $\Sigma$ and symbolic heaps $\phi$ of $\slflsf$ are given by the following grammar:
\begin{eqnarray}
t & ::= & x \mid n \mid \fsz(x) \mid t+t \label{eq:syn-SLA-term} %\mid nt
\\[1mm]
\Pi & ::= & t=t \mid t \ne t \mid t \leq t \mid t < t \mid \Pi \land \Pi
\\[1mm]
\Sigma & ::= & \emp \mid \fck(x;y) \mid P(x,\vec{z};y,\vec{u};\vec{v}) \mid \Sigma \sepc \Sigma
\\[1mm]
\phi & ::= & \exists \vec{z}\cdot \Pi : \Sigma \mid \phi \lor \phi
\end{eqnarray}
where
$x,y$ ranges over an infinite set $\mathcal{V}$ of variables,
$\vec{z},\vec{u},\vec{v}$ over vectors of variables,
$n$ over $\NN$, and
$P$ is a predicate in a finite set of predicates $\mathcal{P}^F$ which have linear  inductive definitions.
For convenience, we suppose that $\mathcal{V}$ includes a variable $nil$.
The predicate $\fck(x;y)$ is defined by the \sla\ formula:
\begin{equation}
\fck(x;y)\triangleq
\exists sz,st\cdot sz > 3 :
		x\pto sz \sepc x+1\pto 1 \sepc x+2\pto y \sepc \blk(x,3,sz)\label{eq:fck}
\end{equation}
\end{definition}


\begin{definition}[Linear inductive definitions in $\slflsf$]\label{def:syn-fls-LID}
A linear  inductive definition of a predicate $P$ in $\mathcal{P}^F$
has \emph{only one base rule} of the form:
\begin{eqnarray*}
P(x,\vec{z};y,\vec{u};\vec{v}) & \Leftarrow & (x=y \land \vec{z}=\vec{u} : \emp)
\end{eqnarray*}
and \emph{at least one recursive rule} of the form:
\begin{eqnarray*}
P(x,\vec{z};y,\vec{u};\vec{v}) & \Leftarrow &
\big(\exists w,\vec{z}'\cdot \Pi(x,\vec{z},w,\vec{z}',\vec{v}) :
\fck(x;w) \sepc P(w,\pvec{z}';y,\vec{u};\vec{v})
\big)
\end{eqnarray*}
where $|\vec{z}|=|\vec{z}'|=|\vec{u}|\ge 0$.
The pure constraints $\Pi$ in each recursive rule shall be satisfiable.
The parameters $\vec{z}$ are called data\todo{vocabulary?}{} parameters
	because they are updated at the recursive call.
The parameters $\vec{u}$ are called data glue\todo{vocabulary?}{} parameters
	because they are not changed in the recursive call but are constrained by the base rule.
The parameters $\vec{v}$ are called static parameters
	because they are not changed in the recursive call and not constrained by the base rule.
\end{definition}

Although the free lists used by DMA are either acyclic (ending in $nil$) or cyclic, we don't restrict inductive rules to avoid lasso shaped free lists.
(Such non-lasso shapes may be obtained by adding the non aliasing constraint $x\ne y$ in the recursive rules.)
However, lasso shapes are not useful because they may lead to infinite loops in the search for a matching free chunk.


\begin{definition}[Semantics of $\slflsf$]\label{def:sem-fls}
A term $t$ of the form $\fsz(x)$ is interpreted as
$\sem t \antic_{I,h} = h(I(x))$ if $I(x)\in\wpos{h}$ and undefined otherwise.
%
The satisfaction relation $I,h\models A$ with $I$ an interpretation of free variables in $A$ and $h$ a heap, is defined by extending the satisfaction relation in \sla\ by the following rules:
\begin{eqnarray*}
I,h\models \fck(x;y) & \mbox{ iff } &
	I,h\models \exists sz,st\cdot \begin{array}[t]{l}
		sz > 3 : x\pto sz \sepc x+1\pto 1 \\
		\sepc\ x+2\pto y\sepc\blk(x,3,sz)
		\end{array}
\\
I,h\models P(x,\vec{z};y,\vec{u};\vec{v}) & \mbox{ iff } &
	\exists k\in\NN\cdot I,h\models P^k(x,\vec{z};y,\vec{u};\vec{v})
\\
I,h\models P^0(x,\vec{z};y,\vec{u};\vec{v}) & \mbox{ iff } &
    I,h\models (x=y \land \vec{z}=\vec{u} : \emp)
\\
I,h\models P^{k+1}(x,\vec{z};y,\vec{u};\vec{v}) & \mbox{ iff } &
	\mbox{there exists a recursive rule } \\
& & P(x,\vec{z};y,\vec{u};\vec{v})\Leftarrow \varphi(P(w,\vec{z}';y,\vec{u};\vec{v}))\mbox{ s.t. } \\
& & I,h\models \varphi(P^k(w,\vec{z}';y,\vec{u};\vec{v}))
\end{eqnarray*}
\end{definition}

To obtain a small model property of formulas employing inductively defined predicate atoms $P(x,\vec{z};y,\vec{u};\vec{v})$, we restrict the constraints used in the inductive definitions for:
\begin{itemize}
\item the addresses bounding the list segment ($x$ and $y$),
\item the number of cells in the list (that may be counted using cumulating parameters in $\vec{z}$),
\item the size of the chunks in the list, which is given by the $\fsz(x)$ term.
\end{itemize}
The key idea is to keep the length of the list segments defined by these predicates ``elastic'' such that the list segments may be unfolded a finite number of times without losing satisfiability.

\begin{definition}[Elastic linear inductive definitions in $\slflsf$]\label{def:syn-fls-EID}
A linear inductive definition of a predicate $P$ in $\mathcal{P}^F$
is \emph{elastic} iff they satisfy the following constraints:
\begin{enumerate}
\item the predicate has no data parameters, i.e., it has only parameters $P((x;y;\vec{v})$,
\item the recursive rule
$P(x;y;\vec{v})\Leftarrow \exists w\cdot \Pi(x,w,\vec{v}) : \fck(x;w)\sepc P(w;y;\vec{v})$
includes a restricted form of pure constraints:
\begin{eqnarray*}
\Pi(x,w,\vec{v}) & ::= &
\Omega(x,w) \land \bigwedge_j \Omega_j(\fsz(x),\vec{v}_j)
\\
\Omega(x,y) & ::= & \textit{true} \mid x < y \mid x > y
\end{eqnarray*}
\end{enumerate}
\end{definition}

Elastic ID allows only empty (true) or an ordering constraint ($x < w$ or $x > w$ ) between $x$, the first location in the list segment, and its successor in the list $w$ by the field next (3rd field).
Also, the size of each chunk can only be compared with some static parameters.

Interesting invariants about the free lists may be expressed using elastic ID:
\begin{itemize}
\item \emph{The free list is circular.}
We use the simplest predicate that satisfy the wellformedness constraints, and has only two (mandatory) parameters:
\begin{equation}
\fls{}(x;y) \triangleq (x=y :\emp) \lor (\exists w\cdot \fck(x,w)\sepc\fls{}(w,y))
\end{equation}
The property is expressed by the formula:
\begin{equation}
\fls{}(x,y) \sepc \fck(y;x)\label{eq:flsf}
\end{equation}

\item \emph{The free list is acyclic and sorted by addresses of free chunks.}
The definition of the predicate $\fls{o}$ expressing the sorting constraint is
given in equation (\ref{id:flso}).
%\begin{eqnarray*}
%\fls{o}(x;y) & \triangleq & (x=y : \emp) \\
%& \lor & \big(\exists w\cdot x < w : \fck(x;w) \sepc \fls{o}(w;y)\big)
%\end{eqnarray*}
Using this predicate, the property is specified by:
\begin{equation}
\fls{o}(x;y) \sepc \fck(y;nil)
\end{equation}
Notice that we specify a list with at least one element because
the ordering constraint is not satisfied for $nil$ which is the smallest
value for locations.

\item \emph{The free list is acyclic and split according the first-fit policy by the request of size $r$.}
We collect the first-fit policy by the predicate $\fls{<}$ which has one static parameter used to bound the size of each chunk:
\begin{eqnarray*}
\fls{<}(x;y;v) & \triangleq & (x=y : \emp) \\
& \lor & \big(\exists w\cdot fsz(x) < v : \fck(x;w) \sepc \fls{<}(w;y;v))
\end{eqnarray*}
The invariant is specified using both $\fls{<}$ and $\fls{}$:
\begin{equation}
\fsz(Y) \ge r : \fls{<}(x;y;r) \sepc \fck(y;z) \sepc \fls{}(z;nil)
\end{equation}

\item \emph{The free list is acyclic and split according the best-fit policy by the request of size $r$.}
The predicate $\fls{bf}$ is used to express that the chunks inside the list segment are not the best choice for the request $v$ compared with the best-fit chunk which size is $v'$:
\begin{eqnarray*}
\fls{bf}(x;y;v,v') & \triangleq & (x=y : \emp) \\
& \lor & \big(\exists w\cdot fsz(x) < v : \fck(x;w) \sepc \fls{bf}(w;y;v,v'))\\
& \lor & \big(\exists w\cdot fsz(x) > max(v,v') : \fck(x;w) \sepc \fls{bf}(w;y;v,v'))
\end{eqnarray*}
The invariant is specified using the above predicate:
\begin{equation}
b=\fsz(y) \land b > r : \fls{bf}(x;y;r,b) \sepc \fck(y;z) \sepc \fls{bf}(z;nil;r,b)
\end{equation}
\end{itemize}

