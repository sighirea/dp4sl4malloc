%!TEX root = main-dma.tex

\section{Specifying Memory Allocators}
\label{sec:overview}


Memory allocators manage the heap part allocated to the process.
They usually organize the heap into a set of non-overlapping chunks and some memory used to manage this set. 
\begin{wrapfigure}{R}{0.45\textwidth}
\centering
\vspace{-4eX}
\includegraphics[trim=2cm 2cm 2cm 1cm,scale=0.4]{../figs/fig_dma_chk.pdf}
\caption{Heap-list in DMA}
\label{fig:heap}
\vspace{-4eX}
\end{wrapfigure}
In this work, we consider the organization of the set as a heap-list, i.e., a list stored in a raw array of bytes, where elements are in sequence in this block and the successor relation is given by the element size, see Figure~\ref{fig:heap}.
To obtain the heap-list data structures, DMA managers split each chunk 
in two parts:
(i) the header containing management information, at least the chunk size including the header and its status -- occupied by user data or free to be allocated,
(ii) the body intended to store user's data (payload).

The heap list data structure may be seen as a continuous block of values (suppose that values are all integers) with some properties about its content, for example:
(i) the size of the block is the sum of values contained at some positions in the block,
(ii) the status of two adjacent chunks are not both free (early coalescing policy).
Therefore, to capture these properties, we cannot use only a predicate $\blk(x;y)$ denoting a sequence of values stating at location $x$ and ending just before location $y$, i.e., at $y-1$. We propose to use a special predicate called $\hls{}$ defined inductively by the first two rules below  as a list segment of chunks, each chunk being specified by the predicate $\hck$:
\begin{eqnarray}
\hls{}(x;y) & \Leftarrow & x=y : \emp \label{id:hls-emp}
\\
\hls{}(x;y) & \Leftarrow & \exists w\cdot \hck(x;w)\sepc\hls{}(w;y) \label{id:hls-rec}
\\
\hck(x;y) & \Leftarrow & \exists sz,st,nx\cdot\begin{array}[t]{l}
 		sz > 3 \land y=x+sz \land 0\leq st \leq 1: \\
 		x\pto sz \sepc x+1\pto st \sepc x+2\pto nx 
		\sepc\ \blk(x+3;x+sz)  %%\blk(x,3,sz)
		\end{array}\label{id:hck}
\end{eqnarray}
The chunk is specified using $\blk$ predicate to denote the body of the chunk, and points-to atoms $t\pto t'$ to obtain the value stored in the header. Notice the use of pointer arithmetics to access the chunk fields and to obtain the start location of the next chunk in the heap.
%%\zhilin{Is $\blk(x,3,sz)$ really better than $\blk(x+3, sz)$ ?}
%%MS: is  better than $\blk(x+3, x+sz)$


The heap-lists with no successive free chunks used in early coalescing policy are captured by an ID which employs the integer parameters $z$ and $z'$ to keep track of the status of chunks before the heap-list segment boundaries $x$ and $y$:
\begin{eqnarray}
\hls{c}(x,z;y,z') & \Leftarrow & x=y \land z=z' : \emp \label{id:hlsc-emp}\\
\hls{c}(x,z;y,z') & \Leftarrow & \exists w,z''\cdot \begin{array}[t]{l}
				z\neq\fif(x) \land z''=\fif(x) : \\
				\hck(x;w) \sepc\ \hls{c}(w,z'';y,z') 
				\end{array} \label{id:hlsc-rec}
\end{eqnarray}
where the term $\fif(x)$ denotes the value of the status field (at offset $+1$ from $x$), which may be 0 (for occupied chunk) or 1 (for free chunk).
Similar definitions are used to capture other properties of allocators' configurations that don't use a special list for the set of free chunks. We provide such predicates in Section~\ref{sec:slid-hls} to specify allocation policies, \eg, best-fit or first-fit.

\medskip
The following operations of the DMA are used to implement the allocation and free methods:
\begin{itemize}
\item searching a free chunk which fits (has size bigger than) a requested size,

\item splitting a free chunk whose size is (much) bigger than the requested size into 
    two chunks where one of them has the requested size, and
\item joining two contiguous free chunks.
\end{itemize}

The first two operations are used to fulfill an allocation request while the third one may be used during the freeing request (in DMA with early collapsing)
or during the allocation (in DMA with lazy collapsing).

The pre/post-condition specifications of these operations 
are given in Figure~\ref{fig:specs}
for a single linked heap-list, specified using the $\hls{}(x;y)$ predicate.
The post-condition is the strongest one.
%
The first post-condition of \texttt{search} specifies the case of a list where there is a chunk (returned as result) which size is bigger than \texttt{sz}. 
This case may be further refined depending on the policy used 
(first fit, best fit) to return the chunk with the fitting size.
The second post-condition specifies that all the chunks in the 
heap-list have a size less than \texttt{sz}.
%
The splitting generates a new chunk with the requested size at the end of the initial chunk; the new chunk is set to be used (not free).

\begin{figure}
$
\begin{array}{l}
\{ \hls{}(st;end) \} \\
\texttt{search}(sz) ~\texttt{returns}~b \\
\left\{\begin{array}{ll}
  & b \neq nil \limp \exists z \cdot size(b) \geq sz : \hls{}(st;b) \sepc \hck(b;z) \sepc \hls{}(z;end) \\
  \land &  b = nil \limp \hls{<}(st;end;sz)
  \end{array}\right\}
\end{array}
$

\vspace{2eX}
$
\begin{array}{l}
\{ s > sz > 3 : y \pto s \sepc y+1 \pto 1 \sepc y+2 \pto nx \sepc \blk(y+3;z) \} \\
\texttt{split}(y,sz) ~\texttt{returns}~u \\
\left\{\begin{array}{ll}
  & y \pto s - sz \sepc y+1 \pto 1 \sepc y+2 \pto nx \sepc \blk(y+3;u) \sepc \\
  & u \pto sz \sepc u+1 \pto 0 \sepc u+2 \pto 0 \sepc \blk(u+3;z)
  \end{array}\right\}
\end{array}
$

\vspace{2eX}
$
\begin{array}{l}
\left\{\begin{array}{l}
   \hls{}(st;y) \sepc
   y \pto sz_y \sepc y+1\pto 1 \sepc y+2 \pto nx_y  \sepc \blk(y+3;z) \\
   \sepc z \pto sz_z \sepc z+1\pto 1 \sepc z+2 \pto nx_z \sepc \blk(z+3;u) \sepc \hls{}(u;end)
   \end{array}\right\} \\
\texttt{join}(y,z) \\
\{ \hls{}(st;y) \sepc 
   y \pto sz_y + sz_z \sepc y+1\pto 1 \sepc y+2 \pto nx_y \sepc \blk(y+3;u)
   \sepc \hls{}(u;end)
\}
\end{array}
$

\caption{Specification of DMA basic operations}
\label{fig:specs}
\end{figure}

To prove the correctness of the implementation of these operations or
of allocations and freeing functions calling them, 
we have to prove the validity of entailments like:
\begin{eqnarray}
post_\texttt{join} & \limp & \hls{}(st;end) \label{eq:post-join}
\\[2mm]
post_\texttt{split} & \limp & \hls{}(y;z) \label{eq:post-split}
\\[2mm]
\exists t \cdot sz(t) \le sz : \hls{}(st;t) \sepc \hck(t;y) \sepc \hls{}(y;end)
	& \limp & \hls{}(st;y) \sepc \hls{}(y;end) \label{eq:search-loop}
\end{eqnarray}
The last entailment is used to prove some invariant of the searching loop.
