(set-logic QF_SLID_LC)

;; declare fields
(declare-fun f () (Field Int Int))

;; declare variables
(declare-fun t1 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun z1 () Int)
(declare-fun z2 () Int)


(assert (and
		(< t1 x1)
		(tobool (ssep
			(pto t1 (ref f 3))
			(blk x1 x2)
			(hck y1 y2)
			(hls z1 z2)
			)
		)
	)
)

(assert (not
		(and
			(< t1 x1)
			(tobool (ssep
				(pto t1 (ref f 3))
				(blk x1 x2)
				(hck y1 y2)
				(hls z1 z2)
				)
			)
		)
	)
)

(check-sat)

