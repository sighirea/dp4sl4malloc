(set-logic QF_SLID_LC)

;; declare fields
(declare-fun f () (Field Int Int))

;; declare variables
(declare-fun t1 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun z1 () Int)
(declare-fun z2 () Int)


(assert (and
		(= t1 x2)
		(= x1 z2)
		(tobool (ssep
			(pto t1 (ref f 3))
			(blk x1 x2)
			(hls z1 z2)
			)
		)
	)
)

(assert (not
		(and
			(> t1 z1)
			(tobool (ssep
				(blk z1 z2)
				(blk z2 t1)
				)
			)
		)
	)
)

(check-sat)

;;false,abs(A) \vDash abs(B) z2>z1
