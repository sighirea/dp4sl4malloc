(set-logic QF_SLID_LC)

;; declare fields
(declare-fun f () (Field Int Int))


;; 256 <= oldsz &
;; y = hst + oldsz &
;; t = hst + oldsz
;; :
;; integer(hst,oldsz) *
;; blk(hst+1,y) *
;; hls(y,hen)
;; 
;; ==>
;; 
;; hck(hst, oldsz, t) *
;; hls(t, hen)

;; declare variables
(declare-fun hst () Int)
(declare-fun hen () Int)

(declare-fun x0 () Int)
(declare-fun sz0 () Int)
(declare-fun y () Int)
(declare-fun t () Int)

(assert (and
		(< 256 sz0)
		(= hst x0)
		(= y (+ x0 sz0))
		(= t (+ x0 sz0))
		(tobool (ssep
			(pto x0 (ref f sz0))
			(blk (+ x0 1) y)
			(hls y hen)
			)
		)
	)
)

(assert (not
		(tobool (ssep
			(pto hst (ref f sz0))
			(blk (+ hst 1) t)
			(hls y hen)
			)
		)
	)
)

(check-sat)

