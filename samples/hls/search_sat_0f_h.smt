(set-logic QF_SLID_LC)
;; size field
(declare-fun sz () (Field Int Int))
;; variables
(define-fun hst () Int)
(define-fun hen () Int)
;; heap chunk
(define-fun hck ((?x Int) (?y Int)) Space
	(tospace (exists ((?sz Int))
		(and
			(> ?sz 2)
			(= ?y (+ ?x ?sz))
			(tobool	(ssep
					(pto ?x (ref sz ?sz))
					(blk (+ ?x 1) ?y)
					)
		)
		)
	))
)
;; heap list
(define-fun hls ((?x Int) (?y Int)) Space
	(tospace (or
		(and (= ?x ?y) (tobool emp))
		(exists ((?w Int))
			(tobool	(ssep
					(hck ?x ?w)
					(hls ?w ?y)
					)
		))
	))
)

(declare-fun rsz () Int)
(declare-fun x0 () Int)
(declare-fun sz0 () Int)
(declare-fun xnxt () Int)

(assert
	(and
	(distinct hst hen)
	(= hst x0)
	(= (+ x0 sz0) xnxt)
	(>= sz0 rsz)
	(tobool (ssep
		(hck x0 xnxt)
		(hls xnxt hen)
		)
	)
	)	)
;; end of problem
(check-sat)
