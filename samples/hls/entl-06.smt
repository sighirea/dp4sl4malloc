(set-logic QF_SLID_LC)

;; declare fields
(declare-fun f () (Field Int Int))

;; declare variables
(declare-fun t1 () Int)
(declare-fun t2 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)


(assert (and
		(< t1 t2)
		(tobool (ssep
			(pto t1 (ref f 10))
			(pto t2 (ref f 11))
			)
		)
	)
)

(assert (not
		(and
			(< x1 x2)
			(tobool
				(blk x1 x2)
			)
		)
	)
)

(check-sat)
;;false abs(A) entl abs(B) is false
