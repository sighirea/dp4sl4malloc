(set-logic SLALIA)
(set-info :source ||)
(set-info :smt-lib-version 2.6)
(set-info :category "crafted")
(set-info :status sat)
(set-info :version "2020-07-02")

; Sort of locations
(declare-sort Loc 0)

; Ops on Loc
(declare-fun plusLoc ((x Loc) (c Int)) Loc)
(declare-fun ltLoc ((x Loc) (y Loc)) Bool)

; Sort of values
(declare-datatypes (
       (Val 0)
       ) (
       ((vInt (val Int)) (vBool (val Bool)) (vLoc (val Loc))
       )
)

; Type of heap
(declare-heap (Loc Val)
)

; Predicate definitions
;; Memory Block
(define-fun-rec blk ((x Loc)(y Loc)) Bool
            (or
                (and (= x (plusLoc y 1)) (pto x _))
                (and (ltLoc x (plusLoc y 1)) (sep (pto x _) (blk (plusLoc x 1) y)))
            )
)

;; Heap chunk
(define-fun hck ((x Loc)(y Loc) Bool
            (exists ((sz Int) (st Bool))
              (and
                 (< 3 sz)
                 (= y (plusLoc x sz))
              (sep
                 (pto x (vInt sz))
                 (pto (plusLoc x 1) (vBool st))
                 (blk (plusLoc x 3) y)
              )
              )
            )
)

;; Heap list
(define-fun-rec hls ((x Loc)(y Loc)) Bool
           (or
              (and (= x y) (_ emp Loc Val))

              (exists ((w Loc))

                  (and (ltLoc x y)   (sep (hck x w) (hls w y)))

              )
           )
)

; Check definitions???
(check-sat)

; Variables
(declare-const x Loc)

