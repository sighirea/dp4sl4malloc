
(*
sz > 256 &
!(a = 0) &
!(sz = 0) &
!(sz = 1) :
integer(a, sz) *
blk(a+1,a+sz)

==>

hls(a, a+sz)
*)
