(set-logic QF_SLID_LC)

;; declare fields
(declare-fun f () (Field Int Int))

;; 256 <= szy &
;; z = (y + szy) &
;; 256 <= szz &
;; u = z + szz 
;; :
;; integer(y,szy+szz) *
;; blk(y+1,z) *
;; integer(z,szz) *
;; blk(z+1,u)
;; 
;; ==>
;; 
;; hck(y, szy+szz, u)

;; declare variables
(declare-fun y () Int)
(declare-fun szy () Int)
(declare-fun z () Int)
(declare-fun szz () Int)
(declare-fun u () Int)

(assert (and
		(< 256 szy)
		(= z (+ y szy))
		(< 256 szz)
		(= u (+ z szz))
		(tobool (ssep
			(pto y (ref f (+ szy szz)))
			(blk (+ y 1) z)
			(pto z (ref f szz))
			(blk (+ z 1) u)
			)
		)
	)
)

(assert (not
		(tobool (ssep
			(pto y (ref f (+ szy szz)))
			(blk (+ y 1) u)
			)
		)
	)
)

(check-sat)

