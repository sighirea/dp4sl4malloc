
(*

sz + MIN_SIZE <= szy &
sz >= MIN_SIZE &
nv = y + sz - sz &
z = y + szy
:
integer(y,szy-sz) *
blk(y+1,y+szy-sz) *
integer(nc, sz) *
blk(nc+1,z)

==>

hck(y, szy-sz,nc) *
hck(nc, sz, z)

*)
