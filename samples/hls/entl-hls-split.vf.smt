(set-logic QF_SLID_LC)

;; declare fields
(declare-fun f () (Field Int Int))

;; sz + MIN_SIZE <= szy &
;; sz >= MIN_SIZE &
;; nc = y + szy - sz &
;; z = y + szy
;; :
;; integer(y,szy-sz) *
;; blk(y+1,y+szy-sz) *
;; integer(nc, sz) *
;; blk(nc+1,z)
;; 
;; ==>
;; 
;; hck(y, szy-sz,nc) *
;; hck(nc, sz, z)

;; declare variables
(declare-fun sz () Int)
(declare-fun y  () Int)
(declare-fun szy () Int)
(declare-fun z  () Int)
(declare-fun nc () Int)

(assert (and
		(< 256 sz)
		(< 256 szy)
		(< (+ sz 256) szy)
		(= z (+ y szy))
		(= nc (+ y (- szy sz)))
		(tobool (ssep
			(pto y (ref f (- szy sz)))
			(blk (+ y 1) (+ y (- szy sz)))
			(pto nc (ref f sz))
			(blk (+ nc 1) z)
			)
		)
	)
)

(assert (not
		(tobool 
			(hls y z)
		)
	)
)

(check-sat)

