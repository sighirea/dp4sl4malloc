(set-logic QF_SLID_LC)

;; declare fields
(declare-fun f () (Field Int Int))

;; (sz > 256 &
;; !(a = 0) &
;; !(sz = 0) &
;; !(sz = 1) :
;; pto(a, sz) *
;; blk(a+1,a+sz)
;; )
;; ==>
;; hls(a, a+sz)

;; declare variables
(declare-fun sz () Int)
(declare-fun a  () Int)

(assert (and
		(< 256 sz)
		(distinct a 0)
		(distinct sz 0)
		(distinct sz 1)
		(tobool (ssep
			(pto a (ref f sz))
			(blk (+ a 1) (+ a sz))
			)
		)
	)
)

(assert (not
		(tobool
			(hls a (+ a sz))
		)
	)
)

(check-sat)

