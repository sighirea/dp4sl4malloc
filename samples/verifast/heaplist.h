#ifndef HEAPLIST_H
#define HEAPLIST_H

//@ #include "listex.gh"        // Useful lemmas on lists
//@ #include "vflist.gh"        // Additional predicates and lemmas on lists

#define MIN_SIZE 256

/*@
fixpoint bool is_suffix_of<t>(list<t> xs, list<t> ys) {
    switch (ys) {
        case nil: return xs == ys;
        case cons(y, ys0): return xs == ys || is_suffix_of(xs, ys0);
    }
}

predicate hls(int *st, int *end; list<int*> vs);
predicate hck(int *c; unsigned int sz, int *cnx);
@*/

int *create_heaplist(unsigned int sz) ;
  //@ requires sz > MIN_SIZE;
  //@ ensures hls(result, result+sz, cons(result, nil));

int *search_heaplist(int *st, int *end, unsigned int sz);
  //@ requires hls(st, end, ?vs) &*& MIN_SIZE <= sz &*& end > st + sz &*& 0 < length(vs);
  //@ ensures hls(st, result, ?vs1) &*& result == end ? vs1 == vs : (hls(result, end, ?vs2) &*& vs == append(vs1, vs2));
  //TODO: introduce policy
  //TODO: failure case

int* split_chunk(int *y, unsigned int sz);
  //@ requires hck(y, ?szy, ?z) &*& sz + MIN_SIZE <= szy &*& sz >= MIN_SIZE;
  //@ ensures hck(y, szy-sz, result) &*& hck(result, sz, z);

void join_chunk(int *y, int*z);
  //@ requires hck(y, ?szy, z) &*& hck(z, ?szz, ?u);
  //@ ensures hck(y, szy+szz, u);

int *join_chunklist(int *st, int* end);
  //@ requires hck(st, ?oldsz, ?y) &*& hls(y, end, ?vs) &*& 0 < length(vs);
  //@ ensures hck(st, ?newsz, result) &*& hls(result, end, ?vso) &*& is_suffix_of(vso, vs) == true;

#endif
