#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "heaplist.h"

/*
predicate integer(p,v) = *p |-> v defined in bin/prelude.h and at page 14 VeriFast manual
 */
 
/*
lemma void malloc_block_split(void *p, int nsz)
  requires malloc_block(p, ?sz) &*& sz > nsz &*& nsz > 0;
  ensures malloc_block(p, nsz) &*& malloc_block(p+nsz, sz-nsz);
 {
    // TODO
 }
 
lemma void malloc_block_ints_split(int* c, int nsz) 
  requires malloc_block_ints(c, ?sz) &*& sz > nsz;
  ensures malloc_block_ints(c, nsz) &*& malloc_block_ints(c+nsz, sz-nsz);
{
  // TODO
}
*/

/*@
predicate hck(int *c; unsigned int sz, int* cnx) =
    *c |-> sz &*&
    sz >= MIN_SIZE &*&
    cnx == c+sz &*& 
    ints(c+1, sz-1, _) &*& malloc_block_ints(c,sz);
    
lemma void assert_hck(int *c, int *cnx) 
    requires hck(c, _, cnx);
    ensures  hck(c, _, cnx) &*& c < cnx;
{
    open hck(c, _, cnx);
    assert c < cnx;
    close hck(c, _, cnx);
}

@*/

/*@
predicate hls(int *st, int *end; list<int*> vs) =
    st == end ?
        vs == nil
    :
        hck(st, _, ?cnx) &*& hls(cnx, end, ?vstail)
        &*& vs == cons(st, vstail);

lemma void assert_hls(int *st, int *end) 
    requires hls(st, end, ?vs0);
    ensures hls(st, end, vs0) &*& st <= end;
{
  if (st == end)
  {
    true;
  } else {
    open hls(st, end, vs0);
    open hck(st, _, ?cnx);
    assert st < cnx;
    assert_hls(cnx, end);
    close hck(st, _, cnx);
    close hls(st, end, vs0);
  }
}

@*/
/*

lemma void close_hls_snoc(int *n1, int* n3)
    requires hls(n1, ?n2, ?vs) &*& hck(n2, _, _, n3);
    ensures hls(n1, n3, snoc(vs, n2));
{
  switch(vs){
    case nil: {
      open hls(n1, n2, nil);
      open hck(n2, _, _, n3);
      assert n2 < n3;
      close hck(n2, _, _, n3);
      close hls(n1, n3, snoc(nil, n2));
    }
    case cons(x, vst): {
      open hls(n1, n2, cons(x, vst));
      assert x == n1;
      assert hck(n1, _, _, ?n1p);
      open hck(n1, _, _, n1p);
      // assert_hck(n1, n1p);
      assert n1 < n1p;
      // close hck(n1, _, _, _);
      assert hls(n1p, n2, vst);
      assert_hls(n1p, n2);
      assert n1p <= n2;
      assert_hck(n2, n3);
      assert n2 < n3;
      close_hls_snoc(n1p, n3);
      // close hck(n2, _, _, n3);
      close hls(n1, n3, cons(x, snoc(vst,n2)));
    }
  }
}

*/

int *create_heaplist(unsigned int sz)
  //@ requires sz > MIN_SIZE;
  //@ ensures hls(result, result+sz, cons(result, nil));
{
  int *a = malloc(sz * sizeof(int));
  if (a == 0) abort();
  //@ open ints(a, sz, _);
  *a = (int) sz;
  //@ close ints(a+1, sz-1, _);
  //@ close hck(a, sz, a+sz);
  return a;
}


int* split_chunk(int *y, unsigned int sz)
  //@ requires hck(y, ?szy, ?z) &*& sz + MIN_SIZE <= szy &*& sz >= MIN_SIZE;
  //@ ensures hck(y, szy-sz, result) &*& hck(result, sz, z);
{
  //@ open hck(y, szy, _);
  int* nc = y + szy - sz;
  y[0] = y[0] - (int) sz;
  // ints_split(c+1, szy-1-sz);
  //@ close hck(y, szy-sz, _);
  nc[0] = sz;
  //@ close hck(nc, sz, _);
  return nc;
}


int *search_heaplist(int *st, int *end, unsigned int sz)
  /*@ requires hls(st, end, ?vs) &*& 
               MIN_SIZE <= sz &*& 
               end > st + sz &*& 
               0 < length(vs);
  @*/
  /*@ ensures hls(st, result, ?vs1) &*& 
              result == end ? (vs1 == vs)
              : (hck(result, ?szr, ?cnx) &*& 
                 szr > sz &*&
                 hls(cnx, end, ?vs2) &*& 
                 vs == append(vs1, cons(result, vs2)));
  @*/
  //TODO: introduce policy as predicate of hls
{
  int *t = st;
  bool found = false;
  while(t < end && ! found) 
    /*@ invariant hls(st, t, ?vs1p) &*& 
              hls(t, end, ?vs2p) &*& 
              vs == append(vs1p, vs2p);
     @*/
  {
    //@ assert !(t == end);
    //@ open hls(t, end, vs2p);
    //@ assert !(vs2p == nil);
    //@ open hck(t, ?szt, _, ?u);
    //@ assert t + szt == u;
    if (((unsigned int) t[0]) > sz) { // && t[1] == 1) {
      found = true;
      //@ close hck(t, _, _, _);
      // close hls(t, end,  vs2p);
    } else {
      //@ int* old_t = t;
      t = t + t[0];
      //@ close hck(old_t, _, _, u);
      //@ assert t == u;
      //@ close hls(u, end,  _);
      //@ close hls(st, u, snoc(vs1p,old_t));
    }
  }
  if (found == true) {
    return t;
    //@ open hls(t, end,  _);
  } else {
    //@ assert t >= end;
    return end;
  }
}

void join_chunk(int *y, int*z)
  //@ requires hck(y, ?szy, z) &*& hck(z, ?szz, ?u);
  //@ ensures hck(y, szy+szz, u);
{
  //@ open hck(y, _, _);
  //@ open hck(z, _, _);
  y[0] = y[0] + z[0];
  // ints_join(z, z+1);
  // ints_join(y+1, z);
  //@ close hck(y,_, _);
}

int *join_list(int *st, int* end)
  //@ requires hck(st, ?oldsz, ?y) &*& hls(y, end, ?vs) &*& 0 < length(vs);
  //@ ensures hck(st, ?newsz, end);
  // newsz = oldsz + sum_size(vs)
{
  //@ open hck(st, _, _);
  int* t = st + st[0];
  while (t < end)
    /*@
    invariant hck(st, ?sz, t) &*& hls(t, end, ?vst); // &*& is_suffix_of(vst, vs)==true; 
    @*/
    // sz = oldsz + sum_size(vs) - sum_size(vst)
  { 
    //@ open hck(st, _, _);
    //@ open hls(t, end, _);
    //@ open hck(t, ?szt, ?u);
    st[0] = st[0] + t[0];
    t = t + t[0];
    //@ close hck(st, _, t);
  }
  return st;
}




