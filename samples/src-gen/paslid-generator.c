/**
 *  Generator of satisfiability and entailment s for PASLID
 *
 *  @author MS
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

/** Types */

/** Source of  generated */
enum kind_e {
  JOIN, SEARCH, RANDOM, ALL
};

/** Global options */
static bool isent = false;
static enum kind_e program = ALL;
static bool use_hck = false;

/**
 * Utility printing start of SMTLIB files
 */
void
print_prelude(FILE * f)
{
  fprintf(f, "(set-logic QF_SLID_LC)\n");
  /* size field */
  fprintf(f, ";; size field\n");
  fprintf(f, "(declare-fun sz () (Field Int Int))\n");
  /* TODO: other fields or predicates */
  /* variables: heap start and end */
  fprintf(f, ";; variables\n");
  fprintf(f, "(define-fun hst () Int)\n");
  fprintf(f, "(define-fun hen () Int)\n");
  /* predicate hck */
  fprintf(f, ";; heap chunk\n");
  fprintf(f, "(define-fun hck ((?x Int) (?y Int)) Space\n");
  fprintf(f, "\t(tospace (exists ((?sz Int))\n");
  fprintf(f, "\t\t(and\n\t\t\t(> ?sz 2)\n");
  fprintf(f, "\t\t\t(= ?y (+ ?x ?sz))\n");
//TODO:add status field
    // fprintf(f, "\t\t\t(<= 0 ?st) (<= ?st 1)\n");
  fprintf(f, "\t\t\t(tobool\t(ssep\n");
  fprintf(f, "\t\t\t\t\t(pto ?x (ref sz ?sz))\n");
  fprintf(f, "\t\t\t\t\t(blk (+ ?x 1) ?y)\n");
  fprintf(f, "\t\t\t\t\t)\n\t\t)\n\t\t)\n\t))\n)\n");
  /* predicate hls */
  fprintf(f, ";; heap list\n");
  fprintf(f, "(define-fun hls ((?x Int) (?y Int)) Space\n");
  fprintf(f, "\t(tospace (or\n");
  fprintf(f, "\t\t(and (= ?x ?y) (tobool emp))\n");
  fprintf(f, "\t\t(exists ((?w Int))\n");
  fprintf(f, "\t\t\t(tobool\t(ssep\n");
  fprintf(f, "\t\t\t\t\t(hck ?x ?w)\n");
  fprintf(f, "\t\t\t\t\t(hls ?w ?y)\n");
  fprintf(f, "\t\t\t\t\t)\n\t\t))\n\t))\n)\n\n");
}

/**
 * Utility printing end of SMTLIB files
 */
void
print_check(FILE * f)
{
  fprintf(f, ";; end of problem\n");
  fprintf(f, "(check-sat)\n");
}

/**
 *  Generate sat/entl s for unrolling loop of chunk join
 * - sat: pto blk (pto blk)^n hls
 * - ent: pto blk (pto blk)^n hls ==> pto blk hls
 *
 *  @param fname output files prefix
 *  @param n     unroll of the loop
 *  @return      number of successful files generated
 */
int
join_unroll(char *fname, short n)
{
  int res = 0;
  size_t max_fname_full = strlen(fname) + 4 + 3 + 4 + 2 + 1;
  char *fname_full = (char *)malloc(max_fname_full * sizeof(char));

  for (short i = 1; i < n; i++) {
    snprintf(fname_full, max_fname_full, "%s_%s_%x%s.smt",
	     fname, (isent == true) ? "ent" : "sat",
	     i, (use_hck == true) ? "_h" : "");
    FILE *f = fopen(fname_full, "w");
    if (f == NULL)
      continue;
    print_prelude(f);
    /* iteration vars, current variable is xi */
    for (short k = 0; k <= i; k++) {
      fprintf(f, "(declare-fun x%x () Int)\n", k);
      fprintf(f, "(declare-fun sz%x () Int)\n", k);
    }
    /* next chunk of xi */
    fprintf(f, "(declare-fun xnxt () Int)\n");
    /* full size */
    fprintf(f, "(declare-fun szall () Int)\n");

    /* start assert-and */
    fprintf(f, "\n(assert\n\t(and\n");
    /* pure part: start */
    /* szall = sum of szi */
    fprintf(f, "\n\t(= szall (+");
    for (short k = 0; k <= i; k++) {
      fprintf(f, " sz%x", k);
    }
    fprintf(f, "))\n");
    /* chunks are inline until xnxt */
    fprintf(f, "\t(= hst x0)\n");
    for (short k = 0; k < i; k++) {
      fprintf(f, "\t(= (+ x%x sz%x) x%x)\n", k, k, k + 1);
      /* TODO: add free status randomly */
    }
    fprintf(f, "\t(= (+ x%x sz%x) xnxt)\n", i, i);
    /* pure part: end */
    /* spatial part: begin */
    fprintf(f, "\t(tobool (ssep\n");
    /* chunks until xnxt */
    fprintf(f, "\t\t(pto x0 (ref sz szall))\n");
    fprintf(f, "\t\t(blk (+ x0 1) x1)\n");
    for (short k = 1; k < i; k++) {
      if (use_hck == false) {
	fprintf(f, "\t\t(pto x%x (ref sz sz%x))\n", k, k);
	fprintf(f, "\t\t(blk (+ x%x 1) x%x)\n", k, k + 1);
      } else {
	fprintf(f, "\t\t(hck x%x x%x)\n", k, k + 1);
      }
    }
    if (use_hck == false) {
      fprintf(f, "\t\t(pto x%x (ref sz sz%x))\n", i, i);
      fprintf(f, "\t\t(blk (+ x%x 1) xnxt)\n", i);
    } else {
      fprintf(f, "\t\t(hck x%x xnxt)\n", i);
    }
    fprintf(f, "\t\t(hls xnxt hen)\n");
    /* spartial part: end */
    fprintf(f, "\t\t)\n\t)\n");
    /* end assert-and */
    fprintf(f, "\t)\t)\n");

    if (isent == true) {
      /* start assert-not */
      fprintf(f, "(assert (not\n");
      fprintf(f, "\t(tobool (ssep\n");
      if (use_hck == false) {
	fprintf(f, "\t\t(pto hst (ref sz szall))\n");
	fprintf(f, "\t\t(blk (+ hst 1) xnxt)\n");
      } else {
	fprintf(f, "\t\t(hck hst xnxt)\n");
      }
      fprintf(f, "\t\t(hls xnxt hen)\n");
      /* end assert-not */
      fprintf(f, "\t)\t)\n");
      fprintf(f, "))\n");
    }
    print_check(f);
    fclose(f);
    /* TODO: update res correctly */
    res++;
  }
  free(fname_full);
  return res;
}

/**
 *  Generate sat/entl s for unrolling loop of chunk search
 * - sat: (pto blk)^{n+1} hls
 * - ent: (pto blk)^{n+1} hls ==> hls
 *
 *  @param fname output files prefix
 *  @param n     unroll of the loop
 *  @param found size constraint satisfied?
 *  @return      number of successful files generated
 */
int
search_unroll(char *fname, short n, bool found)
{
  int res = 0;
  size_t max_fname_full = strlen(fname) + 4 + 3 + 1 + 4 + 2 + 1;
  char *fname_full = (char *)malloc(max_fname_full * sizeof(char));
  for (short i = 0; i < n; i++) {
    snprintf(fname_full, max_fname_full, "%s_%s_%x%s%s.smt",
	     fname, (isent == true) ? "ent" : "sat", i,
	     (found == true) ? "f" : "n", (use_hck == true) ? "_h" : "");
    FILE *f = fopen(fname_full, "w");
    if (f == NULL)
      continue;
    print_prelude(f);
    /* parameter: size */
    fprintf(f, "(declare-fun rsz () Int)\n");
    /* iteration vars, current variable is xi */
    for (short k = 0; k <= i; k++) {
      fprintf(f, "(declare-fun x%x () Int)\n", k);
      fprintf(f, "(declare-fun sz%x () Int)\n", k);
    }
    /* next chunk of xi */
    fprintf(f, "(declare-fun xnxt () Int)\n");
    /* start assert-and */
    fprintf(f, "\n(assert\n\t(and\n");
    /* pure part: start */
    /* not empty heap */
    fprintf(f, "\t(distinct hst hen)\n");
    /* x0 is hst */
    fprintf(f, "\t(= hst x%x)\n", 0);
    /* chunks are inline until xnxt and their size < rsz */
    for (short k = 0; k < i; k++) {
      fprintf(f, "\t(= (+ x%x sz%x) x%x)\n", k, k, k + 1);
      fprintf(f, "\t(< sz%x rsz)\n", k);
      /* TODO: add free status randomly */
    }
    fprintf(f, "\t(= (+ x%x sz%x) xnxt)\n", i, i);
    fprintf(f, "\t(%s sz%x rsz)\n", (found == true) ? ">=" : "<", i);
    /* TODO: add free status randomly */
    /* pure part: end */
    /* spatial part: begin */
    fprintf(f, "\t(tobool (ssep\n");
    /* chunks until xnxt */
    for (short k = 0; k < i; k++) {
      if (use_hck == true) {
	fprintf(f, "\t\t(hck x%x x%x)\n", k, k + 1);
      } else {
	fprintf(f, "\t\t(pto x%x (ref sz sz%x))\n", k, k);
	fprintf(f, "\t\t(blk (+ x%x 1) x%x)\n", k, k + 1);
      }
    }
    if (use_hck == true) {
      fprintf(f, "\t\t(hck x%x xnxt)\n", i);
    } else {
      fprintf(f, "\t\t(pto x%x (ref sz sz%x))\n", i, i);
      fprintf(f, "\t\t(blk (+ x%x 1) xnxt)\n", i);
    }
    /* end in a hls */
    fprintf(f, "\t\t(hls xnxt hen)\n");
    /* spartial part: end */
    fprintf(f, "\t\t)\n\t)\n");
    /* end assert-and */
    fprintf(f, "\t)\t)\n");

    if (isent == true) {
      /* start assert-not */
      fprintf(f, "(assert (not\n");
      fprintf(f, "\t(tobool (ssep\n");
      fprintf(f, "\t\t(hls hst xnxt)\n");
      fprintf(f, "\t\t(hls xnxt hen)\n");
      /* end assert-not */
      fprintf(f, "\t)\t)\n");
      fprintf(f, "))\n");
    }
    print_check(f);
    fclose(f);
    /* TODO: update res correctly */
    res++;
  }
  free(fname_full);
  return res;
}

/**
 *  Generate sat/entl s with random shape
 * - sat: hls (pto blk)^{r} hls (pto blk)^{r} ... hls
 * - ent: sat ==> hls * hls
 *
 *  @param fname output files prefix
 *  @param n     n+1 number of hls predicates
 *  @return      number of successful files generated
 */
int
random_unroll(char *fname, short n)
{
  int res = 0;
  size_t max_fname_full = strlen(fname) + 4 + 3 + 3 + 4 + 2 + 1;
  char *fname_full = (char *)malloc(max_fname_full * sizeof(char));
  /* random number of not hls atoms between hls */
  srandom(time(NULL));
  /* generate n+1 files */
  for (int i = 0; i <= n; i++) {
    short r = random() % (n + 1);
    snprintf(fname_full, max_fname_full, "%s_%s_%x-%x.smt",
	     fname, (isent == true) ? "ent" : "sat", n, r);
    FILE *f = fopen(fname_full, "w");
    if (f == NULL)
      continue;
    print_prelude(f);
    /* hls vars */
    for (short k = 0; k <= n; k++) {
      fprintf(f, "(declare-fun x%x () Int)\n", k);
      fprintf(f, "(declare-fun y%x () Int)\n", k);
    }
    /* pto and blk vars */
    for (short j = 0; j < n; j++)
      for (short k = 0; k <= r; k++) {
	fprintf(f, "(declare-fun z%x_%x () Int)\n", j, k);
	fprintf(f, "(declare-fun sz%x_%x () Int)\n", j, k);
	fprintf(f, "(declare-fun u%x_%x () Int)\n", j, k);
	fprintf(f, "(declare-fun v%x_%x () Int)\n", j, k);
      }

    /* start assert-and */
    fprintf(f, "\n(assert\n\t(and\n");
    /* pure part: start */
    /* not empty heap */
    fprintf(f, "\t(= x0 hst)\n");
    if (n > 0)
      fprintf(f, "\t(= y%x hen)\n", n);
    fprintf(f, "\t(< x0 y%x)\n", n);
    /* joint of hls and pto */
    for (short j = 0; j < n; j++) {
      fprintf(f, "\t(%s y%x z%x_0)\n",
	      (isent == true) ? "=" : "< ", j, j);
    }
    /* joint of pto/blk and hls */
    for (short j = 1; j < n; j++) {
      fprintf(f, "\t(%s v%x_%x x%x)\n",
	      (isent == true) ? "=" : "<", j, r, j + 1);
    }
    /* join of pto and blk between hls */
    for (short j = 0; j < n; j++) {
      /*
       * the j-th sequence is pto followed by blk/pto depending on the parity
       * of j, szj_0 = zj_0 - x{j+1}
       */
      fprintf(f, "\t(= (+ z%x_0 sz%x_0) x%x)\n", j, j, j + 1);
      if (j % 2 == 0) {
	/* generate r pto in consecutive addresses */
	for (int k = 1; k <= r; k++) {
	  if (isent)
	    fprintf(f, "\t(= (+ z%x_%x 1) z%x_%x)\n", j, k - 1, j, k);
	  else
	    fprintf(f, "\t(<= (+ z%x_%x 1) z%x_%x)\n", j, k - 1, j, k);
	}
      } else {
	/* generate r blk in consecutive addresses */
	fprintf(f, "\t(= (+ z%x_0 1) u%x_1)\n", j, j);
	for (int k = 1; k <= r; k++) {
	  if (isent)
	    fprintf(f, "\t(= v%x_%x u%x_%x)\n", j, k - 1, j, k);
	  else
	    fprintf(f, "\t(<= v%x_%x u%x_%x)\n", j, k - 1, j, k);
	}
	if (isent)
	  fprintf(f, "\t(= v%x_%x x%x)\n", j, r, j + 1);
      }
    }
    /* pure part: end */

    /* spatial part: begin */
    fprintf(f, "\t(tobool (ssep\n");
    /* n parts hls pto pto|blk */
    for (int j = 0; j < n; j++) {
      /* head chunk hls */
      fprintf(f, "\t\t(hls x%x y%x)\n", j, j);
      /* first pto with size */
      fprintf(f, "\t\t(pto z%x_0 (ref sz sz%x_0))\n", j, j);
      /* next pto  or blk */
      if (j % 2 == 0) {
	for (short k = 1; k <= r; k++) {
	  fprintf(f, "\t\t(pto z%x_%x (ref sz sz%x_%x))\n", j, k, j, k);
	}
      } else {
	for (short k = 1; k <= r; k++) {
	  fprintf(f, "\t\t(blk u%x_%x v%x_%x)\n", j, k, j, k);
	}
      }
    }
    /* last hls */
    fprintf(f, "\t\t(hls x%x y%x)\n", n, n);
    /* spartial part: end */
    fprintf(f, "\t\t)\n\t)\n");
    /* end assert-and */
    fprintf(f, "))\n");

    if (isent == true) {
      /* start assert-not */
      fprintf(f, "(assert (not\n");
      fprintf(f, "\t(tobool (ssep\n");
      fprintf(f, "\t\t(hls x0 y%x)\n", r);
      fprintf(f, "\t\t(hls y%x y%x)\n", r, n);
      /* end assert-not */
      fprintf(f, "\t)\t)\n");
      fprintf(f, "))\n");
    }
    print_check(f);
    fclose(f);
    /* TODO: update res correctly */
    res++;
  }
  free(fname_full);
  return res;
}


/**
 * Help message
 *
 * @param exe executable name
 */
void
usage(char *exe)
{
  printf("Usage: %s n [sat|ent] -[j|r|s] [-h]\n", exe);
}

/**
 * Generate examples
 * argv[1]: number of loop unroll or chunks
 * argv[2]: sat/ent
 * argv[3]: kind of  among (join|search|random)
 */
int
main(int argc, char **argv)
{
  short n = 1;
  if (argc > 1) {
    n = strtol(argv[1], NULL, 10);
  }
  if (n == 0) {
    perror("Please give an integer argument!");
    return 1;
  }
  for (int i = 2; i < argc; i++) {
    if (strncmp(argv[i], "sat", 3) == 0) {
      isent = false;
    } else if (strncmp(argv[i], "ent", 3) == 0) {
      isent = true;
    } else if (strncmp(argv[i], "-j", 2) == 0) {
      program = JOIN;
    } else if (strncmp(argv[i], "-s", 2) == 0) {
      program = SEARCH;
    } else if (strncmp(argv[i], "-r", 2) == 0) {
      program = RANDOM;
    } else if (strncmp(argv[i], "-h", 2) == 0) {
      use_hck = true;
    } else {
      printf("Unknown option!\n");
      usage(argv[0]);
    }
  }

  if (program == SEARCH || program == ALL) {
#ifndef NDEBUG
    fprintf(stdout, "search: start with parameters %d, %s\n",
	    n,
	    (isent == false) ? "sat" : "ent");
#endif
    if (search_unroll("search", n, true) < n) {
      fprintf(stderr, "Error: only %d files generated!\n", n);
    } else {
      fprintf(stdout, "search: %d files generated!\n", n);
    }
  }
  if (program == JOIN || program == ALL) {
#ifndef NDEBUG
    fprintf(stdout, "join: start with parameters %d, %s\n",
	    n,
	    (isent == false) ? "sat" : "ent");
#endif
    if (join_unroll("join", n) < n - 1) {
      fprintf(stderr, "Error: only %d files generated!\n", n);
    } else {
      fprintf(stdout, "join: %d files generated!\n", n);
    }
  }
  if (program == RANDOM || program == ALL) {
#ifndef NDEBUG
    fprintf(stdout, "rand: start with parameters %d, %s\n",
	    n,
	    (isent == false) ? "sat" : "ent");
#endif
    if (random_unroll("rand", n) < n - 1) {
      fprintf(stderr, "Error: only %d files generated!\n", n);
    } else {
      fprintf(stdout, "rand: %d files generated!\n", n);
    }
  }
  return 0;
}
