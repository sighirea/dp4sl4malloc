%!TEX root = aplas2021.tex

\section{Satisfiability problem of {\slah}}
\label{sec:sat-hls}

The satisfiability problem for an \slah\ formula $\varphi$ is to decide
whether there is a stack $s$ and a heap $h$ such that $s,h \models \varphi$.
In this section, we propose a decision procedure for the satisfiability problem, 
thus showing that the satisfiability problem is NP-complete.

\begin{theorem}\label{thm-sat}
The satisfiability problem of {\slah} is NP-complete.
\end{theorem}
The NP lower bound follows from that of ASL in \cite{BrotherstonGK17}. The NP upper bound is achieved by encoding the satisfiability problem of {\slah} as that of an existentially quantified Presburger arithmetic formula. The rest of this section is devoted to the proof of the NP upper bound.

Presburger arithmetic (\PbA) is the first-order theory with equality
of the signature $\langle 0,1,+, <, (\equiv_n)_{n \in \NN \setminus \{0\}}\rangle$ interpreted over the
domain of naturals $\NN$ with `$+$' interpreted as the addition,  
`$<$' interpreted as the order relation, and $\equiv_n$ interpreted as the congruence relation modulo $n$.\footnote{%
Although `$<$' may be encoded using existential quantification in \PbA\ over naturals,
we prefer to keep it in the signature of \PbA\ to obtain quantifier free formulas.%
}
%The relations $\neq$, $\leq$ and $<$ may be encoded (using existential quantification) as well as multiplication by a constant.
%Reviewer 2 ESOP'21: < is not needed for PA over natural numbers, 
%as it can be derived: x < y iff exists z . x+z=y ^ x =/= y . 
% It would be needed for integers.
\PbA\ is a useful tool for showing complexity classes because its 
satisfiability problem belongs to various complexity classes depending
on the number of quantifier alternations~\cite{Haase2018ASG}.
In this paper, we consider quantifier-free \PbA\ formulas (abbreviated as \qfpa) and the $\mathsf{\Sigma}_1$-fragment of \PbA\ (abbreviated as \EPbA), which
contains existentially quantified Presburger arithmetic formulas. 
We recall that the satisfiability problem of {\qfpa} and {\EPbA} is NP-complete.
%When the number of existentially quantified variables is fixed, 
%the problem is P-complete.
%Moreover, the existential fragment of \PbA\  
%extended with a divisibility relation ($\PbA_{\%}$) is still 
%decidable~\cite{Lipshitz78,Lipshitz81}, and known to be NP-hard and in NEXP.


%The pure part of \slah\ formulas is in \EPbA. %,
%Brotherston and Kanovich~\cite{BrotherstonK18} showed that even if 
%the pure formulas are difference constraints and
%the spatial formulas contain only points-to atoms,
%the satisfiability of ASL is still NP-complete.
%
%The NP upper bound of ASL is obtained by the encoding
%ASL formulas into equi-satisfiable {\EPbA} formulas.

We basically follow the same idea as ASL to build 
an equi-satisfiable {\qfpa} abstraction of a \slah\ formula $\varphi$.
%that encodes its satisfiability: 
%\begin{compactitem}
%\item At first, points-to atoms $t_1 \pto t_2$ are transformed into $\blk(t_1, t_1+1)$. 
%\item Then, the block atoms $\blk(t_1, t_2)$ are encoded by the constraint $t_1 < t_2$.
%\item The predicate atoms $\hls{}(t_1, t_2; t_3)$, absent in ASL, are encoded by a formula in {\qfpa}, $t_1 = t_2 \vee (t_1 < t_2 \wedge \abs^+(\hls{}(t_1, t_2; t_3)))$. 
%\item Lastly, the separating conjunction is encoded by an {\qfpa} formula constraining the address terms of spatial atoms. 
%\end{compactitem}

%We utilize $\abs^+(\hls{}(x, y; z))$ %defined in the above section 
%to obtain in polynomial time an equi-satisfiable {\qfpa} abstraction for a symbolic heap $\varphi$, denoted by $\abs(\varphi)$.

We introduce some notations first.
%
Given a formula $\varphi\equiv \Pi : \Sigma$, 
$\atoms(\varphi)$ denotes the set of spatial atoms in $\Sigma$, and 
$\patoms(\varphi)$ denotes the set of predicate atoms in $\Sigma$. 
We also denote $\overline{\patoms}(\varphi)$ for $\atoms(\varphi)\setminus\patoms(\varphi)$.

\begin{definition}{(Presburger abstraction of \slah\ formula)}
Let $\varphi\equiv \Pi : \Sigma$ be a \slah\ formula.
The abstraction of $\varphi\equiv \Pi : \Sigma$, 
	denoted by $\abs(\varphi)$, is 
	the formula $\Pi\wedge\phi_{\Sigma}\wedge\phi_*$
	where:
\begin{itemize}
\item $\phi_{\Sigma}\triangleq\bigwedge\limits_{a \in \atoms(\varphi)}\abs(a)$ such that
{\small
\begin{eqnarray}
\abs(t_1\mapsto t_2) & \triangleq & \ltrue \\
\abs(\blk(t_1,t_2))  & \triangleq & t_1<t_2 \\
\abs(\hls{}(t_1, t_2, t_3))  & \triangleq & t_1=t_2 \\
& \lor &(t_1 < t_2 \land \abs^+ (\hls{}(x, y;z))[t_1/x, t_2/y, t_3/z]),
\end{eqnarray}
}
%where the boolean variables $\isemp_a$ are introduced for each atom $a \in \patoms(\varphi)$ to encode whether $a$ denotes an empty heap.
where $\abs^+ (\hls{}(x, y;z))$ is the least-fixed-point summary of $\hls{}(x, y;z)$.

\item $\phi_\sepc\triangleq\phi_1\wedge \phi_2 \wedge \phi_3$ 
	specifies the semantics of separating conjunction, 
where 	
{\small
\begin{eqnarray}
\phi_1 & \triangleq & \bigwedge\limits_{a_i,a_j \in \patoms(\varphi), i<j} 
	\left(\begin{array}{l}
	(\isnonemp_{a_i} \land \isnonemp_{a_j}) \limp \\
	\quad (\atomtail(a_j) \le \atomhead(a_i)\lor \atomtail(a_i) \le \atomhead(a_j))
	\end{array}\right)
\\
\phi_2 & \triangleq & \bigwedge\limits_{\tiny\begin{array}{l}a_i \in \patoms(\varphi), \\[1mm] a_j \in \overline{\patoms}(\varphi)\end{array}} 
	\left(\begin{array}{l}
	\isnonemp_{a_i} \limp \\
	\quad (\atomtail(a_j) \le \atomhead(a_i) \lor \atomtail(a_i) \le \atomhead(a_j))
	\end{array}\right)
\\
\phi_3 & \triangleq & \bigwedge\limits_{a_i, a_j \in \overline{\patoms}(\varphi),  i < j} 
		\atomtail(a_j) \le \atomhead(a_i) \lor \atomtail(a_i) \le \atomhead(a_j)
\end{eqnarray}
}
\end{itemize}
and for each spatial atom $a_i$, $\isnonemp_{a_i}$ is an abbreviation of the formula $\atomhead(a_i) < \atomtail(a_i)$.
\end{definition}

For formulas $\varphi\equiv \exists \vec{z}\cdot \Pi : \Sigma$,
we define $\abs(\varphi) \triangleq \abs(\Pi:\Sigma)$ since $\exists \vec{z}\cdot \Pi : \Sigma$ and $\Pi:\Sigma$ are equi-satisfiable.

%The Appendix~\ref{app:sat-hls} provides more details.
%
The crux of this encoding and 
its originality with respect to the ones proposed for ASL in~\cite{BrotherstonGK17}
is the computation of $\abs^+(\hls{}(x, y; z))$, which is presented in the following.
%
%ESOP'21 Reviewer 3: 4.1 could use a little "closing" text to summarise where we are now: I felt as though we had perhaps already seen the key idea behind the submission (of course, there is more to come); maybe you could reflect on the significance of the Lemma and lead into the next part.
%
%%\subsection{{\EPbA} summary of $\hls{}$ atoms}
%\label{ssec:sat-hls-abs}
%\smallskip
Intuitively, the abstraction of the predicate atoms $\hls{}(x, y; z)$
	shall summarize the relation between $x$, $y$ and $z$ 
	for all $k \ge 1$ unfoldings of the predicate atom.
From the fact that the pure constraint in the inductive rule of $\hls{}$ is $2 \le x' - x \le v$, it is easy to observe that 
for each $k \ge 1$, $\hls{k}(x, y; z)$ can be encoded by $2 k \le y - x \le k z$. It follows that $\hls{}(x, y; z)$ can be encoded by $\exists k.\ k \ge 1 \wedge 2k \le y - x \le k z$.
If $z \equiv\infty$, then $\exists k.\ k \ge 1 \wedge 2 k \le y - x \le k z$ is equivalent to  $\exists k.\ k \ge 1 \wedge 2k \le y - x \equiv 2 \le y - x$, thus a {\qfpa} formula.
Otherwise, $2 k \le y - x \le k z$ is a non-linear formula since $k z$ is a non-linear term. 
The following lemma states that 
%In the sequel, we are going to show that 
$\exists k.\ k \ge 1 \wedge 2 k \le y -x \le k z$ can actually be turned into an equivalent {\qfpa} formula.

\begin{lemma}[Summary of $\hls{}$ atoms]\label{lem-hls}
Let $ \hls{}(x, y; z)$ be an atom in \slah\
representing a non-empty heap, where $x, y, z$ are three distinct variables in $\cV$.
%Then there is an {\EPbA} formula, denoted by $\abs^+(\hls{}(x,y; z))$, 
We can construct in polynomial time an {\qfpa} formula $\abs^+(\hls{}(x,y; z))$
which summarizes $\hls{}(x, y; z)$, namely we have 
for every stack $s$, $s \models \abs^+(\hls{}(x,y; z))$ iff 
there exists a heap $h$ such that $s, h \models \hls{}(x, y; z)$. 
%Similarly for $ \hls{}(x, y; \infty)$ and $ \hls{}(x, y; d)$ with $d \in \NN$.
\end{lemma}

Since the satisfiability problem of {\qfpa} is NP-complete,
%We are using $\abs^+(\hls{}(x, y; z))$ defined above 
%to obtain in polynomial time an equi-satisfiable \EPbA\ abstraction for a symbolic heap $\varphi$, denoted by $\abs(\varphi)$.
the satisfiability problem of \slah\ is in NP. 
%
The correctness of $\abs(\varphi)$ is guaranteed by the following result.

%\vspace{-2mm}
\begin{proposition}\label{prop-sat-correct}
A \slah\ formula $\varphi$ is satisfiable iff $\abs(\varphi)$ is satisfiable.
\end{proposition}
%\vspace{-2mm}

%Notice that $\abs(\varphi)$ is essentially a quantifier-free \PbA\ formula containing modulo constraints $t_1 \equiv r \bmod n$. The satisfiability of such formulas is still NP-complete. 
From now on, we shall assume that {\bf $\abs(\varphi)$ is a \qfpa\ formula}. This enables using the off-the-shelf SMT solvers, e.g. Z3, to solve the satisfiability problem of \slah\ formulas.


