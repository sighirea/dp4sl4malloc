%!TEX root = aplas2021.tex

\section{\slah: a separation logic fragment for heap lists}
\label{sec:logic-hls}

This section defines the syntax and the semantics of 
the logic \slah, which extends array separation logic (ASL)~\cite{BrotherstonGK17}
with the $\hls{}$ predicate.
Notice that ASL borrows the structure of formulas from 
	the symbolic heap fragment of SL~\cite{BerdineCO04}
	and introduces a new spatial atom for memory blocks.

%\vspace{-2mm}
\begin{definition}[\slah\ Syntax]\label{def:syn-slah}
Let $\cV$ denote an infinite set of address variables ranging over $\NN$, the set of natural numbers. 
The syntax of terms $t$, pure formulas $\Pi$, spatial formulas $\Sigma$
and symbolic heaps $\varphi$ is given by the following grammar: 
%\vspace{-2mm}
\[
\begin{array}{l l l r}
t & ::= x \mid n \mid t+t &\ \ \ & \mbox{terms}
\\ 
\Pi & ::= \top \mid \bot \mid t=t \mid t \ne t \mid t \leq t \mid t < t \mid \Pi \land \Pi &\ \ \  & \mbox{pure formulas}
\\ 
\Sigma & ::= \emp \mid t \pto t \mid \blk(t, t) \mid \hls{}(t, t; t^\infty) \mid \Sigma \sepc \Sigma &\ \ \  & \mbox{spatial formulas}
\\ 
\varphi & ::= \exists \vec{z}\cdot \Pi : \Sigma &\ \ \  & \mbox{formulas}
\end{array}
\]
where $x$ and $\vec{z}$ are variables resp. set of variables from $\cV$, 
$n \in \NN$, $t^\infty$ is either a term or $\infty$,
$\hls{}$ is the predicate defined inductively by 
the rules in Equations~(\ref{eq:hlsv-emp}) and (\ref{eq:hlsv-rec}),
%the following two rules:
%\begin{align}
%\label{eq:hlsv-emp}
%\hls{}(x, y; v) & \Leftarrow x=y : \emp  \\
%\label{eq:hlsv-rec}
%\hls{}(x, y; v) & \Leftarrow \exists z \cdot 2 \le z - x \le v : 
%	x \pto z-x \sepc \blk(x+1,z) \sepc \hls{}(z,y; v) 
%\end{align}
where $v$ is a variable interpreted over $\NN\cup\{\infty\}$. 
%Here we assume that $c \ge 2$ since $x \pto x'-x \sepc \blk(x+1,x')$ occurs in the body of Rule~(\ref{eq:hlsv-rec}).
An atom $\hls{}(x,y;\infty)$ is also written $\hls{}(x,y)$;
%$\top$ and $\bot$ are shorthands for $x=x$ and $x\ne x$ atoms..
Whenever one of $\Pi$ or $\Sigma$ is empty, we omit the colon. 
We write $\fv(\varphi)$ for the set of free variables occurring in $\varphi$. 
If $\varphi = \exists\vec{z}\cdot\Pi:\Sigma$, 
	we write $\qf(\varphi)$ for $\Pi:\Sigma$, 
	the quantifier-free part of $\varphi$.
%For convenience, we suppose that $\mathcal{V}$ includes a special address variable $nil$.
We define $\atomhead(a)$ and $\atomtail(a)$ where $a$ is a spatial atom as follows:
\begin{itemize}
\item if $a \equiv t_1 \pto t_2$
          then $\atomhead(a)\triangleq t_1$, $\atomtail(a)\triangleq t_1+1$,
%
\item if $a\equiv \blk(t_1, t_2)$
          then $\atomhead(a)\triangleq t_1$ and $\atomtail(a)\triangleq t_2$,
%
\item if $a\equiv \hls{}(t_1, t_2; t_3)$
      then $\atomhead(a) \triangleq t_1$ and $\atomtail(a) \triangleq t_2$.
\end{itemize}
\end{definition}
%\vspace{-2mm}

For $n, n' \in \NN$ such that $n \le n'$, we use $[n, n']$ to denote the set $\{n, \cdots, n'\}$. Moreover, we use $[n]$ as an abbreviation of $[1,n]$.
We interpret the formulas in the  classic model of separation logic
built from a stack $s$ and a heap $h$.
The stack is a function $s:\cV \tfun \NN$.
It is extended to terms, $s(t)$, to denote 
the interpretation of terms in the given stack;
$s(t)$ is defined by structural induction on terms:
$s(n) = n$, and
$s(t + t') = s(t) + s(t')$.
We denote $s[x\gets n]$ for a stack defined as $s$ except for the
interpretation of $x$ which is $n$.
Notice that $\infty$ is used only to give up the upper bound 
	on the value of the chunk size in the definition of $\hls{}$ 
	(see Equations~(\ref{eq:hlsv-emp})--(\ref{eq:hlsv-infty})).
%
The heap $h$ is a partial function $\NN \pfun \NN$. 
%where the neutral element is $e$ (function undefined in all points)
We denote by $\wpos{h}$ the domain of a heap $h$. 
We use $h_1 \uplus h_2$ to denote the \emph{disjoint} union of $h_1$ and $h_2$, that is, $\wpos{h_1} \cap \wpos{h_2} =\emptyset$, and for $i \in \{1,2\}$, we have $(h_1\uplus h_2)(n) = h_i(n)$ if $n \in \wpos{h_i}$. 
%%MS: this really maps what happens in memory models for DMA because 
%% the fields of the header are either positive values (status or size) 
%% or locations; the content of the block is not interpreted.

%\vspace{-2mm}
\begin{definition}[\slah\ Semantics]
The satisfaction relation $s,h\models \varphi$, 
	where $s$ is a stack, $h$ a heap, and $\varphi$ a \slah\ formula,
	is defined by: % structural induction on $\varphi$ as follows:
\begin{itemize}
\item $s,h \models  \top$ always and never $s,h \models  \bot$,
\item $s,h \models  t_1 \sim t_2$ iff   
	$s(t_1) \sim s(t_2)$, where $\sim\in\{=,\ne,\le,<\}$,
%
\item
$s,h \models  \Pi_1 \land \Pi_2$  iff  
	$s,h \models \Pi_1$ and  $s,h\models \Pi_2$,
%
\item
$s,h \models  \emp$  iff $\wpos{h} = \emptyset$,
%
\item 
$s,h \models  t_1 \pto t_2$ iff $\exists n\in\NN$ s.t.
	$s(t_1)=n$, $\wpos{h}=\{n\}$, and $h(n)=s(t_2)$,
%\\
%
\item $s,h \models  \blk(t_1, t_2)$ iff  
	$\exists n,n'\in\NN$ s.t.  $s(t_1)=n$, $s(t_2)=n'$, $n < n'$, and
	  $\wpos{h}=[n,n'-1]$,
	 
\item $s,h \models  \hls{}(t_1, t_2;t_3)$ iff  
	$\exists k \in\NN$ s.t. $s,h \models \hls{k}(t_1, t_2;t_3)$,
	
\item $s,h \models \hls{0}(t_1, t_2;t^\infty)$ iff  
	$s,h \models t_1 = t_2 : \emp$,

\item 
$s,h \models \hls{\ell+1}(t_1, t_2;t^\infty)$ iff   
	$s,h \models 
		\exists z\cdot  \kappa \le z-t_1 \land \Pi' : t_1\pto z-t_1\ \sepc 
		 \blk(t_1+1,z) \sepc \hls{\ell}(z,t_2;t^\infty),
	$
	 where if  $t^\infty\equiv\infty$, then  $\Pi'\equiv \top$, otherwise,  
	 $\Pi' \equiv z-t_1 \le t^\infty$,

\item 
$s,h \models  \Sigma_1 \sepc \Sigma_2  \mbox{ iff } 
	\exists h_1,h_2\mbox{ s.t. } h=h_1\uplus h_2, 
	s,h_1\models\Sigma_1 \mbox{ and } s,h_2\models\Sigma_2$,

\item 
$s,h \models  \exists\vec{z}\cdot\Pi:\Sigma$ iff $\exists \vec{n}\in\NN^{|\vec{z}|}$ s.t.  
	$s[\vec{z}\gets\vec{n}],h\models\Pi$ and  $s[\vec{z}\gets\vec{n}],h\models\Sigma$.
\end{itemize}
\end{definition}
%\vspace{-2mm}

We write $A \models B$ where $A$ and $B$ are (sub-)formula in \slah\ for
$A$ entails $B$, i.e., 
that for any model $(s,h)$ such that $s,h\models A$ then $s,h\models B$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Notice that the semantics of $\blk(x,y)$ differs from the one given in~\cite{BrotherstonGK17} for $\mathtt{array}(x,y)$
because we consider that the location $y$ is the first after the last location in the memory block, as proposed in~\cite{CalcagnoDOHY06}.
%
Intuitively, an atom $\hls{}(x,y;v)$ with $v$ a variable
defines a heap lists where all chunks have sizes between $\kappa$ and the value of $v$.
Notice that if $v < \kappa$ then the atom $\hls{}(x,y;v)$ has a model iff $x=y$.
%
With this semantics, the $\blk$ and $\hls{}$ predicates are compositional predicates~\cite{EneaSW15} and therefore they satisfy the following composition lemmas:
% \mihaela{r2 CADE}
%it is easy to show the following lemmas:
\begin{align}
\blk(x,y)\sepc\blk(y,z) \models & ~\blk(x,z) \label{lemma:blk}
\\
\hls{}(x,y;v)\sepc\hls{}(y,z;v) \models & ~\hls{}(x,z;v)  
%\hls{}(x,y;v_1)\sepc\hls{}(y,z;v_2) \models & ~\hls{}(x,z;\max(v_1,v_2))  
\label{lemma:hls}
\end{align}
%%i.e., such block definition is compositional.
%Similarly to \cite{BrotherstonGK17}, the block predicate may be employed
%with the absolute addressing $\blk(\ell,\ell')$ or
%with base-offset addressing $\blk(\ell, i, j)$ for the array from $\ell+i$ to $\ell+j-1$.

\noindent
For readability, we fix $\kappa=2$ in Eqns~(\ref{eq:hlsv-emp})--(\ref{eq:hlsv-infty}) in the remainder of the paper.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



