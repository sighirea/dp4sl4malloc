%!TEX root = aplas2021.tex

%\section{Decision procedure for $\sla(\blk,\hls{},\pto)$}

\section{Entailment problem of {\slah}}
\label{sec:ent}

We consider the following entailment problem:
Given the symbolic heaps $\varphi$ and $\psi$ in \slah\ such that 
	$\psi$ is quantifier free and 
	$\fv(\psi) \subseteq \fv(\varphi)$,
	decide if $\varphi \models \psi$.
	Notice that the existential quantifiers in $\varphi$, if there is any, can be dropped.


%Let $\varphi \equiv \Pi_1: \Sigma_1$ and $\psi \equiv \Pi_2: \Sigma_2$ be two {\slah} formulas such that $\vars(\psi) \subseteq \vars(\varphi)$. Moreover, we assume that the recursive rule of $\hls{}(x, y; v)$ is of the form
%\begin{eqnarray*}
%\hls{}(x, y; v) & \Leftarrow & \exists x' \cdot c \le x' - x \le v : 
%	x \pto x'-x \sepc \blk(x+1,x') \sepc \hls{}(x',y).
%\end{eqnarray*}

%We show how to decide $\varphi \vDash \psi$.
	
Our goal in this section is to show that the entailment problem is decidable, as stated in the following theorem.

\begin{theorem}\label{thm-entail}
The entailment problem of {\slah} is coNP-complete. %\mihaela{state upperbound}
\end{theorem}
The coNP lower bound follows from the fact that the entailment problem of quantifier-free ASL formulas is also coNP-complete~\cite{BrotherstonGK17}. 
The remainder of this section is devoted to the proof of the coNP upper bound. 
%where we propose the decision procedure for the entailment problem
%Let 
%$\varphi \models \psi$ where
%In the rest of this section, let us assume that 
%$\varphi = \Pi: \Sigma$ and $\psi = \Pi': \Sigma'$.
%

In a nutshell, we show in Section~\ref{ssec:order}
that the entailment problem $\varphi \models \psi$ 
can be decomposed into a finite number of \emph{ordered entailment problems} 
$\varphi' \models_{\preceq} \psi'$ where
all the terms used as start and end addresses of spatial atoms in $\varphi'$ and $\psi'$
 are ordered by a preorder $\preceq$.
Then, we propose a decision procedure to solve ordered entailment problems.
In Section~\ref{ssec:ent-1}, we consider the special case    
    where the consequent $\psi'$ has a unique spatial atom; 
    this part reveals a delicate point which appears when the consequent 
    and the antecedent are $\hls{}$ atoms because of the constraint on the chunk sizes.
The general case is dealt with in Section~\ref{ssec:ent-all}; 
    the procedure calls the special case for the first atom of the consequent with 
    all the compatible prefixes of the antecedent, and 
	it does a recursive call for the remainders of the consequent and the antecedent. Note that to find all the compatible prefixes of the antecedent, some spatial atoms in the antecedent might be split into several ones. 

We show how to derive the coNP upper bound from the aforementioned decision procedure.  We first observe the following facts.
\begin{enumerate}
\item The original entailment problem is valid iff all of at most exponentially many 
\emph{ordered entailment problems} are valid, since there are at most exponentially many total preorders.
\item Each ordered entailment problem is valid iff all of at most exponentially many \emph{special ordered entailment problems}, where the consequent contains only one spatial atom, are valid.
%\item The original entailment problem is invalid iff there is an invalid special ordered entailment problem instance.
\item The special ordered entailment problem is in coNP.
\end{enumerate}
Therefore,  to show that $\varphi \models \psi$ is not valid, the procedure 
\begin{itemize}
\item first guesses a total preorder and obtain an ordered entailment problem (of polynomial size), 
\item then guesses for the ordered entailment problem one special ordered entailment problem (of polynomial size) where the consequent contains only one spatial atom, 
\item finally guesses and verifies in polynomial time a polynomial-size witness for the invalidity of the special ordered entailment problem. 
\end{itemize}
The above procedure runs in nondeterministic polynomial time, thus showing the invalidity of the entailment problem is in NP. From this, we deduce that the entailment problem of {\slah} is in coNP.

%At first, it is easy to observe that if $\abs(\varphi)$ is unsatisfiable, 
%then the entailment holds. Moreover, if $\abs(\varphi) \not\models \abs(\psi)$, 
%the the entailment $\varphi \models \psi$ does not hold. 

\smallskip

In the sequel, we assume that $\abs(\varphi)$ is satisfiable and $\abs(\varphi) \models \abs(\psi)$. Otherwise, the entailment is trivially invalid.

\subsection{Decomposition into ordered entailments}
\label{ssec:order}

Given the entailment problem $\varphi \models \psi$,
we denote by $\addr(\varphi)$ (and $\addr(\psi)$) 
	the set of terms $\atomhead(a)$ and $\atomtail(a)$ for spatial atoms $a$ 
	in $\varphi$ (resp. $\psi$).
%Moreover, let $\addr(\varphi,\psi)$ denote $\addr(\varphi) \cup \addr(\psi)$.
%
Recall that a \emph{preorder} $\preceq$ over a set $A$ 
	is a reflexive and transitive relation on $A$. 
	The preorder $\preceq$ on $A$ is \emph{total} if for every $a, b \in A$, 
	either $a \preceq b$ or $b \preceq a$. 
	For $a,b\in A$, we denote by $a \simeq b$ the fact that 
		$a \preceq b$ and $b \preceq a$,
	and we use $a \prec b$ for  $a \preceq b$ but not $b \preceq a$.

%\vspace{-2mm}
\begin{definition}[Total preorder compatible with $\abs(\varphi)$]
Let  $\preceq$ be a total preorder over $\addr(\varphi) \cup \addr(\psi)$. Then $\preceq$  is said to be \emph{compatible with}  $\varphi$ if
$C_\preceq \wedge \abs(\varphi)$ is satisfiable, where
\begin{align}\label{eq:start-end}
C_\preceq & \triangleq &
	\bigwedge_{t_1, t_2 \in \addr(\varphi) \cup \addr(\psi), t_1  \simeq  t_2} t_1 = t_2 \wedge \bigwedge_{t_1, t_2 \in \addr(\varphi) \cup \addr(\psi), t_1  \prec  t_2} t_1 < t_2.
\end{align}
\end{definition}
%\vspace{-4mm}
\begin{example}
Let $\varphi \equiv  \blk(x_1, x_2) \sepc \hls{}(x_2, x_3; y)$ and $\psi \equiv \blk(x_1, x_3)$. Then $\addr(\varphi) \cup \addr(\psi) = \{x_1, x_2, x_3\}$. From $\abs(\varphi) \models x_1 < x_2 \wedge x_2 \le x_3$, there are two total preorders compatible with $\varphi$, namely, $x_1 \prec_1 x_2 \prec_1 x_3$ and $x_1 \prec_2 x_2 \simeq_2 x_3$.
\end{example}

%\vspace{-4mm}
\begin{definition}[$\varphi \models_\preceq \psi$]
Let $\preceq$ be a total preorder over $\addr(\varphi) \cup \addr(\psi)$ that is compatible with $\varphi$. Then we say $\varphi \models_\preceq \psi$ if $C_\preceq \wedge \Pi: \Sigma \models \Pi': \Sigma'$. 
\end{definition}

%\vspace{-4mm}
\begin{lemma}\label{lem:split-prec}
$\varphi \models \psi$ iff 
for every total preorder $\preceq$ over $\addr(\varphi) \cup \addr(\psi)$ that is compatible with $\varphi$,
we have $\varphi \models_\preceq \psi$.
\end{lemma}
The proof of the above lemma is immediate.
There may be exponentially many total preorders over $\addr(\varphi) \cup \addr(\psi)$ that are compatible with $\varphi$ in the worst case.

%The problem $\varphi' \models_{\preceq} \psi'$ can be solved by 
%	a translation into the satisfiability problem of a \PbA\ formula in the  ${\sf \Pi}_1$-fragment. 
%The decomposition process and the decision procedure for $\varphi' \models_{\preceq} \psi'$ employ
%	the (quantifier-free) \PbA\ abstraction of \slah\ formulas.

The procedure to decide $\varphi \models_\preceq \psi$ is presented in the rest of this section.
We assume that $\varphi \equiv \Pi: a_1 \sepc \cdots \sepc a_m$ and $\psi \equiv \Pi': b_1 \sepc \cdots \sepc b_n$ such that
\begin{align}
C_\preceq \wedge \abs(\varphi) \mbox{ is satisfiable and }
C_\preceq \wedge \abs(\varphi) \models \abs(\psi).
\label{eq:order}
\end{align}
We can remove the atoms $\hls{}(t_1, t_2; t_3)$ in $\varphi$ or $\psi$ such that $C_\preceq \models t_1 = t_2$ because they correspond to an empty heap.
Moreover, after a renaming, 
we assume that the spatial atoms are sorted in each formula, namely, 
the following two \PbA\ entailments hold:
\begin{align}
C_\preceq \models & \bigwedge \limits_{i \in [1,m]} \atomhead(a_i) < \atomtail(a_i) \wedge \bigwedge \limits_{1 \le i < m}  \atomtail(a_i) \le  \atomhead(a_{i+1}),
\label{eq:order-ante} \\
C_\preceq \models & \bigwedge \limits_{i \in [1,n]} \atomhead(b_i) < \atomtail(b_i) \wedge \bigwedge \limits_{1 \le i < n}  \atomtail(b_i) \le  \atomhead(b_{i+1}).
\label{eq:order-conseq}
\end{align}

Section~\ref{ssec:ent-1} considers the special case of a consequent $\psi$ having only one spatial atom. Section~\ref{ssec:ent-all} considers the general case. 
%by an induction on the number of spatial atoms of $\psi'$.
%
%The decision procedure for $\varphi'\models_{\preceq} \psi'$ is proposed in
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\hide{
The following lemmas deal with the trivial cases of the entailment.

%We first decide whether $\abs(\varphi)$ is satisfiable. If not, then $\varphi \models \psi$ holds. 

\begin{lemma}\label{lem:abs-unsat}
Let $\varphi \models \psi$ be an entailment problem.
If $\abs(\varphi)$ is unsatisfiable, then the entailment is (trivially) valid.
\end{lemma}

%If $\abs(\varphi)$ is satisfiable, then decide whether $\abs(\varphi) \vDash \exists \overrightarrow{\isemp_\psi}.\ \abs(\psi)$ holds, where $\overrightarrow{\isemp_\psi}$ is the vector of Boolean variables introduced when constructing $\abs(\psi)$. If the answer is no, then the entailment $\varphi \models \psi$ does not hold. 

\begin{lemma}\label{lem:abs-ent}
Let $\varphi \models \psi$ be an entailment problem
such that $\abs(\varphi)$ is satisfiable.
%Let $\overrightarrow{\isemp_\psi}$ be the set of Boolean variables 
%	introduced when constructing $\abs(\psi)$.
If $\abs(\varphi) \models \abs(\psi)$ is invalid
then the entailment $\varphi \models \psi$ is invalid.
\end{lemma}

\begin{proof}
It is equivalent to show that if $\varphi \models \psi$ is valid, then $\abs(\varphi) \models  \abs(\psi)$ is valid. 
Suppose $\abs(\varphi)$ is satisfiable and $\varphi \models \psi$ is valid. Then there is an interpretation $s$ such that $s \models \abs(\varphi)$. 
From the proof of Proposition~\ref{prop-sat-correct}, we know that there are $h$ such that $s, h \models \varphi$. From $\varphi \models \psi$, we deduce that $s, h \models \psi$. From the proof of Proposition~\ref{prop-sat-correct} again, we know that $s \models \abs(\psi)$.  Therefore, $\abs(\varphi) \models \abs(\psi)$  is valid.
%Moreover, $I$ is compatible with $s$, that is, for every $$
\qed
\end{proof}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%The decision procedure for the entailment problem exploits these lemmas to return valid resp. invalid
%answers based on the \EPbA\ abstraction computed for satisfiability.
%
%For convenience, we say that $\varphi \models \psi$ is \emph{nontrivially valid} if $\abs(\varphi)$ is satisfiable and the entailment holds.

\hide{
\input{entail-hls-order.tex}
}

\input{entail-hls-one-atom.tex}

%\vspace{-2mm}
\input{entail-hls-mult-atoms.tex}

