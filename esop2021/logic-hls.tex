%!TEX root = cade-28.tex

\section{\slah, a separation logic fragment for heap-list}
\label{sec:logic-hls}

This section defines the syntax and the semantics of 
the logic \slah, which extends array separation logic (ASL)~\cite{BrotherstonGK17}
with the $\hls{}$ predicate.
Notice that ASL borrows the structure of formulas from 
	the symbolic heap fragment of SL~\cite{BerdineCO04}
	and introduces a new spatial atom for memory blocks.

\begin{definition}[\slah\ Syntax]\label{def:syn-slah}
Let $\cV$ denote an infinite set of address variables ranging over $\NN$, the set of natural numbers. 
The syntax of terms $t$, pure formulas $\Pi$, spatial formulas $\Sigma$
and symbolic heaps $\varphi$ is given by the following grammar: 
%where $t$ denotes terms, $\Pi$ denotes pure formulas,  $\Sigma$ denotes spatial formulas, $SH$ denotes symbolic heaps,
\begin{eqnarray*}
t & ::= & x \mid n \mid t+t \label{eq:syn-syn-term} %\mid nt
\\[1mm]
\Pi & ::= & t=t \mid t \ne t \mid t \leq t \mid t < t \mid \Pi \land \Pi
\\[1mm]
\Sigma & ::= & \emp \mid t \pto t \mid \blk(t, t) \mid \hls{}(t, t; t^\infty) \mid \Sigma \sepc \Sigma
\\[1mm]
\varphi & ::= & \exists \vec{z}\cdot \Pi : \Sigma
\end{eqnarray*}
where $x$ and $\vec{z}$ are variables resp. set of variables from $\cV$, 
$n \in \NN$, $t^\infty$ is either a term or $\infty$,
$\hls{}$ is the predicate defined inductively by the following two rules:
\begin{eqnarray}
\label{eq:hlsv-emp}
\hls{}(x, y; v) & \Leftarrow & x=y : \emp  \\
\label{eq:hlsv-rec}
\hls{}(x, y; v) & \Leftarrow & \exists x' \cdot 2 \le x' - x \le v : 
	x \pto x'-x \sepc \blk(x+1,x') \sepc \hls{}(x',y; v)\quad\quad 
\end{eqnarray}
where 
%$c\in\NN$ is a fixed constant and 
$v$ is a variable interpreted over $\NN\cup\{\infty\}$. 
%Here we assume that $c \ge 2$ since $x \pto x'-x \sepc \blk(x+1,x')$ occurs in the body of Rule~(\ref{eq:hlsv-rec}).
Whenever one of $\Pi$ or $\Sigma$ is empty, we omit the colon. 
An atom $\hls{}(x,y;\infty)$ is also written $\hls{}(x,y)$.
We write $\fv(\varphi)$ for the set of free variables occurring in $\varphi$. 
If $\varphi = \exists\vec{z}\cdot\Pi:\Sigma$, 
	we write $\qf(\varphi)$ for $\Pi:\Sigma$, 
	the quantifier-free part of $\varphi$.
%For convenience, we suppose that $\mathcal{V}$ includes a special address variable $nil$.
\end{definition}

We interpret the formulas in the  classic model of separation logic
built from a stack $s$ and a heap $h$.
The stack is a function $s:\cV \tfun \NN$.
It is extended to terms, $s(t)$, to denote 
the interpretation of terms in the given stack;
$s(t)$ is defined by structural induction on terms:
$s(n) = n$, and
$s(t + t') = s(t) + s(t')$.
We denote $s[x\mapsto n]$ for a stack defined as $s$ except for the
interpretation of $x$ which is $n$.
Notice that $\infty$ is used only to give up the upper bound 
	on the value of the chunk size in the definition of $\hls{}$.
	
The heap $h$ is a value of the monoid over partial functions $\NN \pfun \NN$
where the neutral element is $e$ (function undefined in all points)
and the composition $\circ$ is defined only when the functions 
composed have disjoint domains.
We denote by $\wpos{h}$ the domain of a heap $h$. 
By definition, $\wpos{h_1\circ h_2} = \wpos{h_1} \cup \wpos{h_2}$.
%%MS: this really maps what happens in memory models for DMA because 
%% the fields of the header are either positive values (status or size) 
%% or locations; the content of the block is not interpreted.

%\begin{definition}[Semantics of \paslid\ terms]
%The evaluation of a term in a model $(I,h)$ leads to a natural number as follows:
%$$ %\begin{eqnarray*}
%\sem n \antic_{I,h}\ = \ n 
%\quad\quad 
%\sem x \antic_{I,h} \ = \ I(x) 
%\quad\quad 
%\sem t_1 + t_2 \antic_{I,h} \ =\ \sem t_1 \antic_{I,h} + \sem t_2 \antic_{I,h} 
%$$ %\end{eqnarray*}
%\end{definition}

%To give the semantics of formulas, we use the following notations.
%For a variable $v\in\mathcal{V}$ and $m\in\NN$, we denote by $I[v\mapsto m]$ for the interpretation defined as $I$ but with $v$ mapped to $m$. Interpretations are extended in a natural way to tuples.
%The domain of a heap is denoted by $\wpos{h}$. 
%% and the heap with empty domain is $e$.
%Two heaps $h_1$ and $h_2$ are composed by $h_1\uplus h_2$ if they have disjoint domains, otherwise the composition is undefined.


\begin{definition}[\slah\ Semantics]
The satisfaction relation $s,h\models \varphi$, 
	where $s$ is a stack, $h$ a heap, and $\varphi$ a \slah\ formula,
	is defined by structural induction on $\varphi$ as follows:
\[\begin{array}{llcl}
s,h \models & t_1 \sim t_2 & \mbox{ iff } & 
	s(t_1) \sim s(t_2)\mbox{ where }\sim\in\{=,\ne,\le,<\}
\\
s,h \models & \Pi_1 \land \Pi_2 & \mbox{ iff } &
	s,h \models \Pi_1 \mbox{ and } s,h\models \Pi_2
\\
s,h \models & \emp & \mbox{ iff } & h = e
\\
s,h \models & t_1 \pto t_2 & \mbox{ iff } & 
	\exists n\in\NN\mbox{ s.t. }
	s(t_1)=n,\ \wpos{h}=\{n\}, h(n)=s(t_2)
\\
s,h \models & \blk(t_1, t_2) & \mbox{ iff } & 
	\exists n,n'\in\NN\mbox{ s.t. }
	s(t_1)=n,\ s(t_2)=n',\ n < n'\mbox{ and }\\
	& & & \quad \wpos{h}=\{n,...,n'-1\}
\\
s,h \models & \hls{}(t_1, t_2;t_3) & \mbox{ iff } & 
	\exists k \in\NN\mbox{ s.t. } s,h \models \hls{k}(t_1, t_2;t_3)
\\
s,h \models & \hls{0}(t_1, t_2;t^\infty) & \mbox{ iff } & 
	s,h \models t_1 = t_2 : \emp
\\
s,h \models & \hls{\ell+1}(t_1, t_2;t^\infty) & \mbox{ iff } & 
	s,h \models \begin{array}[t]{rl}
		\exists x'\cdot & 2 \le x'-x \land \Pi' : x\pto x'-x\ \sepc \\
		& \blk(x+1,x') \sepc \hls{\ell}(t_1,t_2;t^\infty) 
		\end{array} \\
	& & & \mbox{ where if } t^\infty\equiv\infty \mbox{ then } \Pi'\equiv x=x  \\
	& & & \mbox{ otherwise } \Pi' \equiv x'-x \le t^\infty
\\
s,h \models & \Sigma_1 \sepc \Sigma_2 & \mbox{ iff } & 
	\exists h_1,h_2\mbox{ s.t. } h=h_1\circ h_2, 
	s,h_1\models\Sigma_1 \mbox{ and } s,h_2\models\Sigma_2
\\
s,h \models & \exists\vec{z}\cdot\Pi:\Sigma & \mbox{ iff } &
	\exists \vec{n}\in\NN^{|\vec{z}|}\mbox{ s.t. } 
	s[\vec{z}\mapsto\vec{n}],h\models\Pi \mbox{ and } 
	s[\vec{z}\mapsto\vec{n}],h\models\Sigma	
\end{array}\]
\end{definition}

We write $A \models B$ for $A$ and $B$ sub-formula in \slah\ to mean
that $A$ entails $B$, i.e., that for any model $(s,h)$ such that $s,h\models A$
then $s,h\models B$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Notice that the semantics of $\blk(x,y)$ differs from the one given in~\cite{BrotherstonGK17} for $\mathtt{array}(x,y)$
because we consider that the location $y$ is the first after the last location in the memory block, as proposed in~\cite{CalcagnoDOHY06}.
With this semantics, it is easy to show the following lemma:
%(i.e., all models of the left formula are models for the right formula):
\begin{equation}\label{lemma:blk}
\blk(x,y)\sepc\blk(y,z) \models \blk(x,z)
\end{equation}
%%i.e., such block definition is compositional.
%Similarly to \cite{BrotherstonGK17}, the block predicate may be employed
%with the absolute addressing $\blk(\ell,\ell')$ or
%with base-offset addressing $\blk(\ell, i, j)$ for the array from $\ell+i$ to $\ell+j-1$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Intuitively, an atom $\hls{}(x,y;v)$ with $v$ a variable
defines a heap lists where all chunks have sizes between $2$ and the value of $v$.
Notice that if $v < 2$ then the atom $\hls{}(x,y;v)$ has no model and is unsatisfiable.


