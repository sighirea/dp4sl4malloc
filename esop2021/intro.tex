%!TEX root = esop2021.tex

\section{Introduction}

Separation logic~\cite{Reynolds:2002,OHearn19}  
has a big impact on the verification or static analysis of programs
manipulating the dynamic memory. Performant tools have been developed
based on this formalism, among which we cite
\textsc{Infer}~\cite{DBLP:journals/cacm/DistefanoFLO19} for static analysis
and \textsc{VeriFast}~\cite{verifast} for deductive verification.\todo{MS: change tool}

%These tools employ the 
The \emph{symbolic heap} (SH) fragment\todo{MS: change motivation}
of separation logic has been introduced in~\cite{BerdineCO04}
because it provides a good compromise between expressivity
and tractability. This fragment includes existentially quantified formulas
which are conjunctions of 
	aliasing constraints $x = y$ and $x \neq y$ between pointers $x$ and $y$,
and heap constraints $x\pto v$ expressing that at the address $x$ is allocated
    a memory zone containing the value $v$.
    The separating conjunction composes the heap constraints by ensuring 
    that the allocated memory blocks are disjoint.
To capture properties of heaps with unbounded data structures, the SH fragment is
extended with predicates that may be inductively defined using SH formulas or
	defined by the semantics, i.e., built-in.
The first category includes the predicate specifying acyclic singly linked list segments defined by the following two rules:
\begin{eqnarray}
\ls{}(x,y) & \Leftarrow & x = y : \emp \label{eq:ls-0} \\
\ls{}(x,y) & \Leftarrow & \exists x'\cdot x \neq y : x \pto x' \sepc \ls{}(x',y) \label{eq:ls-rec}
\end{eqnarray}
The satisfiability problem for the SH fragment with the $\ls{}$ predicate is in PTIME~\cite{CookHOPW11} and efficient solvers have been implemented for it~\cite{PerezR13,DBLP:conf/tacas/SighireanuPRGIR19}.
This situation changes for more complex definitions of predicates, 
but still there are decidable fragments for which the satisfiability problem
is EXPTIME~\cite{DBLP:conf/lpar/EchenimIP20,DBLP:conf/lpar/KatelaanZ20}
% PSPACE~\cite{IosifRS13}.
in general and NP-complete if the arity of the predicate 
is bound by a known constant~\cite{DBLP:conf/csl/BrotherstonFPG14}.
%
In the second category of predicates, one which is of interest for this paper 
is the memory block predicate $\blk(x,y)$ introduced in \cite{CalcagnoDOHY06}
to capture properties of memory allocators. This predicate has been formalized
as part of the \emph{array separation logic} (ASL) 
	introduced in~\cite{BrotherstonGK17}.
The predicate denotes a block of memory units (bytes or integers) 
	between addresses $x$ and $y$. It is usually combined with
	pointer arithmetics allowing to compute addresses in the memory blocks.
This combination is enough to increase the complexity class of the
	decision problems in ASL: the satisfiability is NP-complete, 
	the bi-abduction is NP-hard and 
	the entailment is EXPTIME resp. coNP-complete for quantified resp. quantifier-free formulas~\cite{BrotherstonGK17}.
The ASL fragment extended with the $\ls{}$ predicate has been studied 
in~\cite{DBLP:journals/corr/abs-1802-05935}, and it is decidable for 
satisfiability and entailment.

Still, the ASL fragment or its extension with $\ls{}$ are not expressive enough 
to specify the heap-list data structure that is defined by the following rules:
\begin{eqnarray}
\hls{}(x,y) & \Leftarrow & x = y : \emp \label{eq:hls-0} \\
\hls{}(x,y) & \Leftarrow & \exists x'\cdot x'-x \ge 2 : x \pto x'-x \sepc \blk(x+1,x') \sepc \hls{}(x',y) \label{eq:hls-rec}
\end{eqnarray}
From this definition, a heap-list between addresses $x$ and $y$ is 
	either an empty block if $x$ and $y$ are aliased,
    or a list cell, called \emph{chunk}, starting at address $x$ and ending at
    address $x'$ followed by a heap-list from $x'$ to $y$.
    The chunk stores its size $x'-x$ at its start 
		(atom $x\pto x'-x$), that we also call \emph{chunk's header}.
	The \emph{chunk's body} is a memory block (atom $\blk(x+1,x')$) starting
		after the header and ending before the next heap-list's chunk.
This fragment has been used in the static analysis of memory
allocators~\cite{CalcagnoDOHY06,conf/lopstr/FangS16} but its decidability
has not been studied because these tools employ incomplete
syntactic methods for efficiency.
A special instance where all chunks have the same size
has been used in the deductive verification of 
	an implementation of the array list collection~\cite{CauderlierS18},
but required an interactive prover \textsc{VeriFast} and user provided
lemma. % to deal with the verification conditions.

The first part of our contributions is a theoretical study 
of the decision problems for ASL extended with 
the $\hls{}$ predicate. We consider several
versions of this predicate that lead to decidability of satisfiability and entailment problems. 
We propose decision procedures based on abstractions in Presburger arithmetics.
The second part of the contribution is experimental.
We implemented the decision procedures in a solver and
we applied it on a set of formulas originating from 
	path conditions and verification conditions of programs working on heap-lists. 
We also push the solver in the corner cases of our decision procedures
by providing randomly generated problems for each case.

%\mihaela{To fix:} a small model property of the fragment.
%\mihaela{Main ideas:}
%We also investigate fragments of SL for buddy systems specification and show that the class of properties required for their specification belong to \mihaela{I think,} an undecidable fragment.
%%MS: question:
%%    not the same class of ID, recursive rule has two recursive calls.
%%

%\smallskip
%\mypar{Paper's organisation:}\ 
The remainder of this paper is structured as follows. 
Section~\ref{sec:over-hls} provides a motivating example for our work and an overview of the decision procedures proposed.
Section~\ref{sec:logic-hls} formally defines the syntax and the semantics of our logic and presents several definitions of the heap-list predicate.
%
The satisfiability problem is studied in Section~\ref{sec:sat-hls}, where
we also present the decision procedure.
%
%Section~\ref{sec:sat-wfid} extends the satisfiability results to \slah\ with
%	well-formed inductive definitions of heap-lists.
%
Section~\ref{sec:ent} presents the results on the entailment problem.
%
The experimental evaluation of the solver implementing our procedures 
	is described in Section~\ref{sec:exp-hls}.
Section~\ref{sec:conc-hls} surveys the related work and concludes.
%by a comparison of our work with existing fragments and its impact on verification of memory allocators.

