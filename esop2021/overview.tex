%!TEX root = esop2021.tex

\section{Motivating example and overview}
\label{sec:over-hls}

This section illustrates the use of our logic for the specification 
	of programs manipulating heap-lists and
give an overview of the ingredients used by the decision procedures
	we propose.

Figure~\ref{fig:search} presents our running example.
The function \lst{search} scans 
	a heap-list between the addresses \lst{hbeg} (included) 
						and \lst{hend} (excluded)
	to find the chunk containing the address given in parameter. 
%
A heap-list is a block of memory divided into \emph{chunks},
such that each chunk stores its size at its start address.
For readability, we consider that the size counts 
the number of machine integers (and not bytes).
Scanning the list requires pointer arithmetics to
compute the start address of the next chunk.
After the size, the chunk may include additional information 
about the chunk in so-called  \emph{chunk's header}.
For readability, we consider in this section that the header
contains only the size information.
%In our example, 
%the second integer includes a flag storing the status 
%(1--free or 0--used) of the chunk.
The data carried by the chunk, called \emph{chunk's body},
starts after the chunk header
and ends before the start of the next chunk or 
\lst{hend}.

%% Example search size
%\input{fig-ex-rsz}

\begin{figure}[t]
\centering
\begin{minipage}[t]{0.35\textwidth}
\vspace{0pt}% Hack!! But does do the job
\lstset{language=C,numbers=left,stepnumber=1,fontadjust=true}
\begin{lstlisting}
int* hbeg; // heap start
int* hend; // heap end
int* search(int* a) {
  int* t = hbeg;
  while (t <= a &&
         t < hend) {
    int sz = *t;
    if (a < (t+sz))
      return t;
    t = t+sz;
  }
  return NULL;
}
\end{lstlisting}
\end{minipage}
\begin{minipage}[t]{0.6\textwidth}
\vspace{0pt}% Hack!! But does do the job
\begin{eqnarray}\label{eq:over-sat}
\texttt{pre} \label{eq:over-pre}
        & \triangleq & 0 < b < e : \hls{}(b,e) 
\\[2mm]
\texttt{path}_{\texttt{4-10}} \label{eq:over-path-once}
	    & \triangleq & 0 < b = t_0 \land t_0 \le a \land t_0 < e \land 
\\
	    & \land & sz_0 = t_1 - t_0 \land 2 \le t_1 - t_0  
\nonumber \\
        & :     & t_0 \pto sz_0 \sepc \blk(t_0+1, t_1) \sepc \hls{}(t_1,e)
\nonumber \\[2mm]
I \label{eq:over-inv}
	    & \triangleq & 0 < b < e : \hls{}(b,t) \sepc \hls{}(t,e)
\\[2mm]
\texttt{post}_{\texttt{5-10}}(I) \label{eq:over-inv-once}
        & \triangleq &  0 < b < e \land t_0 \le a \land t_0 < e \\
		& \land & 2 \le sz_0 \land t - t_0=sz_0  \land a \ge t_0+sz_0 
\nonumber \\
		& :     & \hls{}(b,t_0) \sepc t_0 \pto sz_0 \sepc \blk(t_0+1,t) 
\nonumber \\
		&       & \sepc\ \hls{}(t,e)
\nonumber
\end{eqnarray}
\end{minipage}
\vspace{-2eX}
\caption{Searching an address in a heap-list 
and some of its specifications in \slah}
\label{fig:search}
\vspace{-2eX}
\end{figure}

%The right part of Figure~\ref{fig:search} contains
%the definition of two predicates specifying heap-list segments
%between addresses $x$ (included) and $y$ (excluded).
%The definition is inductive and contains two cases:
%the base case of an empty heap-list segment 
%	(equations (\ref{id:hls-emp}) and  (\ref{id:hlsv-emp}))
%and the inductive case of a list containing at least a chunk
%	(equations (\ref{id:hls-ind}) and  (\ref{id:hlsv-ind})).
%The formulas used in each case are in the fragment \paslid\
%introduced in Section~\ref{sec:paslid}. This fragment is an extension of
%the symbolic heaps (SH) fragment of SL with memory block predicate $\blk$,
%pointer arithmetics and inductively defined predicates. 
%Like in the SH fragment, a formula may be existentially quantified 
%and has two parts: 
%a pure part (left side of ':') which contains linear arithmetic constraints 
%	over addresses and heap's data,
%and a spatial part where the atoms are composed by the separating conjunction $\sepc$.
%For example, the predicate $\hls{}$ specifies the chunk's header
%using two points-to atoms denoting the two integers allocated at 
%addresses $x$ and $x+1$; the chunk's body is specified by the
%memory block atom $\blk(u;v)$ denoting a sequence of integers
%from address $u$ (included) to $v$ (excluded).
%The predicate $\hls{<}$ has an additional parameter $v$ which
%is used as an upper bound for all chunk sizes. This predicate is
%useful to specify the invariant of the \lst{search}'s loop
%where the free chunks of the list segment between \lst{hbeg} and \lst{t}
%have sizes less than \lst{rsz}.

The right part of Figure~\ref{fig:search} provides 
several formulas in the logic \slah\ defined in Section~\ref{sec:logic-hls}.
The formula \texttt{pre} specifies the precondition of \lst{search}, 
i.e., there is a heap-list between addresses \lst{hbeg} and \lst{hend}
represented by the logic variables $b$ resp. $e$.
The pure part of \texttt{pre} (left side of ':') is a linear constraint
on the addresses, while the spatial part (right side of ':') employs the
predicate $\hls{}$ defined by Equations~(\ref{eq:hls-0})--(\ref{eq:hls-rec}).


The formula $\texttt{path}_{\texttt{4-10}}$ is generated by 
	the symbolic execution of the \lst{search} function 
	starting from the pre-condition \texttt{pre} and 
	executing the statements at lines 4--8 and 10.
%	(We use the SSA representation for the program and 
%	 extend it with the logic variables introduced by 
%	 the unfolding of the $\hls{}$ predicate):
	Bounded reachability techniques generate such formulas
	and query their satisfiability.
%the bounded reachability which generates satisfiability queries,
  
The decision procedure proposed for the satisfiability of \slah\
	in Section~\ref{sec:sat-hls} is based
on the translation of $\varphi$ into 
an equi-satisfiable Presburger arithmetic (\PbA) formula $\varphi^P$. 
The delicate point 
with respect to the previous work, e.g., \cite{DBLP:journals/corr/abs-1802-05935},  
is to compute a summary in \PbA\ for the $\hls{}$ atoms.
For this, we identify and formally define in Section~\ref{sec:logic-hls}
a class of inductive definitions of $\hls{}$ for which our method computes 
an exact summary.
% what happens if over/under-approximation?
% what happens if conditions are not satisfied? undecidability?
The predicate $\hls{}$  defined by Equations~(\ref{eq:hls-0})--(\ref{eq:hls-rec}) 
	belongs to this class.
The summary computed for the atom $\hls{}(x;y)$ is
$\exists k\cdot k > 0 \land 3k \le y - x$ when the heap it denotes is not empty.
For the empty case, the summary is trivially $y=x$.
The other spatial atoms $A$ are summarized by constraints on their start address
denoted by $\atomhead(A)$ and their end address $\atomtail(A)$.
For the points-to atom, this constraint is true, 
but for the $\blk(x,y)$ atom, the constraint is 
$x = \atomhead(a) < \atomtail(a) = y$.
Therefore, the spatial part of $\texttt{path}_{\texttt{4-10}}$ is translated 
into the \PbA\ formula $\texttt{pb}^\Sigma_{\texttt{4-10}}$:
$$\underbrace{t_0+1 < t_1}_{\blk{}(t_0+1,t_1)} \land 
  \underbrace{(t_1 = e \lor \exists k\cdot k > 0 \land 2k \le e-t_1)}_{\hls{}(t_1,e)}.$$
%
Alone, $\texttt{pb}^\Sigma_{\texttt{4-10}}$ does not capture the semantics of 
	the separating conjunction in the spatial part.
For that, we add a set of constraints 
expressing the disjointness of memory blocks occupied by the spatial atoms.
A simplified version of these constraints 
(see Section~\ref{sec:sat-hls} for the full version)
gives for our example the following formula 
	$\texttt{pb}^\sepc_{\texttt{4-10}}$:
$t_0 < t_1 \le e$.
By conjoining the pure part of $\texttt{path}_{\texttt{4-10}}$ with 
formulas $\texttt{pb}^\Sigma_{\texttt{4-10}}$ and 
         $\texttt{pb}^\sepc_{\texttt{4-10}}$,
we obtain an equi-satisfiable \PbA\ formula for $\texttt{path}_{\texttt{4-10}}$
in the fragment $\Sigma_1$ which is NP-complete.

\medskip
\noindent
The PA abstraction is also used to decide the validity of entailments 
in \slah\ in combination with a matching procedure between spatial parts.
To illustrate this decision procedure presented in Section~\ref{sec:ent},
we consider the verification conditions generated by the proof of the invariant
for the \lst{search}'s loop. 
Consider the invariant $I$ from Equation~(\ref{eq:over-inv}) in Figure~\ref{fig:search}.
It states that \lst{t} splits the heap-list in two parts.
%A precise invariant states that, 
%in the heap-list from $b$ to $t$ 
%	all the free chunks have size less than $rsz$
%and from $t$ to $e$ there is a heap-list: 
%$$I\ \equiv\ b < e ~:~ \hls{<}(b;t;rsz) \sepc \hls{}(t;e)$$
The following entailment states the initial condition for $I$:
% (it holds at loop's start):
\begin{eqnarray}\label{eq:over-ent-0}
0 < b < e \land\ t = b : \hls{}(b,e) & \limp & I
\end{eqnarray}
The decision procedure first checks that the antecedent is satisfiable
(otherwise the entailment is trivially valid).
If it is the case,\todo{MS: difficult to follow}
    the procedure explore all equivalence classes between location terms
    sed as start address of an atom in the antecedent and the consequent.
    After that, it considers all the total orders between these equivalence classes
    such that the order is implied by the PA abstraction of the antecedent.
%	it enumerates all the total orders between the classes of equal
%	location terms used as start address of an atom in the antecedent and the consequent; each total order shall be implied 
%	by the PA abstraction of the antecedent.
	In our example, the only total order is $0 < b = t < e$. 
Using this order and the equivalence classes of terms, the spatial sub-formulas
of each formula are cleaned from empty heap-list atoms such that only atoms representing non empty heaps are present. 
The spatial formulas are also transformed such that the atoms are ordered using their start location. 
Therefore, only the consequent changes its spatial part to $\hls{}(b,e)$ for our example.
	The ordered spatial parts are matched in the context of the 
		relation on locations given by the antecedent's \PbA\ abstraction.
	In our example, the two spatial atoms are syntactically the same, 
	so the matching succeeds trivially.
		
To illustrate a non trivial case of the matching procedure,
	we consider the verification condition (VC)
	for the inductiveness of $I$.
	The antecedent of the VC is the formula 
	$\texttt{post}_{\texttt{5-10}}(I)$ in Figure~\ref{fig:search},
	obtained by symbolically executing the path including the statements
	at lines 5--8 and 10 of \lst{search} starting from $I$.
The \PbA\ abstraction of $\texttt{post}_{\texttt{5-10}}(I)$ is satisfiable
and implies the total orderings of addresses used as starting address of a spatial atom given by the following constraint:
$$0 < b \le t_0 < t_0+1 < t \le e.$$
Consider the total order $0 < b < t_0 < t_0+1 < t < e$.
The spatial parts of the antecedent and consequent 
are ordered using it into:
\[\begin{array}{lcc}
\textrm{antecedent:} & \hls{}(b,t_0) \sepc\ t_0 \pto sz_0 \sepc\ \blk(t_0+1,t) 
				     & \sepc\ \hls{}(t,e) \\
\textrm{consequent:} & \hls{}(b,t) & \sepc\ \hls{}(t,e)
\end{array}\]
%
The matching procedure starts by searching 
	a prefix of the sequence of atoms in the antecedent
	that matches the first atom in the consequent, $\hls{}(b,t)$, such that
	the start and end addresses of the sequence are respectively $b$ and $t$.
The sequence found is $\hls{}(b,t_0) \sepc t_0 \pto sz_0 \sepc \blk(t_0+1,t)$
	which also satisfies the condition (encoded in \PbA)
		that it defines a contiguous memory block between $b$ and $t$.
The algorithm continues by trying to prove the matching found
	using a splitting lemma $\hls{}(b,t_0) \sepc \hls{}(t_0,t)$
	and the unfolding of the atom $\hls{}(t_0,t)$.\todo{one or the?}
	%ESOP 21: "one unfolding" what determines that one makes *one* unfolding here, in terms of the (automatic) procedure?
The \PbA\ abstraction of the antecedent is used to ensure that the 
	value stored at $t_0$, $sz_0$, is equal to the size of the heap-list starting at $t_0$, i.e., $t-t_0$.
	For this total order, the algorithm is able to prove the matching.
The procedure continues with the other total orderings, until 
    either a matching fails (the entailment is invalid) 
    or all orders lead to a matching (the entailment is valid).
    %\mihaela{what else??}
    
\newpage





%Memory allocators manage the heap part allocated to the process.
%They usually organize the heap into a set of non-overlapping chunks and some memory used to manage this set. 
%\begin{wrapfigure}{R}{0.45\textwidth}
%\centering
%\vspace{-4eX}
%\includegraphics[trim=2cm 2cm 2cm 1cm,scale=0.4]{../figs/fig_dma_chk.pdf}
%\caption{Heap-list in DMA}
%\label{fig:heap}
%\vspace{-4eX}
%\end{wrapfigure}
%In this work, we consider the organization of the set as a heap-list, i.e., a list stored in a raw array of bytes, where elements are in sequence in this block and the successor relation is given by the element size, see Figure~\ref{fig:heap}.
%To obtain the heap-list data structures, DMA managers split each chunk 
%in two parts:
%(i) the header containing management information, at least the chunk size including the header and its status -- occupied by user data or free to be allocated,
%(ii) the body intended to store user's data (payload).
%
%The heap list data structure may be seen as a continuous block of values (suppose that values are all integers) with some properties about its content, for example:
%(i) the size of the block is the sum of values contained at some positions in the block,
%(ii) the status of two adjacent chunks are not both free (early coalescing policy).
%Therefore, to capture these properties, we cannot use only a predicate $\blk(x;y)$ denoting a sequence of values stating at location $x$ and ending just before location $y$, i.e., at $y-1$. We propose to use a special predicate called $\hls{}$ defined inductively by the first two rules below  as a list segment of chunks, each chunk being specified by the predicate $\hck$:
%\begin{eqnarray*}
%\hls{}(x;y) & \Leftarrow & x=y : \emp \label{id:hls-emp}
%\\
%\hls{}(x;y) & \Leftarrow & 
%\exists w, st, nx \cdot  w - x > 3 \land 0\leq st \leq 1: x \pto w-x \sepc x+1\pto st   \\
%		& & \sepc\ x+2\pto nx  \sepc\ \blk(x+3; w)  \sepc \hls{}(w; y)
% %
%%\hls{}(x;y) & \Leftarrow & \exists w\cdot \hck(x;w)\sepc\hls{}(w;y) \label{id:hls-rec}
%%\\
%%\hck(x;y) & \Leftarrow & \exists sz,st,nx\cdot\begin{array}[t]{l}
%% 		sz > 3 \land y=x+sz \land 0\leq st \leq 1: \\
% %		x\pto sz \sepc x+1\pto st \sepc x+2\pto nx 
%%		\sepc\ \blk(x+3;x+sz)  %%\blk(x,3,sz)
%%		\end{array}\label{id:hck}
%\end{eqnarray*}
%The chunk is specified using $\blk$ predicate to denote the body of the chunk, and points-to atoms $t\pto t'$ to obtain the value stored in the header. Notice the use of pointer arithmetics to access the chunk fields and to obtain the start location of the next chunk in the heap.
%%%\zhilin{Is $\blk(x,3,sz)$ really better than $\blk(x+3, sz)$ ?}
%%%MS: is  better than $\blk(x+3, x+sz)$
%
%
%The heap-lists with no successive free chunks used in early coalescing policy are captured by an ID which employs the integer parameters $z$ and $z'$ to keep track of the status of chunks before the heap-list segment boundaries $x$ and $y$:
%\begin{eqnarray}
%\hls{c}(x,z;y,z') & \Leftarrow & x=y \land z=z' : \emp \label{id:hlsc-emp}\\
%\hls{c}(x,z;y,z') & \Leftarrow & \exists w,z''\cdot \begin{array}[t]{l}
%				z\neq\fif(x) \land z''=\fif(x) : \\
%				\hck(x;w) \sepc\ \hls{c}(w,z'';y,z') 
%				\end{array} \label{id:hlsc-rec}
%\end{eqnarray}
%where the term $\fif(x)$ denotes the value of the status field (at offset $+1$ from $x$), which may be 0 (for occupied chunk) or 1 (for free chunk).
%Similar definitions are used to capture other properties of allocators' configurations that don't use a special list for the set of free chunks. We provide such predicates in Section~\ref{sec:slid-hls} to specify allocation policies, \eg, best-fit or first-fit.


