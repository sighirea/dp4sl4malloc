%!TEX root = tacas2021.tex

%\section{Decision procedure for $\sla(\blk,\hls{},\pto)$}

\section{Entailment  of {\paslid} formulas}
\label{sec:ent}

Let $\varphi$ and $\psi$ be two {\paslid} formulas. 
%Since $\hck$ can be replaced by a separating conjunction of points-to and $\blk$ atoms, we assume that neither $\varphi$ nor $\psi$ contains $\hck$. 
%Then $\varphi$ and $\psi$ contain only spatial atoms $\mapsto$, $\blk$, and $\hls{}$. 
We assume that $\vars(\psi) \subseteq \vars(\varphi)$. Our goal is to show how to decide $\varphi \vDash \psi$.

In the sequel, we first consider how to decide the entailment problem for the case that at most one inductive predicate occurs in $\varphi$ and $\psi$, then consider the more general case.

\subsection{At most one inductive predicate occurs in $\varphi$ and $\psi$}

Let $\varphi \equiv \Pi_1: \Sigma_1$ and $\psi \equiv \Pi_2: \Sigma_2$.
%
Suppose that $P(x, \vec{z}; y, \vec{u}; \vec{v})$ is the only inductive predicate occurring in $\varphi$ and $\psi$. 

The decision procedure goes as follows.

\paragraph*{Step 1.} We first decide whether $\abs(\varphi)$ is satisfiable. If not, then $\varphi \models \psi$ holds. If $\abs(\varphi)$ is satisfiable, then decide whether $\abs(\varphi) \vDash \exists \overrightarrow{\isemp_\psi}.\ \abs(\psi)$ holds, where $\overrightarrow{\isemp_\psi}$ is the vector of Boolean variables introduced when constructing $\abs(\psi)$. If the answer is no, then the entailment $\varphi \models \psi$ does not hold. Otherwise, we go to Step 2.

\paragraph*{Step 2.} For every predicate atom $P(t_1, \pvec{t}'_1; t_2, \pvec{t}'_2; \pvec{t}'')$ in $\varphi$ (resp. $\psi$),  we do one of the following to $\varphi$ (resp. $\psi$).
\begin{itemize}
\item Add the pure constraint $t_1 = t_2 \wedge \pvec{t}'_1 = \pvec{t}'_2$ to $\varphi$ (resp. $\psi$), and remove the predicate atom from $\varphi$ (resp. $\psi$).
%
\item Add the pure constraint $t_1 < t_2$ to $\varphi$ (resp. $\psi$).
\end{itemize}
Let $\varphi' \equiv \Pi'_1: \Sigma'_1$ and $\psi' \equiv \Pi'_2: \Sigma'_2$  be the resulting formulas with $\Sigma'_1 = a_1 \sepc \cdots \sepc a_m$ and $\Sigma'_2 = b_1 \sepc \cdots \sepc b_n$. If both $\abs(\varphi')$ is satisfiable and $\abs(\varphi') \vDash \exists \overrightarrow{\isemp_{\psi'}}.\ \abs(\psi')$ holds, then we go to Step 3.

\paragraph*{Step 3.}  
Let $\mathbb{A}_{\varphi', \psi'}$ be the union of the set of $\atomhead(a), \atomtail(a)$ for atoms $a$ in $\varphi'$ and $\psi'$. Evidently, $\abs(\varphi')$ induces an equivalence relation $\sim$ on $\mathbb{A}_{\varphi',\psi'}$, namely, for $t_1, t_2 \in \mathbb{A}_{\varphi',\psi'}$, $t_1 \sim t_2$ iff $\abs(\varphi') \models t_1 = t_2$. Let $[\mathbb{A}_{\varphi',\psi'}]_\sim$ denote the set of equivalence classes of $\mathbb{A}_{\varphi',\psi'}$ with respect to $\sim$. Then $\abs(\varphi')$ induces a partial order on $[\mathbb{A}_{\varphi',\psi'}]_\sim$. We can complete the partial order into a total order $\prec$. Let $\Pi_\prec$ denote the pure constraint corresponding to $\prec$. For each total order $\prec$ compatible with $\varphi'$, namely, $\Pi_\prec \wedge \abs(\varphi')$ is satisfiable, we do the following: Reorder the spatial atoms of $\varphi'$ and $\psi'$ by following the total order $\prec$, with the atom of the smallest head being the leftmost. By abuse of notation, we still assume $\Sigma'_1 = a_1 \sepc \cdots \sepc a_m$ and $\Sigma'_2 =  b_1 \sepc \cdots \sepc b_n$. Then we go to Step 4.

The rest of this section is devoted to Step 4, which is the most technical step of the decision procedure.

We start with the special case that $\psi'$ contains only one spatial atom, namely, $b_1$.
\begin{itemize}
\item If $b_1 = t'_1 \pto t'_2$, then the entailment holds iff $m = 1$, $a_1 = t_1 \pto t_2$ and $\abs(\varphi') \models t_1 = t'_1 \wedge t_2 = t'_2$. (Recall that the ``well-formed'' condition requires that the recursive rule of $P$ contains at least one block atom, thus $b_1$ cannot be a predicate atom.)

\item If $b_1 = \blk(t'_1, t'_2)$, then the entailment holds iff $\abs(\varphi') \models \atomhead(a_1) = \atomhead(b_1) \wedge \atomtail(a_m) = \atomtail(b_1)$ and $a_1 \sepc \cdots \sepc a_m$ is consecutive, namely, $\abs(\varphi') \models \bigwedge \limits_{1 \le j < m} \atomtail(a_j) = \atomhead(a_{j+1})$.

\item If $b_1 = P(t'_1, \pvec{t}''_1; t'_2, \pvec{t}''_2; t'_3)$, then we use the procedure $\mathtt{matchP}$ in Algorithm~\ref{algorithm:matchP} to solve the entailment problem.
\end{itemize}

%\begin{definition}[Sorted entailment problem]
%A \emph{sorted entailment problem} is to decide $\varphi \vDash \psi$ for the given formulas $\varphi, \psi$ such that $\abs(\varphi)$ induces a linear order on $[\mathbb{A}_{\varphi,\psi}]_\sim$, namely, for every $t_1, t_2 \in \mathbb{A}_{\varphi,\psi}$, one of the following holds, $\abs(\varphi) \models t_1 = t_2$ or $\abs(\varphi) \models t_1 < t_2$ or $\abs(\varphi) \models t_1 > t_2$.
%\end{definition}

%\begin{eqnarray*}
%P(x,\vec{z};y,\vec{u};\vec{v}) & \Leftarrow & \\
%& & \hspace{-8mm} \big(\exists \vec{w},x', \pvec{z}'\cdot \Pi(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) :
%\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) \sepc P(x',\pvec{z}'; y,\vec{u};\vec{v})
%\big)\quad \label{eq:ID-rec}
%\end{eqnarray*}

%%%%%%%%matchAtom removed %%%%%%%%%%%%%
%%%%%%%%matchAtom removed%%%%%%%%%%%%%
\hide{
	\begin{algorithm}\label{algorithm:matchAtom}
		\small
		\SetKw{false}{false}
		\SetKw{true}{true}
		\SetKwProg{Fn}{Function}{}{end}
		\SetKwFunction{matchAtom}{matchAtom}
		\SetKwFunction{matchP}{matchP}
		\caption{Decide whether $C\vDash D$ holds when C is sorted and there is only one atom in the spatial part of D}
		\Fn{\matchAtom{$\Pi_C:c_1*\cdots*c_m$,$\Pi_D:d_1$}}{
			\Switch{$d_1$}{
				\uCase{$t\mapsto t'$}{
					\uIf{$m==1$}{
						\uIf{$c_1==t_1\mapsto t_1'$ and $Abs(C)\vDash t=t_1\wedge t'=t_1'$}{
							\KwRet{\true}\;
						}\uElseIf{$c_1==P(x,\vec{z};y,\vec{u};\vec{v})$ and $Abs(C)\vDash t=x\wedge y=t+1\wedge t'=1\wedge\Pi(x,\vec{z},y,\vec{u},\vec{v})$}{
							\KwRet{\true}\;
						}
						
					}
					\KwRet{\false}\;
				}
				\uCase{$blk(h,t)$}{
					\uIf{$Abs(C)\vDash h=head(c_1)\wedge tail(c_1)=head(c_2)\wedge\cdots\wedge tail(c_{m-1})=head(c_m)$}{
						\KwRet{\true}\;
					}\lElse{\KwRet{\false}}
				}
				\uCase{$P(x,\vec{z};y,\vec{u};\vec{v})$}{
					\KwRet{\matchP{$\Pi_C:c_1*\cdots*c_m$,$\Pi_D:d_1$}}\;
				}
			}
		}
	\end{algorithm}
}
%%%%%%%%matchAtom removed%%%%%%%%%%%%%
%%%%%%%%matchAtom removed%%%%%%%%%%%%%




\zhilin{matchP to be cleaned}

	\begin{algorithm}[htbp]\label{algorithm:matchP}
		\scriptsize
		\SetKw{false}{false}
		\SetKw{true}{true}
		\SetKwProg{Fn}{Function}{}{end}
		\SetKwFunction{matchP}{matchP}
		\SetKwFunction{checkSortedEntl}{checkSortedEntl}
		\caption{Decide whether $\varphi \vDash \psi$ holds when $\varphi$ is sorted (i.e. the spatial atoms are ordered according to the order of their head addresses) and the spatial formula of $\psi$ is a predicate atom}
		\Fn{\matchP{$\varphi = \Pi: a_1*\cdots*a_m$,$\psi = \Pi': P(t'_1, \pvec{t}''_1; t'_2, \pvec{t}''_2; \pvec{t}'_3)$}}{
			\lIf{$m=0$ and $\abs(\varphi)\models t'_1 = t'_2 \wedge \pvec{t}''_1 = \pvec{t}''_2$}{
				\KwRet{\true}
			}
			\uElseIf{$m\ge 1$ and $\abs(\varphi)\vDash t'_1=\atomhead(a_1)\wedge t'_2=\atomtail(a_m) \wedge \bigwedge \limits_{1\le j < m} \atomtail(c_j) = \atomhead(c_{j+1})$}{
				\Switch{$a_1$}{
					\lCase{a block atom}{\KwRet{\false}}
					\uCase{a predicate atom, say $P(t_1,\pvec{t}'_1; t_2, \pvec{t}'_2; \vec{t}_3)$}{
						\uIf{$m=1$}{
%							\tcp{$\vec{v}_k$ appears in $\Sigma$ of inductive rule and $\vec{v}_{k'}$ only in pure constraint}\label{cmt}
							\lIf{$Abs(\varphi)\models \pvec{t}''_1 = \pvec{t}'_1 \wedge \pvec{t}''_2 = \pvec{t}'_2 \wedge \bigwedge\limits_k \pvec{t}'_3 \ge \pvec{t}_3$}{
								\KwRet{\true}
							}
						}\uElse{
							\KwRet{\matchP{$Abs(C):c_1,Abs(D):P(x,\vec{z};y,\vec{u}';\vec{v})$} and \matchP{$Abs(C):c_2*\cdots*c_m,Abs(D):P(y,\vec{u}';y,\vec{u};\vec{v})$}}\;
						}
					}
					\uCase{$x_1\mapsto t_1$}{
						$S=\{c_k\mid c_{k+1} \text{ is not blk}\}\cup\{c_m\}$\;
						\uIf{$Abs(C)\models\bigvee\limits_{c_j\in S}(x_1+t_1=tail(c_j))\vee\bigvee\limits_{c_i\text{ is } P}(head(c_i)\le x_1+t_1<tail(c_i))$ is true}{
							res = true\;
							\ForEach{$c_j$ in S}{
								\uIf{$Abs(C)\wedge x_1+t_1=tail(c_j)$ is satisfiable}{
									\tcp{$\mathcal{A}_P=(Q,\delta)$  is FTS associated with P}\label{cmt}
									\ForEach{$\vec{q}_1,\vec{q}_2\in Q$ and $(\vec{q}_1,\vec{q}_2)\in\delta$}{
										res = res and \checkSortedEntl{$Abs(C)\wedge x_1+t_1=tail(c_j)\wedge\vec{z}=\vec{q}_1\wedge\vec{z}'=\vec{q}_2:c_1*\cdots*c_j,Abs(D)\wedge\Pi(x,\vec{z},tail(a_j),\vec{z}',\vec{v}):\Sigma(x,\vec{z},tail(c_j),\vec{z}',\vec{v})$} and \matchP{$Abs(C)\wedge x_1+t_1=tail(c_j)\wedge\vec{z}=\vec{q}_1\wedge\vec{z}'=\vec{q}_2:c_{j+1}*\cdots*c_m,Abs(D)\wedge\Pi(x,\vec{z},tail(c_j),\vec{z}',\vec{v}):P(tail(c_j),\vec{z}';y,\vec{u};\vec{v})$}\;
									} 
								}
							}
							\ForEach{$c_i=P(x_i,\vec{z}';y_i,\vec{u}';\vec{v}')$}{
								\uIf{$Abs(C)\wedge x_i\le x_1+t_1<y_i$ is satisfiable}{
									res = res and \matchP{$Abs(C):c_1*\cdots*c_{i-1}*P(x_i,\vec{z}';x_1+t_1,\vec{z}'';\vec{v}')*P(x_1+t_1,\vec{z}'';y_i,\vec{u}';\vec{v}')*c_{i+1}*\cdots*c_m,Abs(D):P(x,\vec{z};y,\vec{u};\vec{v})$}\;
									\ForEach{$1\le k<l$}{\tcp{$\Sigma=b_1*\cdots*b_l$ in the inductive rule}\label{cmt}
										res = res and \matchP{$Abs(C)\wedge x_1+t_1=tail(b_k)\wedge\Pi(w,\vec{z}'',w',\vec{z}''',\vec{v}'):c_1*\cdots*c_{i-1}*P(x_i,\vec{z}';w,\vec{z}'';\vec{v}')*\Sigma(w,\vec{z}'',w',\vec{z}''',\vec{v}')*P(w',\vec{z}''';y_i,\vec{u}';\vec{v}')$}
									}
								}
							}
							\KwRet{res}\;
						}
					}
				}
			}
			\KwRet{\false}\;
		}
	\end{algorithm}


Step 4 Decide $\Pi_\prec \wedge \Pi'_1: \Sigma'_1 \vDash \Pi'_2: \Sigma'_2$ by executing the following procedure.
\begin{enumerate} 
\item If the maximum element of $\prec$ does not contain $\atomtail(a_m)$ or $\atomtail(b_n)$, then the entailment does not hold.

\item If $\Pi_\prec \models \atomtail(a_i) < \atomtail(b_n) \le \atomhead(a_j)$ for some $a_i,a_j$, then the entailment does not hold.

\item Reorder the spatial atoms in $\varphi'$ and $\psi'$ according to $\prec$, let $\varphi''$ and $\psi''$ denote the resulting formula. Solve the entailment problem $\varphi'' \vDash \psi''$ by using Algorithm~\ref{algorithm:checkSortedEntl}.
\end{enumerate} 

	\begin{algorithm}\label{algorithm:checkSortedEntl}
		\small
		\SetKw{false}{false}
		\SetKw{true}{true}
		\SetKwProg{Fn}{Function}{}{end}
		\SetKwFunction{checkSortedEntl}{checkSortedEntl}
		\SetKwFunction{matchAtom}{matchAtom}
		\caption{Decide whether $C\vDash D$ holds when C and D are sorted and $tail(c_m)=tail(d_n)$}
		\Fn{\checkSortedEntl{$\Pi_C:c_1*\cdots*c_m$,$\Pi_D:d_1*\cdots*d_n$}}{
			\uIf{$tail(d_1)==tail(c_k)$}{
				\lIf{$!$\matchAtom{$Abs(C):c_1*\cdots*c_k,\Pi_D:d_1$}}{
					\KwRet{\false}
				}
				\lIf{$n==1$}{
					\KwRet{\true}
				}\lElse{
					\KwRet{\checkSortedEntl{$\Pi_C:c_{k+1}*\cdots*c_m$,$\Pi_D:d_2*\cdots*d_n$}}
				}
			}\uElseIf{$head(c_k)< tail(d_1)<tail(c_k)$}{
				\uIf{$c_k$ is blk atom}{
					\uIf{$!$\matchAtom{$Abs(C):c_1*\cdots*c_{k-1}*blk(head(c_k),tail(d_1)),\Pi_D:d_1$}}{
						\KwRet{\false}
					}
					\KwRet{\checkSortedEntl{$\Pi_C:blk(tail(d_1),tail(c_k))*c_{k+1}*\cdots*c_m$,$\Pi_D:d_2*\cdots*d_n$}}\;
				}\uElse{\tcp{$c_k=P(x,\vec{z};y,\vec{u};\vec{v})$}\label{cmt}
					res = \matchAtom{$Abs(C):c_1*\cdots*c_{k-1}*P(x,\vec{z};tail(d_1),\vec{u}';\vec{v}),\Pi_D:d_1$} and \checkSortedEntl{$\Pi_C:P(tail(d_1),\vec{u}';y,\vec{u};\vec{v})*c_{k+1}*\cdots*c_m$,$\Pi_D:d_2*\cdots*d_n$}\;
					\ForEach{$b_i$ of $\Sigma(x_1,\vec{u}',x_2,\vec{u}'',\vec
						{v})=b_1*\cdots*b_l$}{
						res = res and \matchAtom{$Abs(C)\wedge x_2>x_1\wedge tail(d_1)=tail(b_i)\wedge\Pi(x_1,\vec{u}',x_2,\vec{u}'',\vec
							{v}):c_1*\cdots*c_{k-1}*P(x,\vec{z};x_1,\vec{u}';\vec{v})*b_1*\cdots*b_i,\Pi_D:d_1$} and \checkSortedEntl{$\Pi_C:b_{i+1}*\cdots*b_l*P(x_2,\vec{u}'';y,\vec{u};\vec{v})*c_{k+1}*\cdots*c_m$,$\Pi_D:d_2*\cdots*d_n$}\;
						\uIf{$b_i=blk(x',y')$}{
							res = res and \matchAtom{$Abs(C)\wedge x_2>x_1\wedge\Pi(x_1,\vec{u}',x_2,\vec{u}'',\vec
								{v}):c_1*\cdots*c_{k-1}*P(x,\vec{z};x_1,\vec{u}';\vec{v})*b_1*\cdots*b_{i-1}*blk(x',tail(d_1)),\Pi_D:d_1$} and \checkSortedEntl{$\Pi_C:blk(tail(d_1),y')*b_{i+1}*\cdots*b_l*P(x_2,\vec{u}'';y,\vec{u};\vec{v})*c_{k+1}*\cdots*c_m$,$\Pi_D:d_2*\cdots*d_n$}\;
						}
					}
					\KwRet{res}\;
				}
			}
		}
	\end{algorithm}


\subsection{Multiple inductive predicates occur in $\varphi$ and $\psi$}