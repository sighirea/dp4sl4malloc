%!TEX root = esop2021.tex

\section{Satisfiability problem for {\slah}}
\label{sec:sat-hls}

The satisfiability problem for a \slah\ formula $\varphi$ is to decide
whether there is a stack $s$ and a heap $h$ such that $s,h \models \varphi$.
In this section, we propose a decision procedure for the satisfiability problem, thus showing that the satisfiability problem is NP-complete.

\begin{theorem}\label{thm-sat}
The satisfiability problem of {\slah} is NP-complete.
\end{theorem}
The NP lower bound follows from that of ASL in \cite{BrotherstonGK17}. The NP upper bound is achieved by encoding the satisfiability problem of {\slah} as that of an existential Presburger arithmetic formula. The rest of this section is devoted to the proof of the NP upper bound.

Presburger arithmetic (\PbA) is the first-order theory with equality
of the signature $\langle 0,1,+, <\rangle$ interpreted over the
domain of naturals $\NN$ with `$+$' interpreted as the addition and $<$ interpreted as the order relation.
%The relations $\neq$, $\leq$ and $<$ may be encoded (using existential quantification) as well as multiplication by a constant.
%Reviewer 2 ESOP'21: < is not needed for PA over natural numbers, 
%as it can be derived: x < y iff exists z . x+z=y ^ x =/= y . 
% It would be needed for integers.
\PbA\ is a useful tool for showing complexity classes because its 
satisfiability problem belongs to various complexity classes depending
on the number of quantifier alternations~\cite{Haase2018ASG}.
Here, we employ the $\mathsf{\Sigma}_1$-fragment of \PbA, which
contains existentially quantified Presburger arithmetic formulas (abbreviated as \EPbA). The satisfiability problem of {\EPbA} is NP-complete.
%When the number of existentially quantified variables is fixed, 
%the problem is P-complete.
%Moreover, the existential fragment of \PbA\  
%extended with a divisibility relation ($\PbA_{\%}$) is still 
%decidable~\cite{Lipshitz78,Lipshitz81}, and known to be NP-hard and in NEXP.


The pure part of \slah\ formulas is in \EPbA,
for which the satisfiability is already NP-hard.
Brotherston and Kanovich~\cite{BrotherstonK18} showed that even if 
the pure formulas are difference constraints and
the spatial formulas contain only points-to atoms,
the satisfiability of ASL is still NP-complete.
%
The NP upper bound of ASL is obtained by the encoding
ASL formulas into equi-satisfiable {\EPbA} formulas.

We basically follow the same idea as ASL. Nevertheless, the availability of the inductive predicate $\hls{}$ poses a challenge for the encoding: How to compute a least-fixed-point summary of the inductive definition of $\hls{}$. We tackle this challenge by showing that an {\EPbA} formula can be computed for this purpose, although the summary is a non-linear formula at the first sight.

In the sequel, we first show how to compute the summary for predicate atoms, then show how to compute the abstractions of {\slah} formulas.
%Our encoding of \slah\ formulas into \EPbA\ formulas 
%	deals with the $\hls{}(t_1, t_2; t_3)$ atoms, which is not a part of ASL. For this, we compute {\EPbA} summaries for $\hls{}(t_1, t_2; t_3)$ atoms.
%The remainder of this section is organized as follows.
%First, we define in Section~\ref{ssec:sat-hls-abs}
%	 an equi-satisfiable abstraction of $\hls{}$ atoms.
%Then, we use it to define an abstraction of formulas in \slah\
%     in Section~\ref{ssec:sat-abs}.
%Notice that we present in Section~\ref{sec:sat-wfid} an extension of this result
%	for more general inductive definitions for heap-lists.
	
%ESOP'21 Reviewer 3: 4.1 could use a little "closing" text to summarise where we are now: I felt as though we had perhaps already seen the key idea behind the submission (of course, there is more to come); maybe you could reflect on the significance of the Lemma and lead into the next part.

\subsection{{\EPbA} summary of $\hls{}$ atoms}
\label{ssec:sat-hls-abs}

The abstraction of spatial atoms $\hls{}(x, y; z)$\todo{MS: explain context} 
	shall summarize the relation between $x$, $y$ and $z$ 
	for all $k \ge 1$ unfoldings of the predicate $\hls{}$.
From the fact that the pure constraint in the inductive rule of $\hls{}$ is $2 \le x' - x \le v$, it is easy to observe that 
for each $k \ge 1$, $\hls{k}(x, y; z)$ can be encoded by $2 k \le y-x \le k z$. It follows that $\hls{}(x, y; z)$ can be encoded by $\exists k.\ k \ge 1 \wedge 2k \le y -x \le kz$.
If $z \equiv\infty$, then $\exists k.\ k \ge 1 \wedge 2 k \le y-x \le k z$ is equivalent to  $\exists k.\ k \ge 1 \wedge 2k \le y - x$, thus an {\EPbA} formula.
Otherwise, $2 k \le y-x \le k z$ is a non-linear formula since $k z$ is a non-linear term. 
In the sequel, we are going to show that $\exists k.\ k \ge 1 \wedge 2 k \le y-x \le k z$ can actually be turned into an equivalent {\EPbA} formula.

\begin{lemma}[Summary of $\hls{}$ atoms]
Let $ \hls{}(x, y; z)$ be an atom in \slah\
representing a non-empty heap, where $x, y, z$ are three distinct variables in $\cV$.
Then there is an {\EPbA} formula, denoted by $\abs^+(\hls{}(x,y; z))$, which summarizes $\hls{}(x, y; z)$, namely for every stack $s$ $s \models \abs^+(\hls{}(x,y; z))$ iff there exists a heap $h$ such that $s, h \models \hls{}(x, y, z)$. 
%Similarly for $ \hls{}(x, y; \infty)$ and $ \hls{}(x, y; d)$ with $d \in \NN$.
\end{lemma}

\begin{proof}
The constraint that $A$ represents a  non-empty heap means that 
the inductive rule defining $\hls{}$ in Equation~(\ref{eq:hlsv-rec})
should be applied at least once. Notice that the semantics of this rule
defines, at each inductive step,
a memory block starting at $x$ and ending before $x'$ of size $x'-x$.
By induction on $k \ge 1$, we obtain that $\hls{k}(x, y; z)$ defines
a memory block of length $y-x$ such that 
$2 k \le y-x \le k z$. Then $\hls{}(x, y; z)$ is summarized by the formula $\exists k.\ k \ge 1 \wedge 2k \le y -x \le kz$, which is a non-linear arithmetic formula.

%Let $t^\infty \equiv d_0 + \sum_{i=1}^n d_i v_i$ 
%with $d_i$ ($i\in\{0..n\}$) constants in $\NN$ 
%	such that at least one $d_i > 0$ for $i\in\{1..n\}$,
%and $v_i$ ($i\in\{1..n\}$) variables in $\cV$.
The formula $\exists k.\ k \ge 1 \wedge 2k \le y -x \le kz$ is actually equivalent to the disjunction of the two formulas corresponding to the following two  cases:
\begin{itemize}
\item If $2 = z$,  then $\abs^+(\hls{}(x, y; z))$ has the formula $\exists k.\ k \ge 1 \land y -x = 2k$ as a disjunct.
%
\item If $2 < z$, then we consider the following sub-cases:  
\begin{itemize}
\item if $k = 1$ 
	then $\abs^+(\hls{}(x, y; z))$ contains the formula
		$ 2 \leq y-x \le z$ as a disjunct;
		
\item if $k \ge 2$, then we observe that the intervals 
	$[2k, k z]$ and $[2(k+1), (k+1) z]$
	overlap.
	Indeed, 
\begin{eqnarray*}
k z - 2(k+1) & = & 
k (z -2) - 2 
 \ge k - 2 \ge 0.
\end{eqnarray*}
    Therefore, $\bigcup \limits_{k \ge 2} [2k, kz] = [4, \infty)$. It follows that the formula 
    $\exists k.\ k \ge 2 \land 2k \le y-x \le k z$ 
    is equivalent to $4 \le y-x$. Therefore, $\abs^+(\hls{}(x, y; z))$ contains $4 \le y-x$ as a disjunct.
\end{itemize}
\end{itemize}
To sum up, we obtain
\begin{eqnarray*}
\abs^+(\hls{}(x, y; z)) & \triangleq 
          & \big(2 = z 
				 ~\land~ \exists k.\ k \ge 1 \land 2k = y-x \big) \\
   & \lor & \big(2 < z
            ~\land~
                   \big(2 \leq y-x \le z \lor 4 \le y-x 
                          \big) 
            \big),
\end{eqnarray*}
which can be further simplified into 
\begin{eqnarray*}
\abs^+(\hls{}(x, y; z)) & \triangleq 
          & \big(2 = z 
				 ~\land~ \exists k.\ k \ge 1 \land 2k = y-x \big) \\
   & \lor & \big(2 < z 
            ~\land~ 2 \le y - x
                          \big).
\end{eqnarray*}
%
%If $t^\infty \equiv \infty$, 
%then $\abs^+(A) \triangleq \exists k \cdot k \ge 1 \land k c \leq y-x$.
%
%For $ \hls{}(x, y; \infty)$, we replace $z$ in $\abs^+(\hls{}(x, y; z))$ by $\infty$, simplify the formula, and deduce that
%$\abs^+(\hls{}(x, y; \infty)) \triangleq  2 \le y - x $.
%
%For $ \hls{}(x, y; d)$ with $d \in \NN$, we replace $z$ in $\abs^+(\hls{}(x, y; z))$ by $d$,   
%$$\abs^+(\hls{}(x, y; d)) \triangleq \exists k.\ k \ge 1 ~\land~ 2k \le y - x \le dk.$$
%\begin{eqnarray*}
%\abs^+(\hls{}(x, y; d)) & \triangleq 
 %         & \big(2 = d 
%				 ~\land~ \exists k.\ k \ge 1 \land 2k = y-x \big) \\
%   & \lor & \big(2 < d
 %           ~\land~
 %                  \big(2 \leq y-x \le z \lor 4 \le y-x 
 %                         \big) 
 %           \big).%
%\end{eqnarray*}
%This constraint is unsatisfiable if $c > d_0$.
\qed
\end{proof}




\subsection{\EPbA\ abstraction for \slah\ formulas}
\label{ssec:sat-abs}

We are going to utilize $\abs^+(\hls{}(x, y; z))$ in the above section 
to obtain in polynomial time an equi-satisfiable \EPbA\ abstraction for a symbolic heap $\varphi$, denoted by $\abs(\varphi)$.
This shows that the satisfiability problem of \slah\ is in NP. Moreover, it enables using the off-the-shelf SMT solvers e.g. Z3 to solve the satisfiability problem of \slah\ formulas.
This abstraction extends the ones proposed in~\cite{BrotherstonGK17,DBLP:journals/corr/abs-1802-05935} for ASL to deal with the inductive predicate $\hls{}$.

We introduce some notations first.
For a spatial atom $a$, we define the head and tail addresses of $a$, 
	denoted by $\atomhead(a)$ and $\atomtail(a)$, as follows:
\begin{itemize}
\item if $a \equiv t_1 \pto t_2$ 
	  then $\atomhead(a)\triangleq t_1$, $\atomtail(a)\triangleq t_1+1$,
%
\item if $a\equiv \blk(t_1, t_2)$
	  then $\atomhead(a)\triangleq t_1$ and $\atomtail(a)\triangleq t_2$,
%
\item if $a\equiv \hls{}(t_1, t_2; t_3)$
      then $\atomhead(a) = t_1$ and $\atomtail(a)  = t_2$.
\end{itemize}
%
Given a formula $\varphi\equiv \Pi : \Sigma$, 
$\atoms(\varphi)$ denotes the set of spatial atoms in $\Sigma$, and $\patoms(\varphi)$ denotes the set of predicate atoms in $\Sigma$. 

The abstraction of $\varphi\equiv \Pi : \Sigma$, 
	denoted by $\abs(\varphi)$ is 
	the formula $\Pi\wedge\phi_{\Sigma}\wedge\phi_*$
	where:
\begin{itemize}
\item $\phi_{\Sigma}\triangleq\bigwedge\limits_{a \in \atoms(\varphi)}\abs(a)$ such that
{\small
\begin{eqnarray}
\abs(t_1\mapsto t_2) & \triangleq & \ltrue \\
\abs(\blk(t_1,t_2))  & \triangleq & t_1<t_2 \\
\abs(\hls{}(t_1, t_2, t_3))  & \triangleq & (\isemp_a \land t_1=t_2) \\
& \lor &( \lnot \isemp_a \land \abs^+ (\hls{}(x, y;z))[t_1/x, t_2/y, t_3/z]), 
\end{eqnarray}
}
where the boolean variables $\isemp_a$ are introduced for each atom $a \in \patoms(\varphi)$ to encode whether $a$ denotes an empty heap.


\item $\phi_\sepc\triangleq\phi_1\wedge \phi_2 \wedge \phi_3$ 
	specifies the semantics of separating conjunction, 
where 	
{\small
\begin{eqnarray}
\phi_1 & \triangleq & \bigwedge\limits_{a_i,a_j \in \patoms(\varphi), i<j} 
	\begin{array}[t]{l}
	(\lnot \isemp_{a_i} \land \lnot \isemp_{a_j}) \limp \\
	\quad (\atomtail(a_j) \le \atomhead(a_i)\lor \atomtail(a_i) \le \atomhead(a_j))
	\end{array}
\\
\phi_2 & \triangleq & \bigwedge\limits_{a_i \in \patoms, a_j \not \in \patoms} 
	\begin{array}[t]{l}
	(\lnot \isemp_{a_i}) \limp \\
	\quad (\atomtail(a_j) \le \atomhead(a_i) \lor \atomtail(a_i) \le \atomhead(a_j))
	\end{array}
\\
\phi_3 & \triangleq & \bigwedge\limits_{a_i, a_j \not \in \patoms(\varphi),  i < j} 
		\atomtail(a_j) \le \atomhead(a_i) \lor \atomtail(a_i) \le \atomhead(a_j)
\end{eqnarray}
}
\end{itemize}

For formulas $\varphi\equiv \exists \vec{z}\cdot \Pi : \Sigma$,
we define $\abs(\varphi) \triangleq \abs(\Pi:\Sigma)$ since $\exists \vec{z}\cdot \Pi : \Sigma$ and $\Pi:\Sigma$ are equi-satisfiable.
% satisfiability
%implies an existential quantification of free variables of the formulas.


\begin{proposition}\label{prop-sat-correct}
A \slah\ formula $\varphi$ is satisfiable iff $\abs(\varphi)$ is satisfiable.
\end{proposition}

\begin{proof}
If $\varphi$ is satisfiable then there exists a stack $s$ and a heap $h$
such that $s,h\models \varphi$. We build an interpretation $I$ of free variables in
$\abs(\varphi)$ by using $s$ for location variables and $h$ for the boolean variables $\isemp_a$ introduced for predicate atoms $a$.
The semantics of \slah\ implies that at least one disjunct in $\abs(a)$
is true for every predicate atom $a$. Therefore, $I \models \phi_\Sigma$. Moreover, $I \models \phi_\sepc$, as a result of the semantics of separating conjunction. Thus $\abs(\varphi)$ is satisfiable.

If $\abs(\varphi)$ is satisfiable, from the interpretation $I$ of its variables,
we built a stack $s$ for location variables such that the pure constraint of $\varphi$ is satisfied (trivially by the definition of  $\abs(\varphi)$).
The heap $h$ is built as follows: 
\begin{itemize}
\item For each points-to atom $t_1 \pto t_2$, $h(I(t_1)) = I(t_2)$.
%
\item For each block atom $\blk(t_1, t_2)$, $h(i) = i$ for each $i \in [I(t_1), I(t_2)-1]$.
%
\item For every predicate atom $\hls{}(t_1,t_2; t_3)$, from the definition of $\abs^+(\hls(x,y; z))$, we know that there is $k \ge 1$ such that $\hls{}(t_1,t_2; t_3)$ can be unfolded for $k$ times to cover the memory block from $I(t_1)$ to $I(t_2)$, with each unfolding corresponding to a memory chunk of size between $2$ and $I(t_3)$. Based on the unfoldings,  we can define $h(i)$ for every $i \in [I(t_1), I(t_2)-1]$.
\end{itemize}
From the definition of $s$ and $h$, we know that $s, h \models \varphi$. Therefore, $\varphi$ is satisfiable.
%Let $k$ be the number of these unfoldings and $k$ addresses in $[I(t_1),I(t_2)-1]$
%at distance between $[c,I(t^\infty)]$ denoting the start of chunks in this atom.
%The others values in the heap are chosen arbitrary.
%Therefore, we obtain a model for $\varphi$ which satisfies all the constraints in its semantics.
\qed
\end{proof}
% ESOP'21, Reviwer 3: At the end of page 10, when discussing the abstraction idea, I wondered to what extent this is related to the decision procedures for ASL itself; is the overall approach new, or is it a similar approach with suitable extensions?

From the construction of $\abs(\varphi)$, we know that  $\abs(\varphi)$ is in {\EPbA} and the size of $\abs(\varphi)$ is polynomial in that of $\varphi$. From the fact that the satisfiability of \EPbA\ is in NP, we conclude that the satisfiability of \slah\ is in NP.\todo{MS: what is new?}
%Indeed, let $n$ be the initial number of variables and $\sigma$ the number of spatial atoms.
%The \PbA\ abstraction has $n+\sigma$ variables because of added boolean variables.
%The $\hls{}$ atoms produces a linear increase of the formula,
%and may introduce an existentially quantified variable.
%The biggest impact in the size of the abstraction is the translation of
%the separating conjunction, $\varphi_\sepc$, which size is in $O(\sigma)$.

\begin{remark}
The Boolean variables $\isemp_a$ in $\abs(\varphi)$ are introduced for improving the readability. They are  redundant in the sense that they can be equivalently replaced by the formula $\atomhead(a) = \atomtail(a)$. Moreover, the formulas $\exists k.\ k \ge 1 \wedge 2k = y - x $ in $\abs^+(\hls{}(x, y; z))$ can be equivalently replaced by $y -x > 0 \wedge y -x \equiv 0 \bmod 2$. Therefore, $\abs(\varphi)$ is essentially a quantifier-free \PbA\ formula containing modulo constraints. The satisfiability of such formulas is still NP-complete. From now on, we shall assume that {\bf $\abs(\varphi)$ is a quantifier-free \PbA\ formula containing modulo constraints}.
\end{remark}

