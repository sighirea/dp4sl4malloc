
%!TEX root = tacas2021.tex
\section{The logic \paslid}
\label{sec:paslid}


\begin{eqnarray*}
hls(x, b_1; y, b_2; v) & \Leftarrow & (x=y \land b_1 = b_2: \emp) \\
hls(x, b_1; y, b_2; v) & \Leftarrow & \\
& & \hspace{-6mm}\exists x',  f \cdot (b_1 = 0 \wedge f = 1 \wedge 3 \le x' - x < v) \vee  (b_1 = 1 \wedge f = 0 \wedge 3 \le x' - x): \\
& & x \pto x' - x \sepc x+1 \pto f \sepc \blk(x+2, x') \sepc hls(x', f; y, b_2; v).
\end{eqnarray*}


In this section, we define the logic {\paslid}, including syntax, semantics, and a ``well-formed'' condition which facilitate the decision procedure design.


\begin{definition}[\paslid\ Syntax]\label{def:syn-paslid}
Let $\cV$ denote an infinite set of address variables ranging over $\NN$, the set of natural numbers. Then the syntax of {\paslid} is defined by the following rules, 
%where $t$ denotes terms, $\Pi$ denotes pure formulas,  $\Sigma$ denotes spatial formulas, $SH$ denotes symbolic heaps,
\begin{eqnarray}
t & ::= & x \mid n \mid t+t \label{eq:syn-syn-term} %\mid nt
\\[1mm]
\Pi & ::= & t=t \mid t \ne t \mid t \leq t \mid t < t \mid \Pi \land \Pi
\\[1mm]
\Sigma & ::= & \emp \mid t \pto t \mid \blk(t, t) \mid P(t, \vec{t}; t, \vec{t}; \vec{t}) \mid \Sigma \sepc \Sigma
\\[1mm]
\varphi & ::= & \exists \vec{z}\cdot \Pi : \Sigma
\end{eqnarray}
where $t$ denotes address terms, $\Pi$ denotes pure formulas,  $\Sigma$ denotes spatial formulas, $\varphi$ denotes symbolic heaps,  $x$ and $\vec{z}$ are variables from $\mathcal{V}$, 
$n \in \NN$, $\vec{t}$ is a vector of address terms, and $P$ is a linear inductively defined predicate (cf. Definition~\ref{def:lid-hls}). 
Whenever one of $\Pi$ or $\Sigma$ is empty, we omit the colon. 
%We write $FV (A)$ for the set of free variables occurring in $A$. 
%If $A = \exists\vec{z}\cdot\Pi:\Sigma$, we write $qf(A)$ for $\Pi:\Sigma$, the quantifier-free part of $A$.
For convenience, we suppose that $\mathcal{V}$ includes a special address variable $nil$.
\end{definition}


\begin{definition}[Linear inductively defined predicates]\label{def:lid-hls}
The inductive definition of a linear predicate $P$ 
has \emph{exactly one base rule} of the form:
{\small
\begin{eqnarray*}
P(x,\vec{z};y,\vec{u};\vec{v}) & \Leftarrow & (x=y \land \vec{z}=\vec{u} : \emp) \label{eq:ID-emp}
\end{eqnarray*}
and \emph{exactly one recursive rule} of the form:
\begin{eqnarray*}
P(x,\vec{z}; y,\vec{u}; \vec{v}) & \Leftarrow & \\
& & \hspace{-6mm}\big(\exists \vec{w}, x', \pvec{z}'\cdot \Pi(x, \vec{z}, \vec{w},x', \pvec{z}',\vec{v}) :
\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) \sepc P(x',\pvec{z}';y,\vec{u};\vec{v})
\big)\quad \label{eq:ID-rec}
\end{eqnarray*}
}
where $|\vec{z}|=|\pvec{z}'|=|\vec{u}|\ge 0$,  
%$w_0$ is the first component of $\vec{w}$, 
$\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v})$ is a spatial formula containing only points-to or block atoms.
The pure constraint $\Pi$ in the recursive rule shall be satisfiable.
The parameters $x$ and $y$ are called \emph{start} resp. \emph{end} parameters
    because they delimit the heap segment defined by the predicate;
    notice that, the semantics constrains the start location to be inside
    the heap and end location to be outside the heap (c.f. the semantics below).
The parameters in $\vec{z}$ are called \emph{start update} 
	because they are updated at the recursive call.
The parameters $\vec{u}$ are called \emph{end glue} 
	because they are not changed in the recursive call but are constrained by the base rule.
The parameters $\vec{v}$ are called static parameters 
	because they stay the same in the recursive call and are unconstrained in the base rule. 
\end{definition}

%Although we do not require non aliasing of start and ending parameters in
%the inductive rule of linear definitions, this aliasing is implicit.
%Indeed, the successor of a cell in a heap list is specified in the chunk header
%and its is usually obtained by pointer arithmetics using a strictly positive value.
%Therefore, the heap-list segments are only linear blocks of values, 
%they can not include loops (lassos).

The model of a formula is a pair $(I, h)$ composed of 
an \emph{interpretation} $I$ of logic variables such that $nil$ is mapped always to $0$
and a \emph{heap} $h$ mapping locations to values. 
For simplicity, we consider that both locations and values are natural numbers, therefore $I:\mathcal{V}\tfun\NN$ and $h:\NN\pfun\NN$.
%%MS: this really maps what happens in memory models for DMA because 
%% the fields of the header are either positive values (status or size) 
%% or locations; the content of the block is not interpreted.

In \paslid, semantics of terms need only the interpretation $I$. 
However, to be uniform, we interpret all the terms and formulas on $(I, h)$. 
%we will extend these terms with operator that require the heap.
%Therefore, we provide here a unified semantics for terms that employs the heap also.

\begin{definition}[Semantics of \paslid\ terms]
The evaluation of a term in a model $(I,h)$ leads to a natural number as follows:
$$ %\begin{eqnarray*}
\sem n \antic_{I,h}\ = \ n 
\quad\quad 
\sem x \antic_{I,h} \ = \ I(x) 
\quad\quad 
\sem t_1 + t_2 \antic_{I,h} \ =\ \sem t_1 \antic_{I,h} + \sem t_2 \antic_{I,h} 
$$ %\end{eqnarray*}
\end{definition}

To give the semantics of formulas, we use the following notations.
For a variable $v\in\mathcal{V}$ and $m\in\NN$, we denote by $I[v\mapsto m]$ for the interpretation defined as $I$ but with $v$ mapped to $m$. Interpretations are extended in a natural way to tuples.
The domain of a heap is denoted by $\wpos{h}$. 
% and the heap with empty domain is $e$.
Two heaps $h_1$ and $h_2$ are composed by $h_1\uplus h_2$ if they have disjoint domains, otherwise the composition is undefined.


\begin{definition}[Semantics of \paslid\ formulas]
The satisfaction relation $I,h\models A$ with $I$ an interpretation of free variables in $A$ and $h$ a heap, is defined by structural induction on $A$ as follows:
\begin{eqnarray*}
I,h \models t_1 \sim t_2 & \mbox{ iff } & 
	\sem t_1 \antic_{I,h} \sim \sem t_2 \antic_{I,h}\mbox{ where }\sim\in\{=,\ne,\le,<\}
\\
I,h \models \Pi_1 \land \Pi_2 & \mbox{ iff } &
	I,h \models \Pi_1 \mbox{ and } I,h\models \Pi_2
\\
I,h \models \emp & \mbox{ iff } & 
	\wpos{h}=\emptyset
\\
I,h \models t_1 \pto t_2 & \mbox{ iff } & 
	\exists n\in\NN\mbox{ s.t. }
	\sem t_1 \antic_{I,h}=n,\ \wpos{h}=\{n\}\mbox{ and } h(n)=\sem t_2 \antic_{I,h}
\\
I,h \models \blk(t_1, t_2) & \mbox{ iff } & 
	\exists n,n'\in\NN\mbox{ s.t. }
	\sem t_1 \antic_{I,h}=n,\ \sem t_2 \antic_{I,h}=n',\ n < n'\mbox{ and }\\
	& & \quad \wpos{h}=\{n,...,n'-1\}
\\
I,h \models \Sigma_1 \sepc \Sigma_2 & \mbox{ iff } & 
	\exists h_1,h_2\mbox{ s.t. } h=h_1\uplus h_2\mbox{ and }
	I,h_1\models\Sigma_1 \mbox{ and } I,h_2\models\Sigma_2
\\
I,h \models \exists\vec{z}\cdot\Pi:\Sigma & \mbox{ iff } &
	\exists \vec{m}\in\NN^{|\vec{z}|}\mbox{ s.t. } 
	I[\vec{z}\mapsto\vec{m}],h\models\Pi \mbox{ and } I[\vec{z}\mapsto\vec{m}],h\models\Sigma	
\end{eqnarray*}
\end{definition}
The semantics of the linear inductively defined predicates $P$ are defined as follows,
\begin{eqnarray*}
I,h\models P(t_1, \overrightarrow{t'_1}; t_2, \overrightarrow{t'_2}; \overrightarrow{t''}) & \mbox{ iff } & 
	\exists k\in\NN\cdot I,h\models P^k(t_1, \overrightarrow{t'_1}; t_2, \overrightarrow{t'_2}; \overrightarrow{t''})
\\
I,h\models P^0(t_1, \overrightarrow{t'_1}; t_2, \overrightarrow{t'_2}; \overrightarrow{t''}) & \mbox{ iff } & 
    I,h\models (t_1=t_2 \land \overrightarrow{t'_1} =\overrightarrow{t'_2} : \emp)
\\
I,h\models P^{k+1}(t_1, \overrightarrow{t'_1}; t_2, \overrightarrow{t'_2}; \overrightarrow{t''}) & \mbox{ iff } & 
\end{eqnarray*}
%
\[ 
\small
I,h\models \big(\exists \vec{w}, x', \pvec{z}'\cdot \Pi(t_1, \overrightarrow{t'_1}, \vec{w}, x', \pvec{z}', \overrightarrow{t''}) :
\Sigma(t_1, \overrightarrow{t'_1}, \vec{w}, x', \pvec{z}', \overrightarrow{t''}) \sepc P^k(x',\pvec{z}'; t_2, \overrightarrow{t'_2}; \overrightarrow{t''})
\big)
\]
%
where the recursive rule of $P$ is of the form 
\begin{eqnarray*}
\small
P(x,\vec{z};y,\vec{u};\vec{v}) & \Leftarrow & \\
&& \hspace{-6mm}\big(\exists \vec{w},x', \pvec{z}'\cdot \Pi(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) :
\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) \sepc P(x',\pvec{z}';y,\vec{u};\vec{v})
\big).\quad \label{eq:ID-rec}
\end{eqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\hide{
This semantics differs from the one given in~\cite{BrotherstonGK17} in the definition of contiguous arrays of values because we consider that in $\blk(\ell,\ell')$, the location $\ell'$ is the first after the last location in the array, like proposed first by in~\cite{CalcagnoDOHY06} for the static analysis of memory allocators.
Moreover, this form is more convenient to have the lemmata about blocks:
\begin{equation}\label{lemma:blk}
\blk(t_1,t_2)\sepc\blk(t_2,t_3)\limp\blk(t_1,t_3),
\end{equation}
i.e., such block definition is compositional.
Similarly to \cite{BrotherstonGK17}, the block predicate may be employed
with the absolute addressing $\blk(\ell,\ell')$ or
with base-offset addressing $\blk(\ell, i, j)$ for the array from $\ell+i$ to $\ell+j-1$.

%\begin{definition}[Generic semantics]\label{def:sem-hls}
The satisfaction relation $I,h\models A$ with $I$ an interpretation of free variables in $A$ and $h$ a heap, is defined by extending the satisfaction relation in \sla\ by the following rules:
\begin{eqnarray*}
I,h\models \hck(x, \vec{w}) & \mbox{ iff } & 
	I,h\models \blk(x;\vec{w}_0)
\\
I,h\models P(x,\vec{z};y,\vec{u};\vec{v}) & \mbox{ iff } & 
	\exists k\in\NN\cdot I,h\models P^k(x,\vec{z};y,\vec{u};\vec{v})
\\
I,h\models P^0(x,\vec{z};y,\vec{u};\vec{v}) & \mbox{ iff } & 
    I,h\models (x=y \land \vec{z}=\vec{u} : \emp)
\\
I,h\models P^{k+1}(x,\vec{z};y,\vec{u};\vec{v}) & \mbox{ iff } & 
	\mbox{there exists a recursive rule } \\
& & P(x,\vec{z};y,\vec{u};\vec{v})\Leftarrow \exists w, \pvec{z}'\cdot \varphi(P(w,\pvec{z}';y,\vec{u};\vec{v}))\mbox{ s.t. } \\
& & I,h\models \exists w, \pvec{z}'\cdot \varphi(P^k(w,\pvec{z}';y,\vec{u};\vec{v}))
\end{eqnarray*}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\end{definition}


\begin{example}\label{exmp-id}
%Heap lists where the sizes of all blocks are less than the size $v$ can be specified by the predicate $\hls{}(x; y ; v)$ below,
%\begin{eqnarray*}
%\hls{}(x;y; v) & \Leftarrow & x=y : \emp,
%\\
%\hls{}(x;y; v) & \Leftarrow & 
%\exists w, st, nx \cdot  3 < w - x < v \land 0\leq st \leq 1: x \pto w-x \sepc x+1\pto st   \\
%		& & \sepc\ x+2\pto nx  \sepc\ \blk(x+3; w)  \sepc \hls{}(w; y; v).
 %
%\hls{}(x;y) & \Leftarrow & \exists w\cdot \hck(x;w)\sepc\hls{}(w;y) \label{id:hls-rec}
%\\
%\hck(x;y) & \Leftarrow & \exists sz,st,nx\cdot\begin{array}[t]{l}
% 		sz > 3 \land y=x+sz \land 0\leq st \leq 1: \\
 %		x\pto sz \sepc x+1\pto st \sepc x+2\pto nx 
%		\sepc\ \blk(x+3;x+sz)  %%\blk(x,3,sz)
%		\end{array}\label{id:hck}
%\end{eqnarray*}
Heap lists where the sizes of all blocks are less than a variable $v$ and no two adjacent blocks are both free can be specified by the predicate $\hls{}(x, b_1; y, b_2; v)$ below,
\begin{eqnarray*}
\hls{}(x, b_1; y, b_2; v) & \Leftarrow & x=y \wedge b_1 = b_2 : \emp,
\\
\hls{}(x, b_1; y, b_2; v) & \Leftarrow & 
\exists x', st \cdot  3 < x' - x < v \land 0\leq b_1 \leq 1 \wedge 0\leq st \leq 1 \wedge b_1 \neq st:    \\
		& & x \pto x'-x \sepc x+1\pto st \sepc \blk(x+2; x')  \sepc \hls{}(x', st; y, b_2; v).
\end{eqnarray*}
\end{example}
 
%The semantics of $\hck$ has to be refined by its several instances
%described in the remaining of this section.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\hide{
\subsection{Overview of variants and fragments of \slhl}

Table~\ref{tab:frg} provides the list of fragments we consider for \slhl\
and their main features.
Given a set of predicate names $A$ ($\pto$ and $\blk$ included), 
the fragment $\slhl(A)$ contains formulas whose spatial part is built
only from atoms using the predicate symbols in $A$.
Notice that $\emp$ is always included in the sets $A$.
%
In particular, the fragment $\sla$ is also the fragment $\slhl(\pto,\blk)$.
The fragment $\slhl(\hls{a},\pto)$ contains formula built with spatial atoms 
$t\pto u$
and $\hls{a}(x;y)$ where $\hls{a}$ is the instance of $\hls{}$
calling predicate $\hck^a$ defined in section~\ref{ssec:hcka}.

\begin{table}[t]
\caption{Fragments of $\slhlsf$}
\label{tab:frg}
\begin{center}
\begin{tabular}{l|c|c|c|c|l|l}
\textit{Fragment} & $\pto$ & $\blk$ & $\hck$ & $\hls{}$ 
				  & \textit{Sat} & \textit{Ent}
\\\hline\hline
\sla              & \checkmark & \checkmark &  &  
			      & \ref{ssec:sla-sat} & \ref{ssec:sla-ent}
\\\hline			      
$\slhl(\hls{a?})$  &   &   & $\hck^{a?}$ & $\hls{a?}$
			      & \multicolumn{2}{|c}{\ref{ssec:dp-hls-a}}
\\\hline			      
$\slhl(\hls{s?})$  &   &   & $\hck^{s?}$ & $\hls{s}$
			      & \multicolumn{2}{|c}{\ref{ssec:dp-hls-s}}
\\\hline			      
$\slhl(\hls{x},
      \pto)$      & \checkmark &    & $\hck^x$ & $\hls{x}$  
			      & \multicolumn{2}{|c}{\ref{sec:dp-hls-pto}}
\\\hline			      
\end{tabular}
\end{center}
\end{table}%



\subsection{Heap chunk with address field}
\label{ssec:hcka}

The following definition of $\hck$ captures the case of chunk headers 
storing the address of the next chunk in the list:
\begin{equation}
\hck^a(x,y,st) \triangleq 
		x+2 < y ~:~ %\land 0\le st \le 1
		x\pto y \sepc x+1\pto st \sepc \blk(x+2;y)
\label{eq:hcka}
\end{equation}

The semantics of the $\hck^a$, $I,h\models \hck^a(x,y,st)$ is defined by:
\begin{eqnarray*}
I,h & \models & x+2 < y %\land 0\le st \le 1
				~:~ x\pto y \sepc x+1\pto st \sepc \blk(x+2;y)
\end{eqnarray*}

The inductive definition of the heap list predicate for this kind of chunk 
is given by the following two rules:
\begin{eqnarray}
\hls{a}(x;y) & \Leftarrow & (x=y : \emp)  \\
\hls{a}(x;y) & \Leftarrow & \big(\exists w,f\cdot \hck^a(x,w,f) \sepc \hls{a}(w;y)\big)\label{eqn:hlsa}
\end{eqnarray}

The following version of the heap list predicate based 
on the $\hck^a$ uses the status field to avoid adjacent free chunks:
\begin{eqnarray}
\hls{ac}(x,st;y,st') & \Leftarrow & (x=y \land st=st' : \emp)  \\
\hls{ac}(x,st;y,st') & \Leftarrow & \big(\exists w,f\cdot st \neq f ~:~ \hck^a(x,w,f) \sepc \hls{ac}(w,f;y,st')\big)
\end{eqnarray}

The next version of $\hls{}$ allows adjacent free chunks, which is the case
for allocators with lazy coalescing of free chunks:
\begin{eqnarray}
\hls{al}(x,st;y,st') & \Leftarrow & (x=y \land st=st' : \emp)  \\
\hls{al}(x,st;y,st') & \Leftarrow & \big(\exists w,f\cdot \hck^a(x;w;f) \sepc \hls{al}(w,f;y,st')\big)
\end{eqnarray}


\subsection{Heap chunk with size field}
\label{ssec:hcks}

The following definition of $\hck^s$ captures the case of chunk headers 
storing the size of the chunk:
\begin{equation}
\hck^s(x,y,st,sz) \triangleq \begin{array}[t]{l}
 sz > 3 \land y=x+sz ~:~ % \land 0\le st \le 1
 x\pto sz \sepc x+1\pto st \sepc \blk(x,3,sz)
 \end{array}\label{eq:hcks}
\end{equation}

%
The semantics of the $\hck^s$, $I,h\models \hck^s(x,y,st,sz)$ is defined by:
\begin{eqnarray}
I,h &\models & \exists sz,st,nx\cdot\begin{array}[t]{l}
 		sz > 3 \land y=x+sz ~:~ %\land 0\le st \le 1
 		x\pto sz \sepc x+1\pto st \\
		\sepc\ \blk(x,3,sz)
 		\end{array}
\end{eqnarray}

The following three versions of the heap list predicate employ 
the atom $\hck^s$ to define respectively 
heap list without constraints of chunk status, 
heap lists for free chunk coalescing and 
heap lists with the free status exposed:
\begin{eqnarray}
\hls{s}(x;y) & \Leftarrow & (x=y : \emp)  \\
\hls{s}(x;y) & \Leftarrow & \big(\exists w,st,sz\cdot \hck^s(x,w,st,sz) \sepc \hls{s}(w;y)\big)
\\[2mm]
\hls{sc}(x,st;y,st') & \Leftarrow & (x=y \land st=st' : \emp)  \\
\hls{sc}(x,st;y,st') & \Leftarrow & \big(\exists w,f,sz\cdot st \neq f : \hck^s(x,w,f,sz) \sepc \hls{sc}(w,f;y,st')\big)~~~~~~
\\[2mm]
\hls{sl}(x,st;y,st') & \Leftarrow & (x=y \land st=st' : \emp)  \\
\hls{sl}(x,st;y,st') & \Leftarrow & \big(\exists w,f,sz\cdot \hck^s(x,w,f,sz) \sepc \hls{sc}(w,f;y,st')\big)
\end{eqnarray}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\subsection{Well-formed inductive definitions for heap lists}

\hide{
The inductive definitions of $\hls{*}$ predicates given above 
belong to a class of definitions that we call 
\emph{well-formed}.\mihaela{Not sure that this is useful here}

For readability, we refer to the data stored in the header of chunks 
using terms whose syntax is:
\begin{eqnarray}
t & ::= & x \mid n \mid \fsz(x) \mid \fif(x) \mid t+t \label{eq:syn-SLA-term} %\mid nt
%
\end{eqnarray}
A term $t$ of the form $\fsz(x)$ denotes the size of the such 
and it is interpreted as 
$\sem \fsz(x) \antic_{I,h} = h(I(x))$ if $I(x)\in\wpos{h}$ 
or undefined otherwise.
Similarly, $\fif(x)$ denotes the status of the chunk
and it is interpreted as
$\sem \fif(x) \antic_{I,h} = h(I(x)+1)$ if $I(x),I(x)+1\in\wpos{h}$
or undefined otherwise.
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\hide{
\begin{definition}[Well-formed linear inductive definitions in \paslid]\label{def:LID-hls-WF}
A linear inductive definition of a predicate $P$ in $\mathcal{F}$
is \emph{well-formed} in \slhl\ iff every recursive rule
$P(x,\vec{z};y,\vec{u};\vec{v})\Leftarrow \exists w,\pvec{z}'\cdot \Pi(x,\vec{z},w,\vec{z}',\vec{v}) : \hck(x;w)\sepc\ P(w,\pvec{z}';y,\vec{u};\vec{v})$
employs a restricted form of pure constraints:\todo{Not checked!}
\begin{eqnarray*}
\Pi(x,\vec{z},w,\pvec{z}',\vec{v}) & ::= &
\bigwedge_i \Omega_i(\fsz(x),\vec{v}_i)
\land \bigwedge_{\ell,k\in\{\fif(x)\}\cup\vec{z}\cup\pvec{z}'} \Delta(\ell,k)
\\
\Omega(x,y) & ::= & \textit{true} \mid x < y \mid x > y
\\
\Delta(x,y) & ::= & \textit{true} \mid x-y \sim n
\end{eqnarray*}
where $n \in \NN$, $\sim\in\{=,\ne,<,\le,\ge,>\}$.
\end{definition}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


In the following, we introduce a condition called ``well-formedness'' , so that under this condition decision procedures can be achieved for \paslid. 

For a spatial atom $a$, we define the head and tail of $a$, denoted by $\atomhead(a)$ and $\atomtail(a)$, as follows.
\begin{itemize}
\item if $a= t_1 \pto t_2$, then $\atomhead(a) =t_1$, $\atomtail(a) = t_1+1$,
%
\item if $a = \blk(t_1, t_2)$, then $\atomhead(a) = t_1$ and $\atomtail(a)=t_2$,
%
\item if $a = P(t_1, \overrightarrow{t'_1}; t_2, \overrightarrow{t'_2}; \overrightarrow{t''})$, then $\atomhead(a) = t_1$ and $\atomtail(a)  = t_2$.
\end{itemize}

A spatial formula $\Sigma$ consisting of only points-to or block atoms is \emph{consecutive} if it is of the form $a_1 \sepc a_2 \sepc \cdots \sepc a_n$ such that for each $i: 1 \le i < n$, $\atomtail(a_i) = \atomhead(a_{i+1})$.

\begin{definition}[Well-formed linear inductive predicates]\label{def:LID-hls-WF}
A linear inductive predicate
$P(x,\vec{z};y,\vec{u};\vec{v})$ where the recursive rule is of the form
%
\begin{eqnarray*}
P(x,\vec{z};y,\vec{u};\vec{v}) & \Leftarrow & \\
& & \hspace{-8mm} \big(\exists \vec{w},x', \pvec{z}'\cdot \Pi(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) :
\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) \sepc P(x',\pvec{z}'; y,\vec{u};\vec{v})
\big)\quad \label{eq:ID-rec}
\end{eqnarray*}
%
is said to be \emph{well-formed} if 
\begin{description}
\item[C1.] $\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) \sepc P(x',\pvec{z}'; y,\vec{u};\vec{v})$ is consecutive, $\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v})$ starts with a points-to atom $x \pto x'-x$, contains exactly one block atom whose tail must be $x'$, moreover, for every points-to atom $t \pto t'$ in $\Sigma$ different from $x \pto x'-x$, $t'$ is a variable from $\vec{z} \cup \vec{w} \cup \pvec{z}'$,
%
%\item  $\Pi(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) \equiv \Pi_0(x, \vec{w}, x', \vec{v}) \wedge  \bigvee \limits_{1 \le i \le k} \bigwedge \limits_{j \in [|\vec{z}|]} \Pi_{i, j}(z_j, z'_j)$ such that
\item[C2.]  $\Pi(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) \equiv  \bigvee \limits_{1 \le i \le k}  (\Pi_{i,0}(x, x', \vec{v}) \wedge \bigwedge \limits_{j \in [|\vec{w}|]} \Pi_{i,j}(w_j) \wedge  \bigwedge \limits_{j \in [|\vec{z}|]} \Pi_{i, |\vec{w}|+j}(z_j, z'_j))$ such that
\begin{description}
\item[C2.1.] $\Pi_{i,0}(x, x', \vec{v})$ is of the form $n \le x' - x$, $n \le x' - x \le n'$, or $n \le x' - x \le v_j - n'$, with $n, n' \in \NN$ and $j \in [|\vec{z}|]$ (note that here it is required that $x' \ge x$), moreover, if $\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v})$ contains $k$ atoms, then $n \ge k$, 

\item[C2.2.] for each $j \in [|\vec{w}|]$, $\Pi_{i,j}(w_j)$ is of the form $w_j = n$ for $n \in \NN$ (Intuitively, we require that the existentially quantified variables $\vec{w}$ take values from a finite set.),
%
%
%\item each formula $\Pi_{i, j}(z_j, z'_j, \vec{v})$ is of the form $n_1 \le z_j \le n_2 \wedge n'_1 \le z'_j \le n'_2$, where $n_1, n_2, n'_1, n'_2 \in \NN$. (Intuitively, we require that the parameters $\vec{z}, \pvec{z'}$ take values from a finite set.)
\item[C2.3.] for each $j \in [|\vec{z}|]$, $\Pi_{i, |\vec{w}|+j}(z_j, z'_j)$ is of the form $z_j = n_1    \wedge z'_j = n'_1$, where $n_1, n'_1 \in \NN$. (Intuitively, we require that the parameters $\vec{z}, \pvec{z'}$ take values from a finite set.),
%
\item[C2.4.] $\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v})$ uniquely determines $\vec{z}$, specifically, for each $j \in [|\vec{z}|]$,  either $z_j$ occurs in $\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v})$, or there is $z'' \in \vec{w} \cup \pvec{z}'$ such that $z''$ occurs in $\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v})$ and $z''$ uniquely determines $z_j$, more precisely, for every $1 \le i_1 < i_2 \le k$, suppose that $z'' = n_1$ and $z_j = n'_1$ occur in $ \bigwedge \limits_{j \in [|\vec{w}|]} \Pi_{i_1, j}(w_j) \wedge  \bigwedge \limits_{j \in [|\vec{z}|]} \Pi_{i_1, |\vec{w}|+j}(z_j, z'_j)$, moreover, $z'' = n_2$ and $z_j = n'_2$ occur in $ \bigwedge \limits_{j \in [|\vec{w}|]} \Pi_{i_2, j}(w_j) \wedge  \bigwedge \limits_{j \in [|\vec{z}|]} \Pi_{i_2, |\vec{w}|+j}(z_j, z'_j)$, then $n_1 = n_2$ implies $n'_1 = n'_2$, 
%
\item[C2.5.] $\vec{z}$ uniquely determines $\pvec{z}'$, specifically, for every $1 \le i_1 < i_2 \le k$, suppose for each $j \in [|\vec{z}|]$, $\Pi_{i_1, |\vec{w}|+ j}(z_j, z'_j) \equiv z_j = n_{1,j}    \wedge z'_j = n'_{1,j}$  and $\Pi_{i_2,|\vec{w}|+j}(z_j, z'_j) 
\equiv z_j = n_{2,j}    \wedge z'_j = n'_{2,j}$, then $\bigwedge \limits_{j \in [|\vec{z}|]} n_{1,j} = n_{2,j}$ implies $\bigwedge \limits_{j \in [|\vec{z}|]} n'_{1,j} = n'_{2,j}$.
\end{description}
\end{description}
\end{definition}

\begin{example}
The recursive rule of $\hls{}(x, b_1; y, b_2; v)$ in Example~\ref{exmp-id} can be equivalently rewritten into the following rule
\begin{eqnarray*}
\small
\hls{}(x, b_1; y, b_2; v) & \Leftarrow & \\
& & \hspace{-2cm} \exists x', st \cdot  4 \le x' - x \le v-1 \land  (( b_1 = 1 \wedge st = 0) \vee (b_1 = 0 \wedge st = 1 )):    \\
		& & \hspace{-8mm} x \pto x'-x \sepc x+1\pto st \sepc   \blk(x+2; x')  \sepc \hls{}(x', st; y, b_2; v).
\end{eqnarray*}
In the aforementioned recursive rule of $\hls{}(x, b_1; y, b_2; v)$, $st$ occurs in $\Sigma$ and uniquely determines $b_1$, moreover,  $st$ occurs in $\hls{}(x', st; y, b_2; v)$ as the state update parameter and $b_1$ uniquely determines $st$. Therefore, $\hls{}(x, b_1; y, b_2; v)$ is a well-formed linear inductive predicate.

The recursive rule of the $\hls{<}(x;y;v)$ predicate in Section~\ref{sec:overview} can be equivalently rewritten into the following rule 
\begin{eqnarray*}
\hls{<}(x;y;v) & \Leftarrow & \label{id:hlsv-ind}  
\exists x', f \cdot  (3 \le x' - x \wedge f = 0 ) \vee (3 \le x' - x \le v-1 \wedge f= 1 ): \\
&&	x \pto x' - x \sepc x+1 \pto f \sepc\ \blk(x+2; x') \sepc \hls{<}(x'; y; v).
\end{eqnarray*}
Since the predicate $\hls{<}(x;y;v)$ contains neither state update parameters nor end glue parameters, it is easy to see that the aforementioned recursive rule satisfies the constraints in Definition~\ref{def:LID-hls-WF}. Therefore, $\hls{<}(x;y;v)$ is a well-formed linear inductive predicate.
\end{example}

In the rest of this paper, we restrict our attention to \paslid\ formulas where all inductive predicates are well-formed.
%where only one inductive predicate occurs and this predicate is well-formed. 
By abuse of notation, we still use \paslid\ to denote the collection of \paslid\ formulas satisfying this condition.

