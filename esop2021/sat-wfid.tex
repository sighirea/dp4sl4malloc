%!TEX root = esop2021.tex


\section{Well-formed predicates for heap-lists}
\label{sec:sat-wfid}




\begin{definition}[Linear inductively defined predicates]\label{def:lid-hls}
The inductive definition of a linear predicate $P$ 
has \emph{exactly one base rule} of the form:
{\small
\begin{eqnarray*}
P(x,\vec{z};y,\vec{u};\vec{v}) & \Leftarrow & (x=y \land \vec{z}=\vec{u} : \emp) \label{eq:ID-emp}
\end{eqnarray*}
and \emph{exactly one recursive rule} of the form:
\begin{eqnarray*}
P(x,\vec{z}; y,\vec{u}; \vec{v}) & \Leftarrow & \\
& & \hspace{-6mm}\big(\exists \vec{w}, x', \pvec{z}'\cdot \Pi(x, \vec{z}, \vec{w},x', \pvec{z}',\vec{v}) :
\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) \sepc P(x',\pvec{z}';y,\vec{u};\vec{v})
\big)\quad \label{eq:ID-rec}
\end{eqnarray*}
}
where $|\vec{z}|=|\pvec{z}'|=|\vec{u}|\ge 0$,  
%$w_0$ is the first component of $\vec{w}$, 
$\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v})$ is a spatial formula containing only points-to or block atoms.
The pure constraint $\Pi$ in the recursive rule shall be satisfiable.
The parameters $x$ and $y$ are called \emph{start} resp. \emph{end} parameters
    because they delimit the heap segment defined by the predicate;
    notice that, the semantics constrains the start location to be inside
    the heap and end location to be outside the heap (c.f. the semantics below).
The parameters in $\vec{z}$ are called \emph{start update} 
	because they are updated at the recursive call.
The parameters $\vec{u}$ are called \emph{end glue} 
	because they are not changed in the recursive call but are constrained by the base rule.
The parameters $\vec{v}$ are called static parameters 
	because they stay the same in the recursive call and are unconstrained in the base rule. 
\end{definition}


\begin{example}\label{exmp-id}
%Heap lists where the sizes of all blocks are less than the size $v$ can be specified by the predicate $\hls{}(x; y ; v)$ below,
%\begin{eqnarray*}
%\hls{}(x;y; v) & \Leftarrow & x=y : \emp,
%\\
%\hls{}(x;y; v) & \Leftarrow & 
%\exists w, st, nx \cdot  3 < w - x < v \land 0\leq st \leq 1: x \pto w-x \sepc x+1\pto st   \\
%		& & \sepc\ x+2\pto nx  \sepc\ \blk(x+3; w)  \sepc \hls{}(w; y; v).
 %
%\hls{}(x;y) & \Leftarrow & \exists w\cdot \hck(x;w)\sepc\hls{}(w;y) \label{id:hls-rec}
%\\
%\hck(x;y) & \Leftarrow & \exists sz,st,nx\cdot\begin{array}[t]{l}
% 		sz > 3 \land y=x+sz \land 0\leq st \leq 1: \\
 %		x\pto sz \sepc x+1\pto st \sepc x+2\pto nx 
%		\sepc\ \blk(x+3;x+sz)  %%\blk(x,3,sz)
%		\end{array}\label{id:hck}
%\end{eqnarray*}
Heap lists where the sizes of all blocks are less than a variable $v$ and no two adjacent blocks are both free can be specified by the predicate $\hls{}(x, b_1; y, b_2; v)$ below,
\begin{eqnarray*}
\hls{}(x, b_1; y, b_2; v) & \Leftarrow & x=y \wedge b_1 = b_2 : \emp,
\\
\hls{}(x, b_1; y, b_2; v) & \Leftarrow & 
\exists x', st \cdot  3 < x' - x < v \land 0\leq b_1 \leq 1 \wedge 0\leq st \leq 1 \wedge b_1 \neq st:    \\
		& & x \pto x'-x \sepc x+1\pto st \sepc \blk(x+2; x')  \sepc \hls{}(x', st; y, b_2; v).
\end{eqnarray*}
\end{example}


Let $P(x,\vec{z};y,\vec{u};\vec{v})$ be a predicate atom such that the recursive rule of $P$ is of the form
%
\begin{eqnarray*}
\small
P(x,\vec{z};y,\vec{u};\vec{v}) & \Leftarrow & \\
&& \hspace{-8mm}\big(\exists \vec{w},x', \pvec{z}'\cdot \Pi(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) :
\Sigma(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) \sepc P(x',\pvec{z}';y,\vec{u};\vec{v})
\big).\quad
\end{eqnarray*}

Our goal is to compute a summary of $P(x,\vec{z};y,\vec{u};\vec{v})$. 

Suppose 
\begin{eqnarray*}
\small
\Pi(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v}) & \equiv & \\
& & \hspace{-6mm} \bigvee \limits_{1 \le i \le k}  (\Pi_{i,0}(x, x', \vec{v}) \wedge \bigwedge \limits_{j \in [|\vec{w}|]} \Pi_{i,j}(w_j) \wedge  \bigwedge \limits_{j \in [|\vec{z}|]} \Pi_{i, |\vec{w}|+j}(z_j, z'_j)).
\end{eqnarray*}

%We introduce an integer variable $k$ to denote the number of unfoldings of the inductive rule.
The summary computation is based on a model defined in the sequel.
\begin{definition}[Parametric interval weighted transition systems associated with $P$]
The parametric interval weighted labeled transition systems (PIWLTS) associated with $P$, denoted by $\cA_P=(Q,  \delta)$, is defined from $\Pi(x,\vec{z}, \vec{w}, x', \pvec{z}',\vec{v})$ as follows: 
\begin{itemize}
\item 
$Q = Q_{1} \times \cdots \times Q_{|\vec{z}|}$,
%$Q = Q_1 \times \cdots \times Q_{|\vec{w}|} \times Q_{|\vec{w}|+1} \times \cdots \times Q_{|\vec{w}|+|\vec{z}|}$, 
where for every $j \in |\vec{z}|$, $Q_{j}$ is the set of natural number constants occurring in $\Pi_{i, j}(z_j, z'_j)$ with $i \in [k]$,
%
\item $\delta$ comprises the set of tuples $(\vec{r}, \vec{l}, \vec{s}, (\alpha, \beta))$ such that there exists $i \in [k]$ satisfying that 
\begin{itemize}
\item $\Pi_{i,0}(x, x', \vec{v}) \equiv \alpha \le x' - x $ if $u = \infty$, and $\Pi_{i,0}(x, x', \vec{v}) \equiv \alpha \le x' - x \le \beta$ otherwise, (Note that $\beta$ may contain parameters from $\vec{v}$.) 
%
\item for every $j \in [|\vec{w}|]$, $\Pi_{i,j}(w_j) \equiv w_j = l_j$, moreover, for every $j \in [|\vec{z}|]$,  $\Pi_{i, |\vec{w}|+j}(z_j, z'_j) \equiv z_j = r_{j} \wedge z'_j = s_{j}$. 
% (note that no constraints are put on $s_1 \in Q_1, \cdots, s_{|\vec{w}|} \in Q_{|\vec{w}|}$).
\end{itemize}
Intuitively, $\vec{r},\vec{s}$ are the source resp. target state, $\vec{l}$ is the label, $(\alpha, \beta)$ is the weight interval.
%
\end{itemize}
\end{definition}

\begin{lemma}[\cite{linthesis}, Theorem 7.3.1]\label{nfa-pi}
Let $\cA$ be an NFA of $n$ states over an alphabet of size $k$. Then there exists a representation of the Parikh images of $\cA$ as a union of linear sets $L(\vec{n}_1, S_1), \dots, L(\vec{n}_m, S_m)$, where the maximum entry of each $\vec{n}_i$ is $O(n^{3(k+1)}k^{4k+6})$, each $S_i$ is a subset of $\{0, \cdots, n\}^k$ with $|S_i| \le k$, and $m = O(n^{k^2+3k+3}k^{4k+6})$. Furthermore, this is computable in time $2^{O(k^2\log (kn))}$.
\end{lemma}

For every two states $\vec{q}_1, \vec{q}_2 \in Q$, let $\cA^{\vec{q}_1, \vec{q}_2}_P = (Q, \delta, \delta', \vec{q}_1, \{\vec{q}_2\})$ be the NFA where $\delta$ is the alphabet and the transition relation $\delta'$ is obtained from $\delta$ by replacing every transition $\tau = (\vec{q}, \vec{l}, \pvec{q}', (\alpha, \beta))$ with $(\vec{q}, \tau, \pvec{q}')$.

Then from Lemma~\ref{nfa-pi}, linear sets $L(\vec{n}_1, S_1), \dots, L(\vec{n}_m, S_m)$ can be computed in time $2^{O(|\delta|^2 \log (|\delta||Q|))}$.

The summary of $P$, denoted by $\summary(P)(x,\vec{z};y,\vec{u};\vec{v})$, is computed as a disjunction of the formulas of the following form: 
\[
\begin{array}{l}
\vec{z} = \vec{q}_1 \wedge \vec{u} = \vec{q}_2\ \wedge \\
\exists k_1 \cdots k_\ell.\ (\vec{n}_1 + k_1 \vec{n}'_1 + \cdots + k_\ell \vec{n}'_\ell) \cdot \vec{\alpha} \le y - x \le (\vec{n}_1 + k_1 \vec{n}'_1 + \cdots + k_\ell \vec{n}'_\ell) \cdot \vec{\beta},  
\end{array}
\]
where $S_i = \{\vec{n}'_1, \cdots, \vec{n}'_\ell\}$ for some $i \in [m]$ and $\ell \le |\delta|$,  $\vec{\alpha}$ (resp. $\vec{\beta}$) is the vector of the lower (resp. upper) bounds of the transitions of $\cA_P$, and $\cdot$ denotes the inner product of vectors.

The formula can be rewritten into the following form 
\[
\begin{array}{l}
\vec{z} = \vec{q}_1 \wedge \vec{u} = \vec{q}_2\ \wedge \\
\exists k_1 \cdots k_\ell.\ \vec{n}_1 \cdot \vec{\alpha} + \sum \limits_{1 \le i \le \ell} (\vec{n}'_i \cdot \vec{\alpha}) k_i  \le y - x \le \vec{n}_1 \cdot \vec{\beta} + \sum \limits_{1 \le i \le \ell} (\vec{n}'_i \cdot \vec{\beta}) k_i ,  
\end{array}
\]

Then we show that the formula
\begin{eqnarray}
\small
\exists k_1 \cdots k_\ell.\ \vec{n}_1 \cdot \vec{\alpha} + \sum \limits_{1 \le i \le \ell} (\vec{n}'_i \cdot \vec{\alpha}) k_i  \le y - x \le \vec{n}_1 \cdot \vec{\beta} + \sum \limits_{1 \le i \le \ell} (\vec{n}'_i \cdot \vec{\beta}) k_i.\label{formula-sum}
\end{eqnarray}
is equivalent to an existential Presburger arithmetic (EPA) formula. At first, we notice that Formula~\ref{formula-sum} can be rewritten into the formula of the following form 
{
\small
\begin{eqnarray}
\exists k_1 \cdots k_\ell.\ c_0 + \sum \limits_{1 \le i \le \ell} c_i k_i \le y - x \le d_0 + \sum_j d_j v_j + \sum \limits_{1 \le i \le \ell} (d'_{i,0}+\sum_j d'_{i,j} v_j) k_i,
\label{formula-qe}
\end{eqnarray}
}
where $d_0,  d'_{i,0} \in \ZZ$ and $c_0, c_i, d_j, d'_{i,j} \in \NN$ for every $i: 1 \le i \le \ell$ and $j$. Note that $d_0 \in \ZZ$ is due to the fact that the components of $\vec{\beta}$ may be of the form $v_j - n''$ for $n'' \in \NN$. Similarly for $d'_{i,0}$.

For illustration purpose, we start with the special case that $\ell = 1$, the extend the idea to the case $\ell > 1$.

Let $\ell = 1$. Then we get the formula of the form 
\begin{eqnarray}\label{formula-qe-1}
\small
%\exists k_1.\ \vec{n}_1 \cdot \vec{\alpha} + (\vec{n}'_1 \cdot \vec{\alpha}) k_1  \le y - x \le \vec{n}_1 \cdot \vec{\beta} + (\vec{n}'_1 \cdot \vec{\beta}) k_1.
\exists k_1.\ c_0 + c_1 k_1 \le y - x \le d_0 + \sum_j d_j v_j +  (d'_{1,0}+\sum_j d'_{1, j} v_j) k_1.
\end{eqnarray}
We distinguish between the following cases.
\begin{itemize}
\item 
%Case $\vec{n}'_1 \cdot \vec{\beta}$ contains no variables (e.g. $\vec{\beta}$ contains no variables): 
Case $d'_{1, j} = 0 $ for every $j \ge 1$: 
then Formula~\ref{formula-qe-1} is already an EPA formula
% and it is well-known that the existential quantifiers of EPA can be eliminated.
%for every $j \ge 1$, then the formula~\ref{formula-qe-1} is an existential Presburger formula and it is well-known that the existential quantifier can be eliminated. 

\item Case $d'_{1, j} > 0 $ for some $j \ge 1$:
\begin{itemize}
\item Subcase $c_1< d'_{1,0}+\sum_j d'_{1, j} v_j$: Then for every $k_1 \ge c_1$, we have 
\begin{eqnarray*}
(d_0 + \sum_j d_j v_j +  (d'_{1,0}+\sum_j d'_{1, j} v_j) k_1) - (c_0 + c_1 (k_1 +1 ))  & =  & \\
 (d_0 + \sum_j d_j v_j  - c_0) + (d'_{1,0}+\sum_j d'_{1, j} v_j - c_1)k_1 - c_1 & \ge &\\
  (d_0 + \sum_j d_j v_j  - c_0)  + c_1 - c_1 &\ge & 0.
\end{eqnarray*}
Therefore, $\bigcup \limits_{k_1 \ge c_1} [c_0 + c_1 k_1, d_0 + \sum_j d_j v_j +  (d'_{1,0}+\sum_j d'_{1, j} v_j) k_1]$ subsumes the interval $[c_0 + c_1^2, \infty]$.
Then Formula~\ref{formula-qe-1} is equivalent to the quantifier-free Presburger arithmetic formula
\[
y - x \ge c_0 + c_1^2 \vee \bigvee \limits_{0 \le k_1 < c_1} c_0 + c_1 k_1 \le y - x \le d_0 + \sum_j d_j v_j +  (d'_{1,0}+\sum_j d'_{1, j} v_j) k_1.
\]
%
%\item Subcase $c_0 = d_0 + \sum_j d_j v_j$ and $c_1= d'_{1,0}+\sum_j d'_{1, j} v_j$: The formula~\ref{formula-qe-1} is equivalent to the formula $y - x \ge c_0 \wedge y - x  \equiv c_0 \bmod c_1$.
%
\item Subcase $c_1= d'_{1,0}+\sum_j d'_{1, j} v_j$: Then formula~\ref{formula-qe-1} is equivalent to the EPA formula 
\[
\exists k_1.\ c_0 + c_1 k_1 \le y - x \le d_0 + \sum_j d_j v_j +  c_1 k_1.
\]
%where the quantifiers can be eliminated.
%\begin{itemize}
%\item Subcase $d_0 + \sum_j d_j v_j \ge c_0+c_1$: Then $\bigcup \limits_{k_1 \in \NN} [c_0 + c_1 k_1, d_0 + \sum_j d_j v_j +  (d'_{1,0}+\sum_j d'_{1, j} v_j) k_1] = [c_0, \infty]$. In this case,  formula~\ref{formula-qe-1} is equivalent to the formula $y - x \ge c_0$. 
%
%\item Subcase $d_0 + \sum_j d_j v_j < c_0+c_1$: We say that the variable $v_j$ is active if $d_j > 0$ or $d'_{1,j} > 0$. The number of valuations of active $v_j$'s satisfying $c_1= d'_{1,0}+\sum_j d'_{1, j} v_j$ and  $d_0 + \sum_j d_j v_j < c_0+c_1$ are finite. Let $V$ denote the set of these valuations. 
%Then formula~\ref{formula-qe-1} is equivalent to the formula 
%\[
%y - x \ge c_0 \wedge \bigvee \limits_{\rho \in V} \bigvee \limits_{c_0 \le \mu \le d_0 + \sum \limits_{v_j \mbox{\scriptsize active}} d_j \rho(v_j)} y - x \equiv \mu \bmod c_1.
%\]
%\end{itemize}
\end{itemize}
%
\end{itemize}

Next, we consider the general case $\ell > 1$.

If $d'_{i, j} = 0 $ for every $1 \le i \le \ell$ and $j \ge 1$, then Formula~\ref{formula-qe} is already an EPA formula. 

We then assume that $d'_{i, j} > 0 $ for some $1 \le i \le \ell$ and $j \ge 1$.
\begin{itemize}
\item Case $c_{i_0} < d'_{i_0, 0}+\sum_j d'_{i_0, j} v_j$ for some $1\le i_0 \le \ell$: Then $\bigcup \limits_{k_{i_0} \ge c_{i_0}} [c_0 + c_{i_0} k_{i_0},  d_{0} + \sum_j d_{j} v_j +  (d'_{i_0,0}+\sum_j d'_{i_0, j} v_j) k_{i_0}]$ subsumes the interval $[c_0 + c_{i_0}^2, \infty]$. Therefore, Formula~\ref{formula-qe} is equivalent to the quantifier-free Presburger arithmetic formula
\[
\small
y - x \ge c_0 + c^2_{i_0}  \vee  \bigvee \limits_{c_0 + \sum \limits_{1 \le i \le \ell} c_i k_i < c_0 + c^2_{i_0} } 
\left(
\begin{array}{l}
 c_0 + \sum \limits_{1 \le i \le \ell} c_i k_i \le y - x \le \\
 d_0 + \sum_j d_j v_j + \sum \limits_{1 \le i \le \ell} (d'_{i,0}+\sum_j d'_{i,j} v_j) k_i
\end{array}
\right).
\]
%
\item Case $c_{i} = d'_{i, 0}+\sum_j d'_{i, j} v_j$ for every $1\le i \le \ell$: Then Formula~\ref{formula-qe} is equivalent to the EPA formula 
{
\small
\begin{eqnarray*}
\exists k_1 \cdots k_\ell.\ c_0 + \sum \limits_{1 \le i \le \ell} c_i k_i \le y - x \le d_0 + \sum_j d_j v_j + \sum \limits_{1 \le i \le \ell} c_i k_i.
\end{eqnarray*}
}
%where the quantifiers can be eliminated.
%
%\begin{itemize}
%\item Subcase $d_0 + \sum_j d_j v_j \ge c_0+c_{i_0}$ for some $1 \le i_0 \le \ell$: Then $\bigcup \limits_{k_1 \in \NN} [c_0 + c_{i_0} k_{i_0}, d_0 + \sum_j d_j v_j +  (d'_{i_0,0}+\sum_j d'_{i_0, j} v_j) k_{i_0}] = [c_0, \infty]$. In this case,  formula~\ref{formula-qe-1} is equivalent to the formula $y - x \ge c_0$. 
%
%\item Subcase $d_0 + \sum_j d_j v_j < c_0+c_i$ for every $1 \le i \le \ell$: We say that the variable $v_j$ is active if $d_j > 0$ or $d'_{i,j} > 0$ for some $i$. The number of valuations of active $v_j$'s satisfying $c_{i} = d'_{i, 0}+\sum_j d'_{i, j} v_j$ for every $1\le i \le \ell$ and $d_0 + \sum_j d_j v_j < c_0+c_i$ for every $1 \le i \le \ell$ are finite. Let $V$ denote the set of these valuations. 
%Then formula~\ref{formula-qe-1} is equivalent to the formula 
%\[
%y - x \ge c_0 \wedge \bigvee \limits_{\rho \in V} \bigvee \limits_{c_0 \le \mu \le d_0 + \sum \limits_{v_j \mbox{\scriptsize active}} d_j \rho(v_j)} \exists k_1 \cdots k_\ell.\ c_0 + \sum \limits_{i} c_i k_i \le y - x \le c_0 \le \mu \le d_0 + \sum \limits_{v_j \mbox{\scriptsize active}} d_j \rho(v_j) + \sum \limits{i} c_i k_i.
%\]
%\end{itemize}
%
\end{itemize}
