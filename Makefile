
pdf: slid-dma.tex packages.sty commands.tex biblio.bib
	pdflatex slid-dma.tex
	bibtex slid-dma.aux
	pdflatex slid-dma.tex

notes: 18.Beijing.tex packages.sty commands.tex biblio.bib
	pdflatex 18.Beijing.tex
	bibtex 18.Beijing.aux
	pdflatex 18.Beijing.tex

clean:
	latexmk -C
