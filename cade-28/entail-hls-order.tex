%!TEX root = cade-28.tex

\subsection{Splitting into ordered entailments}
\label{ssec:ent-order}


In the sequel, we suppose that 
both $\varphi$ and $\psi$ are satisfiable 
and $\abs(\varphi) \vDash \abs(\psi)$.

\medskip
Let us introduce the notations first.
Recall that $\satoms(\varphi)$ denotes the set of spatial atoms of $\varphi$, $\atomhead(a)$ and $\atomtail(a)$ denote respectively
the terms used for the start and end addresses  of a spatial atom $a$.
Given a set of spatial atoms $\cA$,
we denote by 
$\mathbb{A}(\cA) = \{ t \mid \exists a \in \cA\st t\equiv\atomhead(a) \lor t\equiv\atomtail(a) \}$, 
the terms used as start and end addresses of spatial atoms in $\cA$.

\hide{
Then for every predicate atom $\hls{}(t_1, t_2; t')$ in $\varphi$ (resp. $\psi$),  we do one of the following to $\varphi$ (resp. $\psi$).
\begin{itemize}
\item Add the pure constraint $t_1 = t_2 $ to $\varphi$ (resp. $\psi$), and remove the predicate atom from $\varphi$ (resp. $\psi$).
%
\item Add the pure constraint $t_1 < t_2$ to $\varphi$ (resp. $\psi$).
\end{itemize}
Let $\varphi' \equiv \Pi'_1: \Sigma'_1$ and $\psi' \equiv \Pi'_2: \Sigma'_2$  be the resulting formulas.
 %with $\Sigma'_1 = a_1 \sepc \cdots \sepc a_m$ and $\Sigma'_2 = b_1 \sepc \cdots \sepc b_n$. 
 Note that the formulas $\varphi'$ and $\psi'$ satisfy that the predicate atoms therein have to be matched to nonempty heaps.  If for every $\varphi', \psi'$ such that $\abs(\varphi')$ is satisfiable and $\abs(\varphi') \vDash \exists \overrightarrow{\isemp_{\psi'}}.\ \abs(\psi')$,  it holds that $\varphi' \vDash \psi'$, then the entailment holds. Otherwise, the entailment does not hold. It remains to show how to decide  $\varphi' \vDash \psi'$, which will be presented in the rest of this section.
}

The decomposition builds first the set of all equivalence relations 
	 between the terms in 
	 $\mathcal{T}\triangleq\mathbb{A}(\satoms(\varphi) \cup \satoms(\psi))$ 
	 that are compatible with $\abs(\varphi)$.
	 More precisely, given an equivalence relation $\sim$ over this set,
	 $\sim$ is compatible with $\abs(\varphi)$ if the following
	 formula is satisfiable:
\begin{eqnarray}
C_\sim & \triangleq & \abs(\varphi) \land 
			\bigwedge_{t_1,t_2\in\mathcal{T}, t_1\sim t_2} t_1 = t_2
\end{eqnarray}
	 Notice that there are possibly exponentially many such equivalence relations compatible with $\abs(\varphi)$.
	 It is easy to observe that the terms corresponding to a non-predicate atom cannot be put inside the same equivalence class.
	 Other heuristics may be applied to reduce the set of equivalence relations
	 to be explored.\todo{MS: refer to what done in implementation}.
%ESOP'21 Reviewer 3: seems a bit vague - perhaps you could elaborate on any ideas and/or what has already been done in the implementation.
	The relation $\sim$ is used to transform the formulas $\varphi$ and $\psi$ 
	by removing the atoms $\hls{}(t_1,t_2;t_3)$ when $t_1 \sim t_2$. 
%	Notice that $\sim$ fixes the values of variables $\isemp$ in $\abs(\varphi)$.
We denote by $\varphi_\sim$ and $\psi_\sim$ the resulting formulas, where the pure constraint of $\varphi_\sim$ is $C_\sim$, while the pure constraint of $\psi_\sim$ is the same as that of $\psi$.
The following lemma states the correctness of this first step.


\begin{lemma}\label{lem:split-equiv}
Suppose that $\abs(\varphi)$ and $\abs(\psi)$ are satisfiable. Then $\varphi \models \psi$ is valid iff for every
equivalence relations $\sim$ over $\mathcal{T}=\mathbb{A}(\satoms(\varphi) \cup \satoms(\psi))$
that is compatible with $\abs(\varphi)$, 
we have $\varphi_\sim \models \psi_\sim$.
\end{lemma}

%The lemma follows from the construction of $\sim$.

\smallskip
The second step of the decomposition process considers all total orders $\prec$ between the 
	equivalence classes of terms, 
	i.e. over $[\mathcal{T}]_\sim$,
	which are compatible with $C_\sim$, namely, the following formula is satisfiable:
\begin{eqnarray}
C_\prec & \triangleq & C_\sim \land 
	\bigwedge_{[t_1],[t_2]\in[\mathcal{T}]_\sim,[t_1]_\sim \prec [t_2]_\sim} t_1 < t_2
\end{eqnarray}
	Like for the equivalence classes, there are heuristics allowing to
	improve the naive enumeration of all total orders 
	compatible with $C_\sim$ by considering only the total orders obtained by
	completing the partial order induced by $C_\sim$.
	Given a total order $\prec$, we could transform $\varphi_\sim$ and
	$\psi_\sim$ into respectively $\varphi_\prec$ and $\psi_\prec$
	such that all spatial atoms are ordered using $\prec$ in the 
	increasing order of their start addresses,
	and the pure constraint of $\varphi_\prec$ includes the equality and ordering constraints
	introduced by $\sim$ and $\prec$, while the pure constraint of $\psi_\prec$ is still that of $\psi$.
	The following lemma states the correctness of this second step.
%
\hide{
In a nutshell, to decide $\varphi' \vDash \psi'$, we consider all total orders of the addresses of $\varphi'$ and $\psi'$, and for each total order $\preceq$, use $\preceq$ to reorder the spatial atoms in $\varphi'$ and $\psi'$ by putting the atoms with smaller head addresses before those with greater head addresses, then match the atoms of $\psi'$ to the subheaps in $\varphi'$, from left to right, one by one.
}

\begin{lemma}\label{lem:split-prec}
Suppose $C_\sim$ is satisfiable. Then $\varphi_\sim \models \psi_\sim$ is valid iff 
for every ordering relation $\prec$ over $[\mathcal{T}]_\sim$
that is compatible with $C_\sim$, 
we have $\varphi_\prec \models \psi_\prec$.
\end{lemma}
	
\hide{
Let $\mathbb{A}_{\varphi', \psi'}$ be the union of the set of $\atomhead(a), \atomtail(a)$ for atoms $a$ in $\varphi'$ and $\psi'$. Evidently, $\abs(\varphi')$ induces an equivalence relation $\sim$ on $\mathbb{A}_{\varphi',\psi'}$, namely, for $t_1, t_2 \in \mathbb{A}_{\varphi',\psi'}$, $t_1 \sim t_2$ iff $\abs(\varphi') \models t_1 = t_2$. Let $[\mathbb{A}_{\varphi',\psi'}]_\sim$ denote the set of equivalence classes of $\mathbb{A}_{\varphi',\psi'}$ with respect to $\sim$. Then $\abs(\varphi')$ induces a partial order on $[\mathbb{A}_{\varphi',\psi'}]_\sim$. We can complete the partial order into a total order $\prec$. Let $\Pi_\prec$ denote the pure constraint corresponding to $\prec$. For instance, if $\{t_1, t_2\}$ and $\{t_3\}$ are two equivalence classes of $\sim$, and $\{t_1,t_2\} \prec \{t_3\}$, then $\Pi_\prec \equiv t_1 < t_3$.
For each total order $\prec$ compatible with $\varphi'$, namely, $\Pi_\prec \wedge \abs(\varphi')$ is satisfiable, we do the following: Reorder the spatial atoms of $\varphi'$ and $\psi'$ by following the total order $\prec$, with the atoms of the smaller head addresses before those of greater head addresses. Let us use $\Pi''_1: \Sigma''_1$ and $\Pi''_2: \Sigma''_2$ to denote the resulting formulas, where $\Sigma''_1 = a_1 \sepc \cdots \sepc a_m$ and $\Sigma''_2 =  b_1 \sepc \cdots \sepc b_n$. It remains to show how to solve the entailment problem for $\varphi'', \psi''$ where all spatial atoms are ordered.
}
In the next step, we consider that the operator $\sepc$ is not commutative\todo{MS: precise}
in order to preserve the ordering of spatial atoms fixed by 
the chosen total order $\prec$.
% ESOP'21 Reviewer 3: is both surprising and not explained. I *believe* the idea here is to exploit the list structure of a separating conjunction, i.e. to assume that if a sequence of list segments notionally represents the parts of a list segment then they will also be arranged in the corresponding order and consecutively
